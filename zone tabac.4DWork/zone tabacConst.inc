// File generated 02/10/2018 09:51:12
// Warning! This is a generated file, any manual changes will be
// lost during the next generation.

#constant  Strings32Count    0
#constant  Strings32Size     1
#constant  Strings55Count    0
#constant  Strings55Size     1
#constant  Strings1Count    0
#constant  Strings1Size     1
#constant  Strings2Count    0
#constant  Strings2Size     1
#constant  Strings3Count    0
#constant  Strings3Size     1
#constant  Strings0Count    0
#constant  Strings0Size     1
#constant  Strings25Count    0
#constant  Strings25Size     1
#constant  Strings26Count    0
#constant  Strings26Size     1
#constant  Strings57Count    0
#constant  Strings57Size     1
#constant  Strings4Count    0
#constant  Strings4Size     1
#constant  Strings10Count    0
#constant  Strings10Size     1
#constant  Strings12Count    0
#constant  Strings12Size     1
#constant  Strings13Count    0
#constant  Strings13Size     1
#constant  Strings14Count    0
#constant  Strings14Size     1
#constant  Strings15Count    0
#constant  Strings15Size     1
#constant  Strings16Count    0
#constant  Strings16Size     1
#constant  Strings17Count    0
#constant  Strings17Size     1
#constant  Strings18Count    0
#constant  Strings18Size     1
#constant  Strings19Count    0
#constant  Strings19Size     1
#constant  Strings20Count    0
#constant  Strings20Size     1
#constant  Strings21Count    0
#constant  Strings21Size     1
#constant  Strings22Count    0
#constant  Strings22Size     1
#constant  Strings23Count    0
#constant  Strings23Size     1
#constant  Strings24Count    0
#constant  Strings24Size     1
#constant  Strings11Count    0
#constant  Strings11Size     1
#constant  Strings53Count    0
#constant  Strings53Size     1
#constant  Strings5Count    0
#constant  Strings5Size     1
#constant  Strings6Count    0
#constant  Strings6Size     1
#constant  Strings8Count    0
#constant  Strings8Size     1
#constant  Strings27Count    0
#constant  Strings27Size     1
#constant  Strings28Count    0
#constant  Strings28Size     1
#constant  Strings29Count    0
#constant  Strings29Size     1
#constant  Strings31Count    0
#constant  Strings31Size     1
#constant  Strings9Count    0
#constant  Strings9Size     1
#constant  Strings33Count    0
#constant  Strings33Size     1
#constant  Strings34Count    0
#constant  Strings34Size     1
#constant  Strings35Count    0
#constant  Strings35Size     1
#constant  Strings36Count    0
#constant  Strings36Size     1
#constant  Strings7Count    0
#constant  Strings7Size     1
#constant  Strings58Count    0
#constant  Strings58Size     1
#constant  Strings62Count    0
#constant  Strings62Size     1
#constant  Strings30Count    0
#constant  Strings30Size     1
#constant  Strings66Count    0
#constant  Strings66Size     1
#constant  Strings67Count    0
#constant  Strings67Size     1
#constant  Strings68Count    0
#constant  Strings68Size     1
#constant  Strings69Count    0
#constant  Strings69Size     1
#DATA
    byte iKeyboard0keystrokes 0x07, 0x08, 0x09, 0x04, 0x05, 0x06, 0x01, 0x02, 0x03, 0x0A, 0x00, 0x0B, 0x0C, 0x0D
#END


// object indexes into ImageControl
#CONST
    iWinbutton29    // offset 0x0
    iStatictext15   // offset 0x4A00
    iStatictext35   // offset 0xA800
    iStatictext38   // offset 0x10A00
    iStatictext100  // offset 0x14400
    iWinbutton39    // offset 0x14A00
    iWinbutton54    // offset 0x1B400
    iWinbutton55    // offset 0x21E00
    iStatictext39   // offset 0x28800
    iWinbutton56    // offset 0x2C000
    iStatictext4    // offset 0x33A00
    iStatictext5    // offset 0x36E00
    iStatictext6    // offset 0x37200
    iStatictext7    // offset 0x38200
    iStatictext8    // offset 0x38E00
    iStatictext17   // offset 0x39200
    iStatictext18   // offset 0x3A200
    iStatictext19   // offset 0x3AE00
    iStatictext20   // offset 0x3B200
    iStatictext33   // offset 0x3B600
    iStatictext34   // offset 0x3BE00
    iWinbutton7     // offset 0x3C600
    i4Dbutton2      // offset 0x41200
    iStatictext0    // offset 0x60C00
    iStatictext1    // offset 0x62E00
    iStatictext2    // offset 0x63E00
    iStatictext9    // offset 0x64E00
    iUserimages1    // offset 0x65E00
    i4Dbutton1      // offset 0x6A000
    i4Dbutton4      // offset 0x89A00
    iStatictext10   // offset 0xA9400
    iStatictext11   // offset 0xAC800
    iStatictext16   // offset 0xAF000
    iLeddigits2     // offset 0xB2A00
    iiLeddigits2    // offset 0xB3E00
    iLeddigits3     // offset 0xB9A00
    iiLeddigits3    // offset 0xBAE00
    iStatictext52   // offset 0xC0A00
    iStatictext21   // offset 0xC0C00
    iStatictext22   // offset 0xC4600
    iStatictext23   // offset 0xC5200
    iStatictext24   // offset 0xC6000
    iStatictext26   // offset 0xC6400
    iStatictext27   // offset 0xC7400
    iStatictext28   // offset 0xC8400
    iStatictext29   // offset 0xC8800
    iStatictext30   // offset 0xC8C00
    iWinbutton8     // offset 0xC9800
    iWinbutton9     // offset 0xCF600
    iWinbutton10    // offset 0xD5400
    iWinbutton11    // offset 0xDB200
    iWinbutton12    // offset 0xE1000
    iWinbutton13    // offset 0xE6E00
    iWinbutton14    // offset 0xECC00
    iWinbutton15    // offset 0xF2A00
    iStatictext31   // offset 0xF8800
    iStatictext32   // offset 0xF9000
    i4Dbutton0      // offset 0xFA000
    iStatictext77   // offset 0x100E00
    iStatictext78   // offset 0x101600
    iStatictext79   // offset 0x101E00
    iStatictext80   // offset 0x102600
    iStatictext81   // offset 0x102E00
    iStatictext82   // offset 0x103200
    i4Dbutton5      // offset 0x103600
    iWinbutton20    // offset 0x10A400
    iKeyboard0      // offset 0x10F000
    iKeyboard0_0    // offset 0x162E00
    iKeyboard0_1    // offset 0x16D400
    iKeyboard0_2    // offset 0x177A00
    iKeyboard0_3    // offset 0x182000
    iKeyboard0_4    // offset 0x18C600
    iKeyboard0_5    // offset 0x196C00
    iKeyboard0_6    // offset 0x1A1200
    iKeyboard0_7    // offset 0x1AB800
    iKeyboard0_8    // offset 0x1B5E00
    iKeyboard0_9    // offset 0x1C0400
    iKeyboard0_10   // offset 0x1CAA00
    iKeyboard0_11   // offset 0x1D5000
    iKeyboard0_12   // offset 0x1DF600
    iKeyboard0_13   // offset 0x1FE400
    iStatictext25   // offset 0x208A00
    iWinbutton36    // offset 0x209C00
    iStatictext48   // offset 0x20E800
    iDipswitch0     // offset 0x211000
    iStatictext55   // offset 0x21B600
    iStatictext56   // offset 0x21CE00
    iStatictext57   // offset 0x21D800
    iStatictext101  // offset 0x21E400
    iStatictext36   // offset 0x21EE00
    iStatictext37   // offset 0x221200
    iStatictext40   // offset 0x222C00
    iWinbutton1     // offset 0x224800
    iWinbutton2     // offset 0x22B200
    iStatictext41   // offset 0x231C00
    iStatictext49   // offset 0x233200
    iWinbutton3     // offset 0x234A00
    iWinbutton4     // offset 0x23B400
    iWinbutton5     // offset 0x241E00
    iWinbutton16    // offset 0x248800
    iStatictext50   // offset 0x24F200
    iStatictext51   // offset 0x250A00
    iWinbutton17    // offset 0x252E00
    iWinbutton6     // offset 0x259800
    iWinbutton30    // offset 0x264000
    iStatictext42   // offset 0x268C00
    iStatictext43   // offset 0x26B800
    iWinbutton31    // offset 0x26D600
    iWinbutton32    // offset 0x274000
    iStatictext44   // offset 0x27AA00
    iWinbutton33    // offset 0x27C400
    iStatictext45   // offset 0x282E00
    iWinbutton34    // offset 0x284E00
    iStatictext46   // offset 0x28B800
    iStatictext47   // offset 0x28D200
    iWinbutton35    // offset 0x28EA00
    iWinbutton22    // offset 0x295400
    iStatictext12   // offset 0x29A000
    iStatictext13   // offset 0x29D800
    iWinbutton23    // offset 0x29E200
    iWinbutton24    // offset 0x2A5800
    iWinbutton25    // offset 0x2ACE00
    iWinbutton26    // offset 0x2B4400
    iWinbutton27    // offset 0x2BBA00
    iWinbutton28    // offset 0x2C4800
    iStatictext14   // offset 0x2CD600
    iLeddigits0     // offset 0x2CE400
    iiLeddigits0    // offset 0x2D2000
    iLeddigits1     // offset 0x2E4400
    iiLeddigits1    // offset 0x2E8000
    iWinbutton59    // offset 0x2FA400
    iWinbutton60    // offset 0x303200
    iWinbutton65    // offset 0x30C000
    iWinbutton67    // offset 0x314E00
    iImage2         // offset 0x31C400
    iStatictext102  // offset 0x36D600
    iWinbutton72    // offset 0x36FA00
    iWinbutton77    // offset 0x37A600
    iStatictext110  // offset 0x385200
    iStatictext111  // offset 0x386C00
    iWinbutton80    // offset 0x388A00
    iStatictext112  // offset 0x38D600
    iWinbutton89    // offset 0x38F400
    iWinbutton0     // offset 0x39A000
    iStatictext3    // offset 0x3A4C00
    iStatictext117  // offset 0x3A6600
    iWinbutton86    // offset 0x3A8A00
    iWinbutton87    // offset 0x3AF400
    iStatictext118  // offset 0x3B5E00
    iStatictext119  // offset 0x3B8800
    iStatictext120  // offset 0x3BCC00
    iStatictext121  // offset 0x3BD400
    iStatictext122  // offset 0x3BD800
    iWinbutton88    // offset 0x3BDC00
    iLed2           // offset 0x3C2800
    iLed3           // offset 0x3C7200
    iStatictext123  // offset 0x3CC200
    iStatictext124  // offset 0x3CDA00
    iLed4           // offset 0x3CF200
    iLed6           // offset 0x3D5600
    iStatictext128  // offset 0x3DAA00
    iWinbutton91    // offset 0x3DC600
    iWinbutton92    // offset 0x3E2400
    iStatictext129  // offset 0x3E8200
    iStatictext130  // offset 0x3E9A00
#END

#constant  Strings37Size     0
#constant  Strings37StartH   0
#constant  Strings37StartL   0
#constant  Strings38Size     0
#constant  Strings38StartH   0
#constant  Strings38StartL   0
#constant  Strings39Size     0
#constant  Strings39StartH   0
#constant  Strings39StartL   0
#constant  Strings40Size     0
#constant  Strings40StartH   0
#constant  Strings40StartL   0
#constant  Strings41Size     0
#constant  Strings41StartH   0
#constant  Strings41StartL   0
#constant  Strings42Size     0
#constant  Strings42StartH   0
#constant  Strings42StartL   0
#constant  Strings43Size     0
#constant  Strings43StartH   0
#constant  Strings43StartL   0
#constant  Strings44Size     0
#constant  Strings44StartH   0
#constant  Strings44StartL   0
#constant  Strings45Size     0
#constant  Strings45StartH   0
#constant  Strings45StartL   0
#constant  Strings46Size     0
#constant  Strings46StartH   0
#constant  Strings46StartL   0
#constant  Strings47Size     0
#constant  Strings47StartH   0
#constant  Strings47StartL   0
#constant  Strings48Size     0
#constant  Strings48StartH   0
#constant  Strings48StartL   0
#constant  Strings49Size     0
#constant  Strings49StartH   0
#constant  Strings49StartL   0
#constant  Strings50Size     0
#constant  Strings50StartH   0
#constant  Strings50StartL   0
#constant  Strings51Size     0
#constant  Strings51StartH   0
#constant  Strings51StartL   0
#constant  Strings52Size     0
#constant  Strings52StartH   0
#constant  Strings52StartL   0
#constant  Strings54Size     0
#constant  Strings54StartH   0
#constant  Strings54StartL   0
#constant  Strings56Size     0
#constant  Strings56StartH   0
#constant  Strings56StartL   0
#constant  Strings59Size     0
#constant  Strings59StartH   0
#constant  Strings59StartL   0
#constant  Strings60Size     0
#constant  Strings60StartH   0
#constant  Strings60StartL   0
#constant  Strings61Size     0
#constant  Strings61StartH   0
#constant  Strings61StartL   0
#constant  Strings63Size     0
#constant  Strings63StartH   0
#constant  Strings63StartL   0
#constant  Strings64Size     0
#constant  Strings64StartH   0
#constant  Strings64StartL   0
#constant  Strings65Size     0
#constant  Strings65StartH   0
#constant  Strings65StartL   0
#constant  Strings32StartH   0x0
#constant  Strings32StartL   0x0
#constant  Strings55StartH   0x0
#constant  Strings55StartL   0x200
#constant  Strings1StartH   0x0
#constant  Strings1StartL   0x400
#constant  Strings2StartH   0x0
#constant  Strings2StartL   0x600
#constant  Strings3StartH   0x0
#constant  Strings3StartL   0x800
#constant  Strings0StartH   0x0
#constant  Strings0StartL   0xA00
#constant  Strings25StartH   0x0
#constant  Strings25StartL   0xC00
#constant  Strings26StartH   0x0
#constant  Strings26StartL   0xE00
#constant  Strings57StartH   0x0
#constant  Strings57StartL   0x1000
#constant  Strings4StartH   0x0
#constant  Strings4StartL   0x1200
#constant  Strings10StartH   0x0
#constant  Strings10StartL   0x1400
#constant  Strings12StartH   0x0
#constant  Strings12StartL   0x1600
#constant  Strings13StartH   0x0
#constant  Strings13StartL   0x1800
#constant  Strings14StartH   0x0
#constant  Strings14StartL   0x1A00
#constant  Strings15StartH   0x0
#constant  Strings15StartL   0x1C00
#constant  Strings16StartH   0x0
#constant  Strings16StartL   0x1E00
#constant  Strings17StartH   0x0
#constant  Strings17StartL   0x2000
#constant  Strings18StartH   0x0
#constant  Strings18StartL   0x2200
#constant  Strings19StartH   0x0
#constant  Strings19StartL   0x2400
#constant  Strings20StartH   0x0
#constant  Strings20StartL   0x2600
#constant  Strings21StartH   0x0
#constant  Strings21StartL   0x2800
#constant  Strings22StartH   0x0
#constant  Strings22StartL   0x2A00
#constant  Strings23StartH   0x0
#constant  Strings23StartL   0x2C00
#constant  Strings24StartH   0x0
#constant  Strings24StartL   0x2E00
#constant  Strings11StartH   0x0
#constant  Strings11StartL   0x3000
#constant  Strings53StartH   0x0
#constant  Strings53StartL   0x3200
#constant  Strings5StartH   0x0
#constant  Strings5StartL   0x3400
#constant  Strings6StartH   0x0
#constant  Strings6StartL   0x3600
#constant  Strings8StartH   0x0
#constant  Strings8StartL   0x3800
#constant  Strings27StartH   0x0
#constant  Strings27StartL   0x3A00
#constant  Strings28StartH   0x0
#constant  Strings28StartL   0x3C00
#constant  Strings29StartH   0x0
#constant  Strings29StartL   0x3E00
#constant  Strings31StartH   0x0
#constant  Strings31StartL   0x4000
#constant  Strings9StartH   0x0
#constant  Strings9StartL   0x4200
#constant  Strings33StartH   0x0
#constant  Strings33StartL   0x4400
#constant  Strings34StartH   0x0
#constant  Strings34StartL   0x4600
#constant  Strings35StartH   0x0
#constant  Strings35StartL   0x4800
#constant  Strings36StartH   0x0
#constant  Strings36StartL   0x4A00
#constant  Strings7StartH   0x0
#constant  Strings7StartL   0x4C00
#constant  Strings58StartH   0x0
#constant  Strings58StartL   0x4E00
#constant  Strings62StartH   0x0
#constant  Strings62StartL   0x5000
#constant  Strings30StartH   0x0
#constant  Strings30StartL   0x5200
#constant  Strings66StartH   0x0
#constant  Strings66StartL   0x5400
#constant  Strings67StartH   0x0
#constant  Strings67StartL   0x5600
#constant  Strings68StartH   0x0
#constant  Strings68StartL   0x5800
#constant  Strings69StartH   0x0
#constant  Strings69StartL   0x5A00
#IFNOT EXISTS NOGLOBALS
var hndl ;
var oKeyboard0[12] := [-1, 0, 0, 0, 0, 255, 255, 255, 255, 255, 14, 0] ;
#ENDIF
