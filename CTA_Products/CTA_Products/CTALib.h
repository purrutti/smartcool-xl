/*
Name:		CTALib.h
Created:	22/02/2017 14:26:31
Author:	puosm
Editor:	http://www.visualmicro.com
*/

#ifndef CTALib_h
#define CTALib_h

#include <PID_v1.h>
#include <rtcLib.h>
/*
#if defined(ARDUINO) && ARDUINO >= 100
#include "arduino.h"
#else
#include "WProgram.h"
#endif*/

#include <EEPROMex.h>
//#include "PID_one.h"


/*
* Classe Ventilateur
*/
class Ventilo {
public:
	PID myPID;
	PID myPIDPression;
	byte startAddress;
	byte addressSize;
	byte pin_ON_OFF;
	byte pin_CMD;
	byte pin_DEFAUT;
	int consigne_m3h;
	double consigne_app_m3h;
	double consigne_app_Pa;
	int cons_app_pc;
	double cons_app_pts;
	double Kp;
	double Ki;
	double Kd;
	double debit;
	double pression;
	int Kfactor;
	bool defaut;
	bool alarmeSurVentil;
	bool alarmePeriode;
	int secondeChangementFiltre;
	int minuteChangementFiltre;
	int heureChangementFiltre;
	int jourChangementFiltre;
	int moisChangementFiltre;
	int anneeChangementFiltre;
	unsigned long changementFiltre;
	bool memoCheckAlarmeSurVentil;
	bool alarmePerteDeCharge;
	bool marche;
	bool changedPID;
	bool commandeMarche;

	int consigneDebitFreeCooling;
	int consigneDebitModeOccupe;
	int consigneDebitModeInoccupe;
	int consignePressionModeOccupe;
	int consignePressionModeInoccupe;

	int consigneForcage;



	Ventilo(byte mypin_ON_OFF, byte mypin_CMD, byte mypin_DEFAUT, int K);
	void setAddress(byte add);
	void calculDebit(float pression);
	double returnCalculDebit(float pression);
	void update();
	void saveToEEPROM();
	void loadFromEEPROM();
	void calculPression(float pression);
};

/*
* Classe entr�e ANA
*/
class Pression {
public:
	byte pin;
	int value;
	Pression(byte myPin);
	int read();
	int readAna();
};

class Batterie {
public:
	PID myPID;
	byte startAddress;
	byte addressSize;
	byte pin_ON_OFF;
	byte pin_CMD;
	byte pin_DEFAUT;
	int consigne_Temp;

	double cons_app_temp_souff;
	int cons_app_pc;
	double cons_app_pts;
	double Kp;
	double Ki;
	double Kd;
	double temp_souff;
	bool defaut;
	bool marche;
	bool changedPID;
	bool commandeMarche;

	int consigneForcage;
	
	Batterie(byte mypin_CMD);
	void setAddress(byte add);
	void update();
	void saveToEEPROM();
	void loadFromEEPROM();
};

/* Temperature */
class Temperature {
public:
	PID myPID;
	byte startAddress;
	byte addressSize;
	bool changedPID;
	double consigne_app_Temp_amb;
	int cons_app_pc;
	double cons_app_temp_souff;
	double Kp;
	double Ki;
	double Kd;
	int consigneTempHiverOccupe;
	int consigneTempEteOccupe;
	int consigneTempHiverInoccupe;
	int consigneTempEteInoccupe;
	double temp_amb;
	Temperature(byte mypin_CMD);
	void recupTemp(float t);
	void setAddress(byte add);
	void saveToEEPROM();
	void loadFromEEPROM();
};

/*
* Classe Registre Motorise
*//*
class RM {
public:
	PID PID_soufflage;
	PID PID_reprise;
	byte startAddress;
	byte addressSize;
	byte pin_CMD;
	double mesureTempS;
	double consigneTempS;
	double cons_app_pcS;
	double cons_app_ptsS;
	double KpS;
	double KiS;
	double KdS;
	double mesureTempR;
	double consigneTempR;
	double cons_app_pcR;
	double cons_app_ptsR;
	double KpR;
	double KiR;
	double KdR;
	double consigneForcage;
	bool forcage;
	bool changedPID;

	RM(byte mypin_CMD);
	void update();
	void saveToEEPROM();
	void loadFromEEPROM();
};*/


#endif
