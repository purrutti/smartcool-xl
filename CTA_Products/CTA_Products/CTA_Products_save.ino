﻿

//#include "PID_one.h"

//#include <EEPROM.h>
#include <EEPROMVar.h>
#include <EEPROMex.h>
#include <SPI.h>
#include "genieArduino.h"
#include "ModbusIP.h"
#include "CTALib.h"
#include <DallasTemperature.h>
#include <OneWire.h>
#include <PID_v1.h>
#include <CONTROLLINO.h>



#define NB_JOURS_ANNEES_PRECEDENTES(a) (((a)*365) + (((a)+3)>>2))
#define PAS_BISSEXTILE(a) ((a)&3)
// VARIABLES GLOBALES

#define memoryBase 32

//PIN LAYOUT
#define pin_DEFAUT_V1 10                   //V2= REPRISE V1= SOUFF			//A check 
#define pin_CMD_RM1 7
#define pin_ONOFF_V1 5
#define pin_ONOFF_V2 6
#define pin_CMD_V1 12
#define pin_TEMPERATURE A13 
#define pin_DEFAUT_V2 11
#define pin_CMD_V2 13
#define pin_P1 A14		
#define pin_P2 A15	
#define pin_P3 A6  
#define RESETLINE 43
#define RESETLINE2 45
#define BP_MARCHE A10
#define BP_ARRET A11
#define POTAR A0
#define VOYANT_MARCHE 3
#define VOYANT_DEFAUT 2
#define PRESSOSTAT_P1 A11
#define PRESSOSTAT_P2 A12

int codeAdmin = 9876;


/*TEMPERATURES*/
OneWire ds(pin_TEMPERATURE); //Cr?ation de l'objet OneWire pour manipuler le bus 1-Wire
DallasTemperature sensors(&ds);
int SOUFFLAGE = 0;
int REPRISE = 1;
int EXTRACTION = 2;
int AIRNEUF = 3;

int seuilTempHorsGel = 5;
int debitMaxCTA = 6000;
int pressionMaxCTA = 2000;
int consignePressionModeOccupe;
int consignePressionModeInoccupe;
int tempsMinBatFroid;
int tempsMaxBatFroid;
int tempsMinBatChaud;
int tempsMaxBatChaud;

int alarmePeriode;
int alarmeSurVentil;

bool regulTempReprise = false; // true = regul sur temperature soufflage; false = regul sur temperature soufflage
bool changeOver = false; //true = ete; false = hiver
bool occupe = false; //true = Occupe; false = inoccupe

bool KB = false;
bool changedParams = false;

bool GTCLive = true;

bool GTCOption = true;
bool registreOption = true;
bool boiteaboutonOption = true;
bool batterieChaudeOption = true;
bool batterieFroideOption = true;
bool alarmeEncrassementFiltreOption = true;
bool commandeDeporteeOption;
bool ecranOption;
bool regulSurDepressionOption;

int heure;
int minute;
int annee;
int mois;
int jour;
int jourSemaine;


int heuremodif;
int minutemodif;
int anneemodif;
int moismodif;
int jourmodif;
int jourSemainemodif;
int anneeFiltre;

typedef struct {
	float value;
	byte addr[8];
	byte status;
}temperatureDS;
temperatureDS temperature[4];

/* Code de retour de la fonction getTemperature() */
enum DS18B20_RCODES {
	READ_OK,
	NO_SENSOR_FOUND,
	INVALID_ADDRESS,
	INVALID_SENSOR
};

/*
* ECRAN 4D SYSTEMS
*/
Genie genie;
static long waitPeriod;
static long waitPeriodTemps;
static long waitPeriodDate;
double *ptr;
int *int_ptr;
String * string_ptr;

/*ECRAN 4D SYSTEMS
*/
Genie genie2;


int int_KBmin, int_KBmax;
double double_KBmin, double_KBmax;

bool readIntFromKB = false;
bool readStrFromKB = false;
bool readHeureFromKB = false;
bool erreurKB = false;
bool bpmarche = false;
bool bparret = false;
bool ecranbparret;
bool ecranbpmarche;

bool Ventilodefaut = false;
bool RegistreONOFF = false;
bool Registreforcage = false;
bool savedate = false;
bool modifdate = false;

int consigne_m3h;

String str = "";
String param = "";
String versioncontrollino = "CTA_Products.ino";
String IPC = "";
String IGW = "";
int slideConsigne_m3h;
bool FormTemperatureActivated = false;
enum FORM {
	PARAMS_RESEAU = 0,
	MENU_CMD = 1,
	MENUADMIN_FULL = 2,
	SYNOPTIQUE = 3,
	PARAMS_VENTILO = 4,
	KEYBOARD = 5,
	PARAMS_CTA = 6,
	PARAMS_CTA2 = 7,
	CONFIG_CTA = 8,
	PARAMS_RM = 9,
	BOUTONS = 10,
	PARAMS_CTA3 = 11,
	DATE_ET_HEURE = 12,
	OPTIONS_CTA = 13,
	MENU = 14,
	OPTION_OFF = 15,
	BATTERIES = 16,
	ECRAN = 18,
	FILTRE = 19
	
};

FORM form = MENU;
/*
* PARAMETRES CTA
*/
enum modeMarche {
	ARRET,
	MANU,
	AUTO
};
enum modeMarcheAuto {
	REGULDEBIT,
	REGULDEPRESSION,
	CMDDEPORTEE
};

enum modeHE {
	HIVER_OCC,
	HIVER_INOCC,
	ETE_OCC,
	ETE_INOCC
};

enum mode {
	NORMAL,
	FREECOOLING,
	FREEHEATING,
	HORSGEL  // pas forcement 
};
modeHE modeHE_Applique = HIVER_OCC;
modeMarche modeMarche_Applique = ARRET;
modeMarcheAuto modeMarcheAutoOcc;
modeMarcheAuto modeMarcheAutoInocc;
mode modeApplique = NORMAL;
int consignesTemp[4];

int heureDebutOccupe;
int heureFinOccupe;
bool jourOccupe[7];
int jourOccupe_int;

/*
* EBM PAPST ROTOR Dimension: 250 / 280 / 310 / 355 / 400 / 450 / 500 / 560 / 630 / 710 / 800 / 900
*                  K-FACTOR:  76 / 77 / 116 / 148 / 188 / 240 / 281 / 348 / 438 / 545 / 695 / 900
*/

Ventilo V2(pin_ONOFF_V2, pin_CMD_V2, pin_DEFAUT_V2, 240);
Ventilo V1(pin_ONOFF_V1, pin_CMD_V1, pin_DEFAUT_V1, 240);

Batterie BC(pin_CMD_V1);
Batterie BF(pin_ONOFF_V1);

Temperature TE(pin_CMD_V1);
Temperature TH(pin_CMD_V1);

Pression P1(pin_P1);
Pression P2(pin_P2);
Pression P3(pin_P3);
//RM RM1(pin_CMD_RM1);

//ModbusIP object
ModbusIP mb;
byte mac[] = { 0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED };
byte ip[] = { 192, 168, 1, 55 };
byte mydns[] = { 8, 8, 8, 8 };
byte gw[] = { 192, 168, 1, 1 };
String adresseIP, passerelle;



unsigned long debutTempoModbus = 0;
unsigned long tempoModbus = 500;

void setModbus() {
	//Config Modbus IP
	mb.config(mac, ip, mydns, gw);
	mb.addHreg(100);	//modeMarche_Applique
	mb.addCoil(101);	//changeOver
	mb.addCoil(102);	//occupe
	mb.addHreg(103);	//debitMaxCTA
	mb.addHreg(104);	//seuilTempHorsGel
	mb.addHreg(105);	//pressionMaxCTA
	mb.addHreg(106);	//consignePressionModeOccupe
	mb.addHreg(107);	//consignePressionModeInoccupe
	mb.addHreg(108);	//BF.consigneTempEteInoccupe
	mb.addHreg(109);	//BF.consigneTempEteOccupe
	mb.addHreg(110);	//BC.consigneTempHiverOccupe
	mb.addHreg(111);	//BC.consigneTempHiverInoccupe
	mb.addCoil(112);	//regulTempReprise
	mb.addHreg(113);	//V1.consigneDebitFreeCooling
	mb.addHreg(114);	//V1.consigneDebitModeOccupe
	mb.addHreg(115);	//V1.consigneDebitModeInoccupe
	mb.addHreg(116);	//V1.consigne_app_m3h
	mb.addHreg(117);	//V1.Kp
	mb.addHreg(118);	//V1.Ki
	mb.addHreg(119);	//V1.Kd
	mb.addHreg(120);	//V1.Kfactor
	mb.addCoil(121);	//V1.marche
	mb.addHreg(122);	//V1.cons_app_pc
	mb.addCoil(123);	//V1.defaut
	mb.addHreg(124);	//V2.consigneDebitFreeCooling
	mb.addHreg(125);	//V2.consigneDebitModeOccupe
	mb.addHreg(126);	//V2.consigneDebitModeInoccupe
	mb.addHreg(127);	//V2.consigne_app_m3h
	mb.addHreg(128);	//V2.Kp 
	mb.addHreg(129);	//V2.Ki 
	mb.addHreg(130);	//V2.Kd 
	mb.addHreg(131);	//V2.Kfactor
	mb.addCoil(132);	//V2.marche
	mb.addHreg(133);	//V2.cons_app_pc
	mb.addCoil(134);	//V2.defaut
	mb.addHreg(135);	//BC.Kp
	mb.addHreg(136);	//BC.Ki
	mb.addHreg(137);	//BC.Kd
	mb.addCoil(138);	//BC.marche
	mb.addHreg(139);	//BF.Kp
	mb.addHreg(140);	//BF.Ki
	mb.addHreg(141);	//BF.Kd
	mb.addCoil(142);	//BF.marche
	mb.addHreg(143);	//BC.min
	mb.addHreg(144);	//BC.max
	mb.addHreg(145);	//BF.min
	mb.addHreg(146);	//BF.max
	mb.addHreg(147);	//jour
	mb.addHreg(148);	//mois
	mb.addHreg(149);	//annee
	mb.addHreg(150);	//minute
	mb.addHreg(151);	//heure
	mb.addHreg(152);	//jour de la semaine
	mb.addHreg(153);	//heureFinOccupe
	mb.addHreg(154);	//heureDebutOccupe
	mb.addHreg(155);	//temperature[REPRISE].value * 100
	mb.addHreg(156);	//temperature[EXTRACTION].value * 100
	mb.addHreg(157);	//temperature[AIRNEUF].value * 100
	mb.addHreg(158);	//temperature[SOUFFLAGE].value * 100
	mb.addHreg(159);	//P3.readAna()
	mb.addHreg(160);	//V1.debit
	mb.addHreg(161);	//V2.debit
	mb.addHreg(162);	//
	mb.addHreg(163);	//
	mb.addHreg(164);	//
	mb.addHreg(165);	//RegistreONOFF
	mb.addCoil(166);	//true quand ecriture
	mb.addCoil(167);	//
	mb.addCoil(168);	//
	mb.addCoil(169);	//
	mb.addCoil(170);	//registreOption
	mb.addCoil(171);	//commandeDeporteeOption
	mb.addCoil(172);	//GTCOption
	mb.addCoil(173);	//alarmeEncrassementFiltreOption
	mb.addCoil(174);	//regulSurDepressionOption
	mb.addCoil(175);	//ecran option
	mb.addCoil(176);	//boite a bouton option
	mb.addCoil(177);	//batterieChaudeOption
	mb.addCoil(178);	//batterieFroideOption
	mb.addCoil(180);	//V1.alarmePeriode
	mb.addCoil(181);	//V2.alarmePeriode
	mb.addCoil(182);	//V1.alarmeSurVentil
	mb.addCoil(183);	//V2.alarmeSurVentil
	mb.addCoil(184);	//V1.alarmePerteDeCharge
	mb.addCoil(185);	//V2.alarmePerteDeCharge
	mb.addHreg(186);	//alarmePeriode	
	mb.addHreg(187);	//alarmeSurVentil
	mb.addCoil(188);	//V1.commandeMarche
	mb.addCoil(189);	//V2.commandeMarche
	mb.addHreg(190);	//V1.jourChangementFiltre
	mb.addHreg(191);	//V1.moisChangementFiltre
	mb.addHreg(192);	//V1.anneeChangementFiltre
	mb.addHreg(193);	//V2.jourChangementFiltre
	mb.addHreg(194);	//V2.moisChangementFiltre
	mb.addHreg(195);	//V2.anneeChangementFiltre
	mb.addHreg(196);	//
	mb.addHreg(197);	//
	mb.addHreg(198);	//
	mb.addHreg(199);	//

}

void readFromGTC() {
	//GTCLive = (bool)mb.Coil(166);
	//Serial.print(GTCLive);
	//if (mb.Coil(166)== 0) return;
	//Serial.print("GTCLive");
	//Serial.print(mb.Coil(166));

	modeMarche_Applique = (modeMarche)mb.Hreg(100);
	changeOver = (bool)mb.Coil(101);
	occupe = (bool)mb.Coil(102);
	debitMaxCTA = mb.Hreg(103);
	seuilTempHorsGel = mb.Hreg(104);
	pressionMaxCTA = mb.Hreg(105);
	consignePressionModeOccupe = mb.Hreg(106);
	consignePressionModeInoccupe = mb.Hreg(107);
	TE.consigneTempEteInoccupe = mb.Hreg(108);
	TE.consigneTempEteOccupe = mb.Hreg(109);
	TH.consigneTempHiverOccupe = mb.Hreg(110);
	TH.consigneTempHiverInoccupe = mb.Hreg(111);
	regulTempReprise = (bool)mb.Coil(112);
	V1.consigneDebitFreeCooling = mb.Hreg(113);
	V1.consigneDebitModeOccupe = mb.Hreg(114);
	V1.consigneDebitModeInoccupe = mb.Hreg(115);
	V1.Kp = (double)(mb.Hreg(117)) / 100;
	V1.Ki = (double)(mb.Hreg(118)) / 100;
	V1.Kd = (double)(mb.Hreg(119)) / 100;
	V1.Kfactor = mb.Hreg(120);
	V1.marche = (bool)mb.Coil(121);
	V2.consigneDebitFreeCooling = mb.Hreg(124);
	V2.consigneDebitModeOccupe = mb.Hreg(125);
	V2.consigneDebitModeInoccupe = mb.Hreg(126);
	V2.Kp = (double)(mb.Hreg(128)) / 100;
	V2.Ki = (double)(mb.Hreg(129)) / 100;
	V2.Kd = (double)(mb.Hreg(130)) / 100;
	V2.Kfactor = mb.Hreg(131);
	V2.marche = (bool)mb.Coil(132);

	heureDebutOccupe = mb.Hreg(154);
	heureFinOccupe = mb.Hreg(153);
	registreOption = mb.Coil(170);
	commandeDeporteeOption = mb.Coil(171);
	GTCOption = mb.Coil(172);
	alarmeEncrassementFiltreOption = mb.Coil(173);
	regulSurDepressionOption = mb.Coil(174);
	bool commandemMemo = mb.Coil(175);
	if (commandemMemo == true) { boiteaboutonOption = true; }
	if (commandemMemo == false) { ecranOption = true; }
	batterieChaudeOption = mb.Coil(177);
	batterieFroideOption = mb.Coil(178);
	alarmePeriode = mb.Hreg(186);
	alarmeSurVentil = mb.Hreg(187);
	V1.commandeMarche = mb.Coil(188);
	V2.commandeMarche = mb.Coil(189);

	if (mb.Hreg(147) == 0)	return;
	heure = mb.Hreg(151);
	minute = mb.Hreg(150);
	annee = mb.Hreg(149);
	mois = mb.Hreg(148);
	jour = mb.Hreg(147);
	jourSemaine = mb.Hreg(152);
}


void writeToGTC_variables() {
	mb.Coil(166, true);
	mb.Hreg(116, V1.consigne_app_m3h);
	mb.Hreg(122, V1.cons_app_pc);
	mb.Coil(123, V1.defaut);
	mb.Hreg(127, V2.consigne_app_m3h);
	mb.Hreg(133, V2.cons_app_pc);
	mb.Coil(134, V2.defaut);
	mb.Coil(132, V2.marche);
	mb.Coil(121, V1.marche);
	mb.Coil(138, BC.marche);
	mb.Coil(142, BF.marche);

	
	mb.Hreg(155, (word)(temperature[REPRISE].value * 100));
	mb.Hreg(156, (word)(temperature[EXTRACTION].value * 100));
	mb.Hreg(157, (word)(temperature[AIRNEUF].value * 100));
	mb.Hreg(158, (word)(temperature[SOUFFLAGE].value * 100));
	mb.Hreg(159, P3.readAna());
	mb.Hreg(160, V1.debit);
	mb.Hreg(161, V2.debit);
	mb.Hreg(165, RegistreONOFF);
	mb.Coil(180, V1.alarmePeriode);
	mb.Coil(181, V2.alarmePeriode);
	mb.Coil(182, V1.alarmeSurVentil);
	mb.Coil(183, V2.alarmeSurVentil);
	mb.Coil(184, V1.alarmePerteDeCharge);
	mb.Coil(185, V2.alarmePerteDeCharge);
	/////////////////////////////////////////

}

void(*resetFunc) (void) = 0;

void writeToGTC_params() {

	mb.Hreg(100, modeMarche_Applique);
	mb.Coil(101, changeOver);
	mb.Coil(102, occupe);
	mb.Hreg(103, debitMaxCTA);
	mb.Hreg(104, seuilTempHorsGel);
	mb.Hreg(105, pressionMaxCTA);
	mb.Hreg(106, consignePressionModeOccupe);
	mb.Hreg(107, consignePressionModeInoccupe);
	mb.Hreg(108, TE.consigneTempEteInoccupe);
	mb.Hreg(109, TE.consigneTempEteOccupe);
	mb.Hreg(110, TH.consigneTempHiverOccupe);
	mb.Hreg(111, TH.consigneTempHiverInoccupe);
	mb.Coil(112, regulTempReprise);
	mb.Hreg(113, V1.consigneDebitFreeCooling);
	mb.Hreg(114, V1.consigneDebitModeOccupe);
	mb.Hreg(115, V1.consigneDebitModeInoccupe);
	mb.Hreg(117, (word)(V1.Kp * 100));
	mb.Hreg(118, (word)(V1.Ki * 100));
	mb.Hreg(119, (word)(V1.Kd * 100));
	mb.Hreg(120, V1.Kfactor);
	mb.Hreg(124, V2.consigneDebitFreeCooling);
	mb.Hreg(125, V2.consigneDebitModeOccupe);
	mb.Hreg(126, V2.consigneDebitModeInoccupe);
	mb.Hreg(128, (word)(V2.Kp * 100));
	mb.Hreg(129, (word)(V2.Ki * 100));
	mb.Hreg(130, (word)(V2.Kd * 100));
	mb.Hreg(131, V2.Kfactor);
	mb.Hreg(135, (word)(BC.Kp * 100));
	mb.Hreg(136, (word)(BC.Ki * 100));
	mb.Hreg(137, (word)(BC.Kd * 100));
	mb.Coil(138, BC.marche);
	mb.Hreg(139, (word)(BF.Kp * 100));
	mb.Hreg(140, (word)(BF.Ki * 100));
	mb.Hreg(141, (word)(BF.Kd * 100));
	mb.Coil(142, BF.marche);
	mb.Hreg(154, heureDebutOccupe);
	mb.Hreg(153, heureFinOccupe);
	mb.Coil(170, registreOption);
	mb.Coil(171, commandeDeporteeOption);
	mb.Coil(172, GTCOption);
	mb.Coil(173, alarmeEncrassementFiltreOption);
	mb.Coil(174, regulSurDepressionOption);
	mb.Coil(175, ecranOption);
	mb.Coil(176, boiteaboutonOption);
	mb.Coil(177, batterieChaudeOption);
	mb.Coil(178, batterieFroideOption);
	mb.Hreg(186, alarmePeriode);
	mb.Hreg(187, alarmeSurVentil);
	mb.Coil(188, V1.commandeMarche);
	mb.Coil(189, V2.commandeMarche);
	mb.Hreg(190, V1.jourChangementFiltre);
	mb.Hreg(191, V1.moisChangementFiltre);
	mb.Hreg(192, V1.anneeChangementFiltre);
	mb.Hreg(193, V2.jourChangementFiltre);
	mb.Hreg(194, V2.moisChangementFiltre);
	mb.Hreg(195, V2.anneeChangementFiltre);
		
}

void modbus() {
	mb.task();
	if (millis() - debutTempoModbus >= tempoModbus) {
		debutTempoModbus = millis();
		writeToGTC_variables();
		readFromGTC();
		saveCTA(0);
	}
}

void loadCTA(int address) {
	SOUFFLAGE = EEPROM.readInt(address); address += sizeof(int);
	REPRISE = EEPROM.readInt(address); address += sizeof(int);
	EXTRACTION = EEPROM.readInt(address); address += sizeof(int);
	AIRNEUF = EEPROM.readInt(address); address += sizeof(int);
	seuilTempHorsGel = EEPROM.readDouble(address); address += sizeof(double);
	modeHE_Applique = (modeHE)EEPROM.readInt(address); address += sizeof(int);
	modeMarche_Applique = (modeMarche)EEPROM.readInt(address); address += sizeof(int);
	modeMarcheAutoOcc = (modeMarcheAuto)EEPROM.readInt(address); address += sizeof(int);
	modeMarcheAutoInocc = (modeMarcheAuto)EEPROM.readInt(address); address += sizeof(int);
	changeOver = EEPROM.readInt(address); address += sizeof(int);
	registreOption = EEPROM.readInt(address); address += sizeof(int);
	boiteaboutonOption = EEPROM.readInt(address); address += sizeof(int);
	ecranOption = EEPROM.readInt(address); address += sizeof(int);
	commandeDeporteeOption = EEPROM.readInt(address); address += sizeof(int);
	regulSurDepressionOption = EEPROM.readInt(address); address += sizeof(int);
	GTCOption = EEPROM.readInt(address); address += sizeof(int);
	batterieChaudeOption = EEPROM.readInt(address); address += sizeof(int);
	batterieFroideOption = EEPROM.readInt(address); address += sizeof(int);
	alarmeEncrassementFiltreOption = EEPROM.readInt(address); address += sizeof(int);
	alarmePeriode = EEPROM.readInt(address); address += sizeof(int);
	alarmeSurVentil = EEPROM.readInt(address); address += sizeof(int);
	debitMaxCTA = EEPROM.readDouble(address); address += sizeof(double);
	pressionMaxCTA = EEPROM.readDouble(address); address += sizeof(double);
	consignePressionModeOccupe = EEPROM.readInt(address); address += sizeof(int);
	consignePressionModeInoccupe = EEPROM.readInt(address); address += sizeof(int);
	heureDebutOccupe = EEPROM.readInt(address); address += sizeof(int);
	heureFinOccupe = EEPROM.readInt(address); address += sizeof(int);
	for (int i = 0; i < 7; i++) {
		jourOccupe[i] = EEPROM.readBit(address, i);
	}
	address += sizeof(int);
	V1.startAddress = address;
	address += V1.addressSize;
	V2.startAddress = address;
	address += V2.addressSize;
	V1.loadFromEEPROM();
	V2.loadFromEEPROM();
	for (int i = 0; i < 4; i++) {
		ip[i] = EEPROM.readByte(address); address += sizeof(byte);
		jourOccupe_int = EEPROM.readByte(address);
	}
	for (int i = 0; i < 4; i++) {
		gw[i] = EEPROM.readByte(address); address += sizeof(byte);

	}
	IPToString();
	tempoModbus = EEPROM.readInt(address); address += sizeof(int);
}

void saveCTA(int address) {
	writeToGTC_params();
	EEPROM.updateInt(address, SOUFFLAGE); address += sizeof(int);
	EEPROM.updateInt(address, REPRISE); address += sizeof(int);
	EEPROM.updateInt(address, EXTRACTION); address += sizeof(int);
	EEPROM.updateInt(address, AIRNEUF); address += sizeof(int);
	EEPROM.updateDouble(address, seuilTempHorsGel); address += sizeof(double);
	EEPROM.updateInt(address, modeHE_Applique); address += sizeof(int);
	EEPROM.updateInt(address, modeMarche_Applique); address += sizeof(int);
	EEPROM.updateInt(address, modeMarcheAutoOcc); address += sizeof(int);
	EEPROM.updateInt(address, modeMarcheAutoInocc); address += sizeof(int);
	EEPROM.updateInt(address, changeOver); address += sizeof(int);
	EEPROM.updateInt(address, registreOption); address += sizeof(int);
	EEPROM.updateInt(address, boiteaboutonOption); address += sizeof(int);
	EEPROM.updateInt(address, ecranOption); address += sizeof(int);
	EEPROM.updateInt(address, commandeDeporteeOption); address += sizeof(int);
	EEPROM.updateInt(address, regulSurDepressionOption); address += sizeof(int);
	EEPROM.updateInt(address, GTCOption); address += sizeof(int);
	EEPROM.updateInt(address, batterieChaudeOption); address += sizeof(int);
	EEPROM.updateInt(address, batterieFroideOption); address += sizeof(int);
	EEPROM.updateInt(address, alarmeEncrassementFiltreOption); address += sizeof(int);
	EEPROM.updateInt(address, alarmePeriode); address += sizeof(int);
	EEPROM.updateInt(address, alarmeSurVentil); address += sizeof(int);
	EEPROM.updateDouble(address, debitMaxCTA); address += sizeof(double);
	EEPROM.updateDouble(address, pressionMaxCTA); address += sizeof(double);
	EEPROM.updateInt(address, consignePressionModeOccupe); address += sizeof(int);
	EEPROM.updateInt(address, consignePressionModeInoccupe); address += sizeof(int);
	EEPROM.updateInt(address, heureDebutOccupe); address += sizeof(int);
	EEPROM.updateInt(address, heureFinOccupe); address += sizeof(int);
	
	for (int i = 0; i < 7; i++) {
		EEPROM.updateBit(address, i ,jourOccupe[i]);
		//Serial.println(jourOccupe[i]);
	}
	address += sizeof(int);
	address += V1.addressSize;
	address += V2.addressSize;
	V1.saveToEEPROM();
	V2.saveToEEPROM();
	for (int i = 0; i < 4; i++) {
		EEPROM.updateByte(address, ip[i]); address += sizeof(byte);
	}
	for (int i = 0; i < 4; i++) {
		EEPROM.updateByte(address, gw[i]); address += sizeof(byte);
	}
	EEPROM.updateInt(address, tempoModbus); address += sizeof(int);

}

void initCTA() {

	loadCTA(0);
	V2.myPID.SetMode(AUTOMATIC);
	V2.myPID.SetOutputLimits(0, 255);
	V2.myPID.SetTunings(V2.Kp, V2.Ki, V2.Kd);
	V1.myPID.SetMode(AUTOMATIC);
	V1.myPID.SetOutputLimits(0, 255);
	V1.myPID.SetTunings(V1.Kp, V1.Ki, V1.Kd);
	V2.myPIDPression.SetMode(AUTOMATIC);
	V2.myPIDPression.SetOutputLimits(0, 255);
	V2.myPIDPression.SetTunings(V2.Kp, V2.Ki, V2.Kd);
	V1.myPIDPression.SetMode(AUTOMATIC);
	V1.myPIDPression.SetOutputLimits(0, 255);
	V1.myPIDPression.SetTunings(V1.Kp, V1.Ki, V1.Kd);
	BC.myPID.SetMode(AUTOMATIC);
	BC.myPID.SetOutputLimits(0, 255);
	BC.myPID.SetTunings(BC.Kp, BC.Ki, BC.Kd);
	BF.myPID.SetMode(AUTOMATIC);
	BF.myPID.SetOutputLimits(0, 255);
	BF.myPID.SetTunings(BF.Kp, BF.Ki, BF.Kd); 
	TH.myPID.SetTunings(TH.Kp, TH.Ki, TH.Kd);
	TE.myPID.SetTunings(TE.Kp, TE.Ki, TE.Kd);

}


uint32_t timeStamp(uint8_t a, uint8_t m, uint8_t j, uint8_t hh, uint8_t mm, uint8_t ss)
{
	uint32_t nbjours;

	nbjours = NB_JOURS_ANNEES_PRECEDENTES(a);

	switch (m)
	{
	case  2: nbjours += 31; break;
	case  3: nbjours += 59; break;
	case  4: nbjours += 90; break;
	case  5: nbjours += 120; break;
	case  6: nbjours += 151; break;
	case  7: nbjours += 181; break;
	case  8: nbjours += 212; break;
	case  9: nbjours += 243; break;
	case 10: nbjours += 273; break;
	case 11: nbjours += 304; break;
	case 12: nbjours += 334; break;
	}

	if ((m < 3) || PAS_BISSEXTILE(a))
	{
		nbjours += j - 1;
	}
	else // le 29/02 est passé
	{
		nbjours += j;
	}
	unsigned long Ts = ss + (60 * (mm + (60 * (hh + (24 * nbjours)))));
	return  Ts;
}

void decomposerTimestamp(uint32_t ts, uint8_t *a, uint8_t *m, uint8_t *j, uint8_t *hh, uint8_t *mm, uint8_t *ss)
{
	uint32_t li;
	uint16_t deb, fin;
	uint8_t i;

	li = ts / 86400;
	*a = (li - ((li + 1401) / 1461)) / 365;

	li = li + 1 - NB_JOURS_ANNEES_PRECEDENTES(*a);
	fin = 0;
	for (i = 1; i <= 12; i++)
	{
		deb = fin;
		switch (i)
		{
		case  2:
			fin += 28 + (!PAS_BISSEXTILE(*a));
			break;
		case  4:
		case  6:
		case  9:
		case 11:
			fin += 30;
			break;
		default:
			fin += 31;
			break;
		}
		if (fin >= li)
		{
			*m = i;
			*j = li - deb;
			i = 12;
		}
	}

	li = ts % 86400;
	*hh = li / 3600;
	li = li % 3600;
	*mm = li / 60;
	*ss = li % 60;
}

void RTCmiseajour() {

	if (modifdate == true) {
		Controllino_RTC_init(0);
		Controllino_SetTimeDate(jour, jourSemaine, mois, annee, heure, minute, 12);
		delay(10);
		modifdate = false;
		return;
	}
	jour = Controllino_GetDay();
	jourSemaine = Controllino_GetWeekDay();
	minute = Controllino_GetMinute();
	heure = Controllino_GetHour();
	mois = Controllino_GetMonth();
	annee = Controllino_GetYear();


}

void setup() {
	EEPROM.setMemPool(memoryBase, EEPROMSizeNano); //Set memorypool base to 32, assume Arduino Uno board
	pinMode(pin_TEMPERATURE, INPUT);
	Serial.begin(9600);
	Serial1.begin(9600);
	Serial2.begin(9600);
	Serial.println("START1");
	sensors.begin();
	Serial.println("INIT CTA");
	initCTA();
	Serial.println("RTC INIT");
	Controllino_RTC_init(0);
	Serial.println("START SCREEN");
	startScreen();
	form = MENU;
	/*if (ecranOption == true) {
		Serial.println("START SCREEN 2");
		startScreen2();
	}
	if (commandeDeporteeOption == false) form = MENU;
	else form = MENU_CMD;*/
	Serial.println("WRITE FORM");
	genie.WriteObject(GENIE_OBJ_FORM, form, 0);
	//genie2.WriteObject(GENIE_OBJ_FORM, form, 0);
	RegistreONOFF = false;
	Serial.println("START");

	//Serial.print("device count:");
	//Serial.println(sensors.getDeviceCount());
	getDS18B20Addresses(4);
	delay(200);

	setModbus();
	delay(1000);
	writeToGTC_params();
}


void setModeHE() {
	if (changeOver) {
		if (occupe) modeHE_Applique = ETE_OCC;
		else modeHE_Applique = ETE_INOCC;
	}
	else {
		if (occupe) modeHE_Applique = HIVER_OCC;
		else modeHE_Applique = HIVER_INOCC;
	}
}

void setOccupation() {
	int heurecourante = heure * 100 + minute;
	if (jourOccupe[jourSemaine - 1]) {
		if (heurecourante >= heureDebutOccupe && heurecourante <= heureFinOccupe) {
			occupe = true;
			return;
		}
	}
	occupe = false;
}

void setMode() {
	if (temperature[AIRNEUF].value < seuilTempHorsGel) {
		modeApplique = HORSGEL;
		//RegistreONOFF = true;  //Ouverture Registre
		return;
	}
	if (modeHE_Applique == HIVER_OCC || modeHE_Applique == HIVER_INOCC) {
		if (temperature[AIRNEUF].value >= TH.consigneTempHiverOccupe) { //MODE HIVER
			modeApplique = FREEHEATING;
			RegistreONOFF = true;  //Ouverture Registre
			return;
		}
	}
	if (modeHE_Applique == ETE_OCC || modeHE_Applique == ETE_INOCC) {
		if (temperature[AIRNEUF].value <= TE.consigneTempEteOccupe) { //MODE ETE
			modeApplique = FREECOOLING;
			RegistreONOFF = true;  //Ouverture Registre
			return;
		}
	}

	modeApplique = NORMAL;
	RegistreONOFF = false;
	if (RegistreONOFF == false && Registreforcage == true) RegistreONOFF = true;
	return;
}
void regulTemperature() {
	if (!changeOver) {
		switch (modeMarche_Applique)
		{
		case 0: //ARRET
			BC.marche = false;
			break;
		case 1://MANU
			BC.marche = BC.commandeMarche;
			BC.consigne_Temp = TH.consigneTempHiverOccupe;
			break;
		case 2://AUTO
			if (modeApplique == FREECOOLING || modeApplique == FREEHEATING) {
				BC.marche = false;
				//break;
			}
			else {
				BC.marche = true;
				if (!occupe) {// INOCCUPE
					TH.consigne_app_Temp_amb = TH.consigneTempHiverInoccupe;

				}
				else { //OCCUPE
					TH.consigne_app_Temp_amb = TH.consigneTempHiverOccupe;

				}
				TH.recupTemp(temperature[SOUFFLAGE].value);
				TH.myPID.Compute();
				BC.cons_app_temp_souff = TH.cons_app_temp_souff;
			}
		}

		//BC.calculTemp(temperature[SOUFFLAGE]);

		if (BC.marche) {
			BC.cons_app_temp_souff = BC.consigne_Temp;
			BC.myPID.Compute();
		}
		else {
			BC.cons_app_temp_souff = -1000;
			BC.cons_app_pts = 0;

		}
		BC.update();
	}
	else {
		switch (modeMarche_Applique)
		{
		case 0: //ARRET
			BF.marche = false;
			break;
		case 1://MANU
			BF.marche = BF.commandeMarche;
			BF.consigne_Temp = TE.consigneTempHiverOccupe;
			break;
		case 2://AUTO
			if (modeApplique == FREECOOLING || modeApplique == FREEHEATING) {
				BF.marche = false;
				//break;
			}
			else {
				BC.marche = true;
				if (!occupe) {// INOCCUPE
					TE.consigne_app_Temp_amb = TE.consigneTempHiverInoccupe;

				}
				else { //OCCUPE
					TE.consigne_app_Temp_amb = TE.consigneTempHiverOccupe;

				}
				TE.recupTemp(temperature[SOUFFLAGE].value);
				TE.myPID.Compute();
				BF.cons_app_temp_souff = TE.cons_app_temp_souff;
			}
		}

		//BF.calculTemp(temperature[SOUFFLAGE]);

		if (BF.marche) {
			BC.cons_app_temp_souff = BF.consigne_Temp;
			BF.myPID.Compute();
		}
		else {
			BF.cons_app_temp_souff = -1000;
			BF.cons_app_pts = 0;

		}
		BF.update();
	}
}
void regulDebit() {

	switch (modeMarche_Applique)
	{
	case 0: //ARRET
		V1.marche = false;
		V2.marche = false;
		break;
	case 1://MANU
		V1.marche = V1.commandeMarche;
		V2.marche = V2.commandeMarche;
		V1.consigne_m3h = V1.consigneDebitModeOccupe;
		V2.consigne_m3h = V2.consigneDebitModeOccupe;

		break;
	case 2: // AUTO
		V1.marche = true;
		V2.marche = true;
		if (modeApplique == FREECOOLING || modeApplique == FREEHEATING) { //FREECOOLING ou FREEHEATING
			V1.consigne_m3h = V1.consigneDebitFreeCooling;
			V2.consigne_m3h = V2.consigneDebitFreeCooling;
		}
		else {
			
			if (!occupe) {
				if (modeMarcheAutoInocc == REGULDEBIT) {
					V1.consigne_m3h = V1.consignePressionModeInoccupe;
					V2.consigne_m3h = V2.consignePressionModeInoccupe;
				}
				else if (modeMarcheAutoInocc == REGULDEPRESSION) {
					V2.consigne_app_Pa = consignePressionModeInoccupe;
					V1.consigne_m3h = V1.returnCalculDebit(P2.read());
				}
				else if (boiteaboutonOption == true) {
					if (bpmarche == 1) {
						V1.marche = true;
						V2.marche = true;
						consigne_m3h = map(analogRead(POTAR), 0, 850, 0, debitMaxCTA);
						V2.consigne_m3h = consigne_m3h;
						V1.consigne_m3h = consigne_m3h;
						bpmarche = 1;
					}
					else {
						bpmarche = 0;
						V1.marche = false;
						V2.marche = false;
						consigne_m3h = map(analogRead(POTAR), 0, 850, 0, debitMaxCTA);
					}
					digitalWrite(VOYANT_DEFAUT, Ventilodefaut);
					digitalWrite(VOYANT_MARCHE, bpmarche);
				}
				else {
					if (bpmarche == 1) {
						V2.consigne_m3h = debitMaxCTA / 100 * (slideConsigne_m3h);
						V1.consigne_m3h = debitMaxCTA / 100 * (slideConsigne_m3h);
						V1.marche = true;
						V2.marche = true;
						bpmarche = 1;
					}
					else {
						V2.consigne_m3h = debitMaxCTA / 100 * (slideConsigne_m3h);
						V1.consigne_m3h = debitMaxCTA / 100 * (slideConsigne_m3h);
						V1.marche = false;
						V2.marche = false;
						bpmarche = 0;
					}
				}
			}
			else { //occupe
				if (modeMarcheAutoOcc == REGULDEBIT) {
					V1.consigne_m3h = V1.consigneDebitModeOccupe;
					V2.consigne_m3h = V2.consigneDebitModeOccupe;
				}
				else if (modeMarcheAutoOcc == REGULDEPRESSION) {
					V2.consigne_app_Pa = consignePressionModeOccupe;
					V1.consigne_m3h = V1.returnCalculDebit(P2.read());
				}
				else if (boiteaboutonOption == true) {
					if (bpmarche == 1) {
						V1.marche = true;
						V2.marche = true;
						consigne_m3h = map(analogRead(POTAR), 0, 850, 0, debitMaxCTA);
						V2.consigne_m3h = consigne_m3h;
						V1.consigne_m3h = consigne_m3h;
						bpmarche = 1;
					}
					else {
						bpmarche = 0;
						V1.marche = false;
						V2.marche = false;
						consigne_m3h = map(analogRead(POTAR), 0, 850, 0, debitMaxCTA);
					}
					digitalWrite(VOYANT_DEFAUT, Ventilodefaut);
					digitalWrite(VOYANT_MARCHE, bpmarche);
				}
				else {
					if (bpmarche == 1) {
						V2.consigne_m3h = debitMaxCTA / 100 * (slideConsigne_m3h);
						V1.consigne_m3h = debitMaxCTA / 100 * (slideConsigne_m3h);
						V1.marche = true;
						V2.marche = true;
						bpmarche = 1;
					}
					else {
						V2.consigne_m3h = debitMaxCTA / 100 * (slideConsigne_m3h);
						V1.consigne_m3h = debitMaxCTA / 100 * (slideConsigne_m3h);
						V1.marche = false;
						V2.marche = false;
						bpmarche = 0;
					}
				}
			}
		}
	}
	digitalWrite(pin_ONOFF_V1, V1.marche);
	digitalWrite(pin_ONOFF_V2, V2.marche);
	if ((modeMarche_Applique == AUTO && modeMarcheAutoOcc == REGULDEPRESSION && occupe) || (modeMarche_Applique == AUTO && modeMarcheAutoInocc == REGULDEPRESSION && !occupe)) {
		V2.calculDebit(P2.read());
		V1.calculDebit(P1.read());
		V2.calculPression(P3.readAna());
		
		if (V2.marche) {
			V2.myPIDPression.Compute();
		}
		else {
			V2.consigne_app_Pa = -1000;
			V2.cons_app_pts = 0;

		}
		V2.update();
		
	}
	else {
		V1.calculDebit(P1.read());
		V2.calculDebit(P2.read());

		if (V2.marche) {
			V2.consigne_app_m3h = V2.consigne_m3h;
			V2.myPID.Compute();
		}
		else {
			V2.consigne_app_m3h = -1000;
			V2.cons_app_pts = 0;

		}
		V2.update();
	}

	if (V1.marche) {
		V1.consigne_app_m3h = V1.consigne_m3h;
		V1.myPID.Compute();
	}
	else {
		V1.consigne_app_m3h = -1000;
		V1.cons_app_pts = 0;
	}
	V1.update();

	
	if (V1.marche || V2.marche) {
		digitalWrite(VOYANT_MARCHE, true);
	}
	

}
void ModeBoutons(void) {

	if (analogRead(BP_ARRET) > 100 || ecranbparret == true) {
		bpmarche = false;
		bparret = true;
		ecranbparret = false;
		return;
	}
	else if (ecranbpmarche == true || analogRead(BP_MARCHE) > 100) {
		bpmarche = true;
		bparret = false;
		ecranbpmarche = false;
		return;
	}
	else if (bpmarche == true) {
		bpmarche = true;
		bparret = false;
		return;
	}
	else if (bparret == true) {
		bpmarche = false;
		bparret = true;
		return;
	}
}

void loop() {
	genie.DoEvents(); // Gestion des evenements de l'ecran		  
	regulDebit();
	if (batterieChaudeOption == true || batterieFroideOption == true) regulTemperature();
	//Serial.println((P3.readAna()));
	
	if (GTCOption == true) modbus();
	if (commandeDeporteeOption == true) {
		if (boiteaboutonOption == true) ModeBoutons();
		if (ecranOption == true) genie2.DoEvents(); // Gestion des evenements de l'ecran		  
	}
	if (registreOption == true) digitalWrite(pin_CMD_RM1, (int)RegistreONOFF);

	//Serial.print("changeover=");
	//Serial.print(changeOver);
	//Serial.print("\r\ndebit=");
	//Serial.print(V1.debit);
	//Serial.print("\r\ndebit=");

	if (millis() >= waitPeriod)
	{
		setModeHE();
		checkAlarme();
		writeScreen();
		setOccupation();
		if (commandeDeporteeOption == true) {
			if (ecranOption == true) writeScreen2(); // Gestion des evenements de l'ecran		  
		}
		if (registreOption == true) setMode();
		waitPeriod = millis() + 1000;
	}
	if (millis() >= waitPeriodTemps)
	{
		readMesures();
		waitPeriodTemps = millis() + 10000;
	}
	if (millis() >= waitPeriodDate)
	{
		RTCmiseajour();

		waitPeriodDate = millis() + 30000;
	}
}
void checkAlarme() {
	V1.defaut = !digitalRead(pin_DEFAUT_V1);
	V2.defaut = !digitalRead(pin_DEFAUT_V2);			// enlever les ! si contacte NO
	if (V1.defaut || V2.defaut) {
		Ventilodefaut = true;
		digitalWrite(VOYANT_DEFAUT, Ventilodefaut);
	}
	else {
		Ventilodefaut = false;
	}
	if (V1.cons_app_pc > alarmeSurVentil &&  V1.memoCheckAlarmeSurVentil == true) {
		V1.alarmeSurVentil = true;
	}
	else V1.alarmeSurVentil = false;
	if (V2.cons_app_pc > alarmeSurVentil && V2.memoCheckAlarmeSurVentil == true) {
		V2.alarmeSurVentil = true;
	}
	else V2.alarmeSurVentil = false;

	unsigned long alarmePeriodememo = timeStamp(0, alarmePeriode, 0, 0, 0, 0);

	unsigned long now = timeStamp(Controllino_GetYear(), Controllino_GetMonth(), Controllino_GetDay(), Controllino_GetHour(), Controllino_GetMinute(), Controllino_GetSecond());
	if (V1.changementFiltre + alarmePeriodememo  < now) {
		V1.alarmePeriode = true;
	}
	else V1.alarmePeriode = false;

	if (V2.changementFiltre + alarmePeriodememo < now) {
		V2.alarmePeriode = true;
	}
	else V2.alarmePeriode = false;
	if (V1.cons_app_pc > alarmeSurVentil) {
		V2.memoCheckAlarmeSurVentil = true;
	}
	else V2.memoCheckAlarmeSurVentil = false;
	if (V1.cons_app_pc > alarmeSurVentil) {
		V1.memoCheckAlarmeSurVentil = true;
	}
	else V1.memoCheckAlarmeSurVentil = false;
	if (alarmeEncrassementFiltreOption == true) {
		V1.alarmePerteDeCharge = digitalRead(PRESSOSTAT_P1);
		V2.alarmePerteDeCharge = digitalRead(PRESSOSTAT_P2);
	}
}

void myGenieEventHandler(void)
{

	//Serial.println("ok");
	int keyboardValue;
	genieFrame Event;
	genie.DequeueEvent(&Event); // Remove the next queued event from the buffer, and process it below

								//If the cmd received is from a Reported Event (Events triggered from the Events tab of Workshop4 objects)
	if (Event.reportObject.cmd == GENIE_REPORT_EVENT)
	{
		waitPeriod = 0;
		if (Event.reportObject.object == GENIE_OBJ_4DBUTTON) // If this event is from a 4DButton
		{
			switch (Event.reportObject.index) {
			case 0: //MARCHEV1
				if (Event.reportObject.data_lsb == 0) V1.commandeMarche = false;
				else V1.commandeMarche = true;
				break;
			case 5: //MARCHEV2
				if (Event.reportObject.data_lsb == 0) V2.commandeMarche = false;
				else V2.commandeMarche = true;
				break;
			case 2: //FORCAGE BYPASS
				if (Event.reportObject.data_lsb == 0) Registreforcage = false;
				else Registreforcage = true;
				break;
			case 3: //CHANGEOVER
				if (Event.reportObject.data_lsb == 0) changeOver = false;
				else changeOver = true;
				break;
			case 4: //MARCHEV1
				ecranbparret = true;
				break;
			case 1: //MARCHEV1
				ecranbpmarche = true;
				break;
			case 6: //CHANGEOVER
				if (Event.reportObject.data_lsb == 0) registreOption = false;
				else registreOption = true;
				break;
			case 7: //CHANGEOVER
				if (Event.reportObject.data_lsb == 0) commandeDeporteeOption = false;
				else commandeDeporteeOption = true;
				break;
			case 8: //CHANGEOVER
				if (Event.reportObject.data_lsb == 0) GTCOption = false;
				else GTCOption = true;
				break;
			case 9: //CHANGEOVER
				if (Event.reportObject.data_lsb == 0) alarmeEncrassementFiltreOption = false;
				else alarmeEncrassementFiltreOption = true;
				break;
			case 10: //CHANGEOVER
				if (Event.reportObject.data_lsb == 0) regulSurDepressionOption = false;
				else regulSurDepressionOption = true;
				break;
			case 11: //CHANGEOVER
				if (Event.reportObject.data_lsb == 0) { boiteaboutonOption = true; ecranOption = false; }
				else { boiteaboutonOption = false; ecranOption = true; }
				break;
			case 12: //CHANGEOVER
				if (Event.reportObject.data_lsb == 0) { batterieChaudeOption = false; }
				else { batterieChaudeOption = true; }
				break;
			case 13: //CHANGEOVER
				if (Event.reportObject.data_lsb == 0) { batterieFroideOption = false; }
				else { batterieFroideOption = true; }
				break;
			case 14: //CHANGEOVER
				if (Event.reportObject.data_lsb == 0) { BC.commandeMarche = false; }
				else { BC.commandeMarche = true; }
				break;
			case 15: //CHANGEOVER
				if (Event.reportObject.data_lsb == 0) { BF.commandeMarche = false; }
				else { BF.commandeMarche = true; }
				break;
			}
			changedParams = true;
			saveCTA(0);
		}
		if (Event.reportObject.object == GENIE_OBJ_DIPSW) // If this event is from a DIPSWITCH
		{
			switch (Event.reportObject.index) {
			case 0: //MODE MARCHE
				if (Event.reportObject.data_lsb == 0) modeMarche_Applique = modeMarche::ARRET;
				else if (Event.reportObject.data_lsb == 1) modeMarche_Applique = modeMarche::MANU;
				else if (Event.reportObject.data_lsb == 2) modeMarche_Applique = modeMarche::AUTO;
				break;
			case 1: //GESTION MODE OCCUPE
				if (Event.reportObject.data_lsb == 0) jourOccupe[0] = false;
				else jourOccupe[0] = true;
				break;
			case 2:
				if (Event.reportObject.data_lsb == 0) jourOccupe[1] = false;
				else jourOccupe[1] = true;
				break;
			case 3:
				if (Event.reportObject.data_lsb == 0) jourOccupe[2] = false;
				else jourOccupe[2] = true;
				break;
			case 4:
				if (Event.reportObject.data_lsb == 0) jourOccupe[3] = false;
				else jourOccupe[3] = true;
				break;
			case 5:
				if (Event.reportObject.data_lsb == 0) jourOccupe[4] = false;
				else jourOccupe[4] = true;
				break;
			case 6:
				if (Event.reportObject.data_lsb == 0) jourOccupe[5] = false;
				else jourOccupe[5] = true;
				break;
			case 7:
				if (Event.reportObject.data_lsb == 0) jourOccupe[6] = false;
				else jourOccupe[6] = true;
				break;
			case 8:
				if (Event.reportObject.data_lsb == 0) modeMarcheAutoOcc = modeMarcheAuto::REGULDEBIT;
				else if (Event.reportObject.data_lsb == 1 && regulSurDepressionOption == true) modeMarcheAutoOcc = modeMarcheAuto::REGULDEPRESSION;
				else if (Event.reportObject.data_lsb == 2 && commandeDeporteeOption == true) modeMarcheAutoOcc = modeMarcheAuto::CMDDEPORTEE;
				break;
			case 9:
				if (Event.reportObject.data_lsb == 0) modeMarcheAutoInocc = modeMarcheAuto::REGULDEBIT;
				else if (Event.reportObject.data_lsb == 1 && regulSurDepressionOption == true) modeMarcheAutoInocc = modeMarcheAuto::REGULDEPRESSION;
				else if (Event.reportObject.data_lsb == 2 && commandeDeporteeOption == true) modeMarcheAutoInocc = modeMarcheAuto::CMDDEPORTEE;
				break;
			}
			changedParams = true;
			for (int i = 0; i < 7; i++) {
				bitWrite(jourOccupe_int, i, jourOccupe[i]);
			}
		}
		if (Event.reportObject.object == GENIE_OBJ_SLIDER) // If this event is from a SLIDER
		{
			switch (Event.reportObject.index) {
			case 0:
				slideConsigne_m3h = Event.reportObject.data_lsb;
				break;
			}
		}
		if (Event.reportObject.object == GENIE_OBJ_WINBUTTON) // If this event is from a WinButton
		{
			//Serial.println("EVENT");
			//Serial.print(Event.reportObject.object);//////////////////////////////
			str = "";
			switch (Event.reportObject.index) {
			case 5: //FORM MENUADMIN ==>Clavier pour mode admin
			case 65: form = FORM::MENUADMIN_FULL;
				param = "mot de passe admin";
				genie.WriteObject(GENIE_OBJ_FORM, FORM::KEYBOARD, 0);
				break;
			case 20: //FORM MENUADMIN ==>Retour
			case 30: //FORM MENUADMIN ==>Retour
			case 19: //FORM MENUADMIN ==>Retour
			case 29: //FORM MENUADMIN ==>Retour
			case 88: //FORM MENUADMIN ==>Retour
			case 70: //FORM MENUADMIN ==>Retour
			case 66: //FORM MENUADMIN ==>Retour
				 form = FORM::MENUADMIN_FULL;
				genie.WriteObject(GENIE_OBJ_FORM, form, 0);
				break;
			case 0: //FORM Boite a boutons 
				if (boiteaboutonOption == true) form = FORM::BOUTONS;
				if (ecranOption == true) form = FORM::ECRAN;
				genie.WriteObject(GENIE_OBJ_FORM, form, 0);
				break;
			case 2: //FORM SYNOPTIQUE
			case 59:
				form = FORM::SYNOPTIQUE;
				waitPeriodTemps = 0;
				genie.WriteObject(GENIE_OBJ_FORM, form, 0);
				break;
			case 4: //FORM CONFIG CTA
			case 58: //FORM CONFIG CTA
				form = FORM::CONFIG_CTA;
				genie.WriteObject(GENIE_OBJ_FORM, form, 0);
				break;
			case 16: //FORM Params Ventilateurs
				form = FORM::PARAMS_VENTILO;
				genie.WriteObject(GENIE_OBJ_FORM, form, 0);
				break;
			case 17: //FORM Params Registre
				if (!registreOption) form = FORM::OPTION_OFF;
				else form = FORM::PARAMS_RM;
				genie.WriteObject(GENIE_OBJ_FORM, form, 0);
				break;
			case 21: //modif de la date et l'heure
			case 67:
				form = FORM::DATE_ET_HEURE;
				genie.WriteObject(GENIE_OBJ_FORM, form, 0);
				break;
			case 3: //FORM Params CTA
			case 46: //FORM Params CTA
			case 60:
				form = FORM::PARAMS_CTA;
				genie.WriteObject(GENIE_OBJ_FORM, form, 0);
				break;
			case 52: //FORM Params CTA2
			case 40: //FORM Params CTA2
				form = FORM::PARAMS_CTA2;
				genie.WriteObject(GENIE_OBJ_FORM, form, 0);
				break;
			case 47: //FORM Params CTA3
				form = FORM::PARAMS_CTA3;
				genie.WriteObject(GENIE_OBJ_FORM, form, 0);
				break;
			case 57: //FORM Params RESEAU
				if(GTCOption == false)form = FORM::OPTION_OFF;
				else form = FORM::PARAMS_RESEAU;
				genie.WriteObject(GENIE_OBJ_FORM, form, 0);
				break;
			case 69: //FORM Options CTA
			case 68:
				form = FORM::OPTIONS_CTA;
				genie.WriteObject(GENIE_OBJ_FORM, form, 0);
				break;
			case 83:
				if (!batterieChaudeOption && !batterieFroideOption) form = FORM::OPTION_OFF;
				else form = FORM::BATTERIES;
				genie.WriteObject(GENIE_OBJ_FORM, form, 0);
				break;
			case 78:
				bpmarche = false;
				break;
			case 79:
				bpmarche = true;
				break;
			case 90:
				form = FORM::FILTRE;
				genie.WriteObject(GENIE_OBJ_FORM, form, 0);
				break;
			case 91:
				V1.secondeChangementFiltre = Controllino_GetSecond();
				V1.minuteChangementFiltre = Controllino_GetMinute();
				V1.heureChangementFiltre = Controllino_GetHour();
				V1.jourChangementFiltre = Controllino_GetDay();
				V1.moisChangementFiltre = Controllino_GetMonth();
				V1.anneeChangementFiltre = Controllino_GetYear();
				V1.changementFiltre = timeStamp(Controllino_GetYear(), Controllino_GetMonth(), Controllino_GetDay(), Controllino_GetHour(), Controllino_GetMinute(), Controllino_GetSecond());
				saveCTA(0);
				break;
			case 92:
				V2.secondeChangementFiltre = Controllino_GetSecond();
				V2.minuteChangementFiltre = Controllino_GetMinute();
				V2.heureChangementFiltre = Controllino_GetHour();
				V2.jourChangementFiltre = Controllino_GetDay();
				V2.moisChangementFiltre = Controllino_GetMonth();
				V2.anneeChangementFiltre = Controllino_GetYear();
				V2.changementFiltre = timeStamp(Controllino_GetYear(), Controllino_GetMonth(), Controllino_GetDay(), Controllino_GetHour(), Controllino_GetMinute(), Controllino_GetMinute());
				saveCTA(0);
				break;
			case 81:
			case 18: //FORM MENU
			case 36: //FORM MENU
			case 41: //FORM MENU
			case 51: //FORM MENU
			case 7:  //FORM MENU
			case 22: //FORM MENU	
			case 1:  //FORM MENU	
				if (commandeDeporteeOption == false) form = MENU;
				else form = MENU_CMD;
				genie.WriteObject(GENIE_OBJ_FORM, form, 0);
				break;
				//Modifs de params au clavier
			case 6:
				processToKeyboard(&seuilTempHorsGel, "seuil T?C Hors Gel", 0, 50);
				break;
			case 8:
				processToKeyboard(&V1.Kp, "Kp Ventilo Soufflage", 0, 1000);
				break;
			case 9:
				processToKeyboard(&V1.Ki, "Ki Ventilo Soufflage", 0, 1000);
				break;
			case 10:
				processToKeyboard(&V1.Kd, "Kd Ventilo Soufflage", 0, 1000);
				break;
			case 11:
				processToKeyboard(&V1.Kfactor, "Kfactor Ventilo Soufflage", 0, 1000);
				break;
			case 12:
				processToKeyboard(&V2.Kp, "Kp Ventilo Reprise", 0, 1000);
				break;
			case 13:
				processToKeyboard(&V2.Ki, "Ki Ventilo Reprise", 0, 1000);
				break;
			case 14:
				processToKeyboard(&V2.Kd, "Kd Ventilo Reprise", 0, 1000);
				break;
			case 15:
				processToKeyboard(&V2.Kfactor, "Kfactor Ventilo Reprise", 0, 1000);
				break;
			case 23:
				processToKeyboard(&jourSemaine, "Jour de la semaine 1:Lundi 2:Mardi ...", 0, 7);
				break;
			case 24:
				processToKeyboard(&jour, "Jours", 0, 31);
				break;
			case 25:
				processToKeyboard(&mois, "Mois", 0, 12);
				break;
			case 26:
				processToKeyboard(&annee, "Annee 2017=17,2018=18", 0, 100);
				break;
			case 27:
				processToKeyboard(&heure, "Heure", 0, 24);
				break;
			case 28:
				processToKeyboard(&minute, "Minute", 0, 59);
				break;
			case 31:
				processToKeyboard(&SOUFFLAGE, "Num sonde soufflage", 0, 3);
				break;
			case 32:
				processToKeyboard(&REPRISE, "Num sonde reprise", 0, 3);
				break;
			case 33:
				processToKeyboard(&EXTRACTION, "Num sonde extraction", 0, 3);
				break;
			case 34:
				processToKeyboard(&AIRNEUF, "Num sonde air neuf", 0, 3);
				break;
			case 35:
				processToKeyboard(&debitMaxCTA, "Debit Max CTA", 0, 20000);
				break;
			case 37:
				processToKeyboard(&TE.consigneTempEteOccupe, "Cons. Temp. Soufflage ete", 0, 50);
				break;
			case 38:
				processToKeyboard(&TH.consigneTempHiverOccupe, "Cons. Temp. Soufflage Hiver", 0, 50);
				break;
			case 39:
				processToKeyboard(&tempoModbus, "tempoModBus", 0, 1000);
				break;
			case 42:
				processToKeyboard(&consignePressionModeOccupe, "Cons Pression reprise Occ", 0, pressionMaxCTA);
				break;
			case 43:
				processToKeyboard(&consignePressionModeInoccupe, "Cons Pression reprise Inocc", 0, pressionMaxCTA);
				break;
			case 44:
				processToKeyboard(&pressionMaxCTA, "Pression Max CTA", 0, 200000);
				break;
			case 45:
				processToKeyboard(&V2.consignePressionModeInoccupe, "Temperature ete Inoccupe", 0, pressionMaxCTA);
				break;
			case 82:
				processToKeyboard(&V2.consignePressionModeInoccupe, "Temperature hiver Inoccupe", 0, pressionMaxCTA);
				break;
			case 48:
				processToKeyboard(&V1.consigneDebitModeOccupe, "Soufflage cons debit Occupe", 0, debitMaxCTA);
				break;
			case 49:
				processToKeyboard(&V1.consigneDebitModeInoccupe, "Soufflage cons debit Inocc.", 0, debitMaxCTA);
				break;
			case 50:
				processToKeyboard(&V1.consigneDebitFreeCooling, "Soufflage cons debit FreeCooling", 0, debitMaxCTA);
				break;
			case 62:
				processToKeyboard(&V2.consigneDebitModeOccupe, "Reprise cons debit Occupe", 0, debitMaxCTA);
				break;
			case 63:
				processToKeyboard(&V2.consigneDebitModeInoccupe, "Reprise cons debit Inocc.", 0, debitMaxCTA);
				break;
			case 64:
				processToKeyboard(&V2.consigneDebitFreeCooling, "Reprise cons debit FreeCooling", 0, debitMaxCTA);
				break;
			case 53:
				processToKeyboard(&heureDebutOccupe, "Heure debut occupation");
				break;
			case 61:
				processToKeyboard(&heureFinOccupe, "Heure fin occupation");
				break;
			case 54:
				processToKeyboard(&passerelle, "Gateway");
				break;
			case 55:
				processToKeyboard(&adresseIP, "IP Address");
				break;
			case 56:
				genie.WriteObject(GENIE_OBJ_FORM, FORM::PARAMS_RESEAU, 0);
				saveCTA(0);
				resetFunc();
				break;
			case 71:
				processToKeyboard(&tempsMaxBatChaud, "Temperature max batterie chaude");
				break;
			case 72:
				processToKeyboard(&tempsMinBatChaud, "Temperature min batterie chaude");
				break;
			case 73:
				processToKeyboard(&tempsMaxBatFroid, "Temperature max batterie froide");
				break;
			case 74:
				processToKeyboard(&tempsMinBatFroid, "Temperature min batterie froide");
				break;
			case 75:
				processToKeyboard(&BC.Ki, "BC.Ki",0,100);
				break;
			case 76:
				processToKeyboard(&BF.Ki, "BF.Ki", 0, 1000);
				break;
			case 77:
				processToKeyboard(&BC.Kp, "BC.Kp", 0, 1000);
				break;
			/*case 78:
				//processToKeyboard(&tempsMinBatFroid, "Temperature min batterie froide");
				break;
			case 79:
				//processToKeyboard(&tempsMaxBatChaud, "Temperature max batterie chaude");
				break;*/
			case 80:
				processToKeyboard(&BF.Kp, "BF.Kp", 0, 1000);
				break;
			/*case 81:
				processToKeyboard(&tempsMaxBatFroid, "Temperature max batterie froide");
				break;
			case 82:
				processToKeyboard(&tempsMinBatFroid, "Temperature min batterie froide");
				break;
			case 83:
				processToKeyboard(&tempsMaxBatChaud, "Temperature max batterie chaude");
				break;*/
			case 84:
				processToKeyboard(&BC.Kd, "BC.Kd", 0, 1000);
				break;
			case 85:
				processToKeyboard(&BF.Kd, "BF.Kd", 0, 1000);
				break;
			case 86:
				processToKeyboard(&alarmePeriode, "seuil alarme periodique (en mois)", 0, 24);
				break;
			case 87:
				processToKeyboard(&alarmeSurVentil, "Alarme seuil vitesse ventilateur (%) ", 0, 100);
				break;
			/*case 88:
				processToKeyboard(&alarmePeriode, "seuil alarme periodique (en mois)", 0, 24);
				break;*/
			case 89:
				processToKeyboard(&TH.Kd, "TH.Kd", 0, 1000);
				break;
			/*case 90:
				processToKeyboard(&alarmePeriode, "seuil alarme periodique (en mois)", 0, 24);
				break;
			case 91:
				processToKeyboard(&alarmeSurVentil, "Alarme seuil vitesse ventilateur (%) ", 0, 100);
				break;
			case 92:
				processToKeyboard(&alarmePeriode, "seuil alarme periodique (en mois)", 0, 24);
				break;*/
			case 93:
				processToKeyboard(&TH.Ki, "TH.Ki", 0, 1000);
				break;
			case 94:
				processToKeyboard(&TH.Kp, "TH.Kp", 0, 1000);
				break;
			case 95:
				processToKeyboard(&TE.Kp, "TE.Kp", 0, 1000);
				break;
			case 96:
				processToKeyboard(&TE.Ki, "TE.Ki", 0, 1000);
				break;
			case 97:
				processToKeyboard(&TE.Kd, "TE.Kd", 0, 1000);
				break;
			/*case 98:
				processToKeyboard(&alarmePeriode, "seuil alarme periodique (en mois)", 0, 24);
				break;
			case 99:
				processToKeyboard(&alarmeSurVentil, "Alarme seuil vitesse ventilateur (%) ", 0, 100);
				break;*/
			}
			if (KB) {
				if (readIntFromKB) {
					processToKeyboard(int_ptr, param, int_KBmin, int_KBmax);
					readIntFromKB = false;
				}
				else {
					processToKeyboard(ptr, param, double_KBmin, double_KBmax);
				}
				KB = false;
			}
			genie.WriteStr(55, passerelle);
		}
		if (Event.reportObject.object == GENIE_OBJ_KEYBOARD) // If this event is from a Keyboard
		{
			if (Event.reportObject.index == 0)  // If from Keyboard0
			{
				keyboardValue = genie.GetEventData(&Event); // Get data from Keyboard0
				if (keyboardValue == 12) // Valider
				{
					if (form == FORM::DATE_ET_HEURE) modifdate = true;
					if ( form == FORM::MENUADMIN_FULL) {
						if (str.toInt() != codeAdmin) {
							if (commandeDeporteeOption == false)form = FORM::MENU;
							else form = FORM::MENU_CMD;
						}
					}
					else {
						if (readIntFromKB) {
							if (str.toInt() >= int_KBmin && str.toInt() <= int_KBmax) {
								*int_ptr = str.toInt();
								erreurKB = false;
								readIntFromKB = false;
							}
							else erreurKB = true;
						}
						else if (readStrFromKB) {
							readStrFromKB = false;
							*string_ptr = str;
							stringToIp();
						}
						else if (readHeureFromKB) {

							if (checkHeure(str.toInt())) {
								*int_ptr = str.toInt();
								erreurKB = false;
								readHeureFromKB = false;
							}
							else erreurKB = true;
						}
						else {
							if (str.toFloat() >= double_KBmin && str.toFloat() <= double_KBmax) {
								*ptr = str.toFloat();
								erreurKB = false;
							}
							else erreurKB = true;
						}
						changedParams = true;
						if (V1.changedPID) {
							V1.myPID.SetTunings(V1.Kp, V1.Ki, V1.Kd);
							V1.changedPID = false;
						}
						if (V2.changedPID) {
							V2.myPID.SetTunings(V2.Kp, V2.Ki, V2.Kd);
							V2.changedPID = false;
						}
						if (BC.changedPID) {
							BC.myPID.SetTunings(BC.Kp, BC.Ki, BC.Kd);
							BC.changedPID = false;
						}
						if (BF.changedPID) {
							BF.myPID.SetTunings(BF.Kp, BF.Ki, BF.Kd);
							BF.changedPID = false;
						}
						if (TH.changedPID) {
							TH.myPID.SetTunings(TH.Kp, TH.Ki, TH.Kd);
							TH.changedPID = false;
						}
						if (TE.changedPID) {
							TE.myPID.SetTunings(TE.Kp, TE.Ki, TE.Kd);
							TE.changedPID = false;
						}
					}
					if (!erreurKB) genie.WriteObject(GENIE_OBJ_FORM, form, 0);

				}
				else if (keyboardValue == 11) //ANNULER
				{
					if (V1.changedPID) V1.changedPID = false;
					if (V2.changedPID) V2.changedPID = false;
					if (BC.changedPID) BC.changedPID = false;
					if (BF.changedPID) BF.changedPID = false;
					if (TH.changedPID) TH.changedPID = false;
					if (TE.changedPID) TE.changedPID = false;
					str = "";
					if (form == FORM::MENUADMIN_FULL && commandeDeporteeOption == false) form = FORM::MENU;
					else if (form == FORM::MENUADMIN_FULL && commandeDeporteeOption == true) form = FORM::MENU_CMD;

					genie.WriteObject(GENIE_OBJ_FORM, form, 0);
				}
				else if (keyboardValue == 10) //'.'
				{
					str += ".";
					genie.WriteStr(11, str);
				}
				else if (keyboardValue == 13) //DELETE
				{
					str.remove(str.length() - 1);
					genie.WriteStr(11, str);
				}
				else {
					str += keyboardValue;
					genie.WriteStr(11, str);
				}
			}
		}
		saveCTA(0);
	}
	//Serial.println("ok");

	genieFrame Event2;
	genie2.DequeueEvent(&Event2); // Remove the next queued Event2 from the buffer, and process it below

								  //If the cmd received is from a Reported Event (Events triggered from the Events tab of Workshop4 objects)
	if (Event2.reportObject.cmd == GENIE_REPORT_EVENT)
	{
		waitPeriod = 0;
		if (Event2.reportObject.object == GENIE_OBJ_4DBUTTON) // If this event is from a 4DButton
		{
			switch (Event2.reportObject.index) {
			case 0: //MARCHEV1
				if (Event2.reportObject.data_lsb == 0) V1.commandeMarche = false;
				else V1.commandeMarche = true;
				break;
			case 5: //MARCHEV2
				if (Event2.reportObject.data_lsb == 0) V2.commandeMarche = false;
				else V2.commandeMarche = true;
				break;
			case 2: //FORCAGE BYPASS
				if (Event2.reportObject.data_lsb == 0) Registreforcage = false;
				else Registreforcage = true;
				break;
			case 3: //CHANGEOVER
				if (Event2.reportObject.data_lsb == 0) changeOver = false;
				else changeOver = true;
				break;
			case 4: //MARCHEV1
				ecranbparret = true;
				break;
			case 1: //MARCHEV1
				ecranbpmarche = true;
				break;
			case 6: //CHANGEOVER
				if (Event2.reportObject.data_lsb == 0) registreOption = false;
				else registreOption = true;
				break;
			case 7: //CHANGEOVER
				if (Event2.reportObject.data_lsb == 0) commandeDeporteeOption = false;
				else commandeDeporteeOption = true;
				break;
			case 8: //CHANGEOVER
				if (Event2.reportObject.data_lsb == 0) GTCOption = false;
				else GTCOption = true;
				break;
			case 9: //CHANGEOVER
				if (Event2.reportObject.data_lsb == 0) alarmeEncrassementFiltreOption = false;
				else alarmeEncrassementFiltreOption = true;
				break;
			case 10: //CHANGEOVER
				if (Event2.reportObject.data_lsb == 0) regulSurDepressionOption = false;
				else regulSurDepressionOption = true;
				break;
			case 11: //CHANGEOVER
				if (Event2.reportObject.data_lsb == 0) { boiteaboutonOption = true; ecranOption = false; }
				else { boiteaboutonOption = false; ecranOption = true; }
				break;
			case 12: //CHANGEOVER
				if (Event2.reportObject.data_lsb == 0) { batterieChaudeOption = false; }
				else { batterieChaudeOption = true; }
				break;
			case 13: //CHANGEOVER
				if (Event2.reportObject.data_lsb == 0) { batterieFroideOption = false; }
				else { batterieFroideOption = true; }
				break;
			case 14: //CHANGEOVER
				if (Event2.reportObject.data_lsb == 0) { BC.commandeMarche = false; }
				else { BC.commandeMarche = true; }
				break;
			case 15: //CHANGEOVER
				if (Event2.reportObject.data_lsb == 0) { BF.commandeMarche = false; }
				else { BF.commandeMarche = true; }
				break;
			}
			changedParams = true;
			saveCTA(0);
		}
		if (Event2.reportObject.object == GENIE_OBJ_DIPSW) // If this event is from a DIPSWITCH
		{
			switch (Event2.reportObject.index) {
			case 0: //MODE MARCHE
				if (Event2.reportObject.data_lsb == 0) modeMarche_Applique = modeMarche::ARRET;
				else if (Event2.reportObject.data_lsb == 1) modeMarche_Applique = modeMarche::MANU;
				else if (Event2.reportObject.data_lsb == 2) modeMarche_Applique = modeMarche::AUTO;
				break;
			case 1: //GESTION MODE OCCUPE
				if (Event2.reportObject.data_lsb == 0) jourOccupe[0] = false;
				else jourOccupe[0] = true;
				break;
			case 2:
				if (Event2.reportObject.data_lsb == 0) jourOccupe[1] = false;
				else jourOccupe[1] = true;
				break;
			case 3:
				if (Event2.reportObject.data_lsb == 0) jourOccupe[2] = false;
				else jourOccupe[2] = true;
				break;
			case 4:
				if (Event2.reportObject.data_lsb == 0) jourOccupe[3] = false;
				else jourOccupe[3] = true;
				break;
			case 5:
				if (Event2.reportObject.data_lsb == 0) jourOccupe[4] = false;
				else jourOccupe[4] = true;
				break;
			case 6:
				if (Event2.reportObject.data_lsb == 0) jourOccupe[5] = false;
				else jourOccupe[5] = true;
				break;
			case 7:
				if (Event2.reportObject.data_lsb == 0) jourOccupe[6] = false;
				else jourOccupe[6] = true;
				break;
			case 8:
				if (Event2.reportObject.data_lsb == 0) modeMarcheAutoOcc = modeMarcheAuto::REGULDEBIT;
				else if (Event2.reportObject.data_lsb == 1 && regulSurDepressionOption == true) modeMarcheAutoOcc = modeMarcheAuto::REGULDEPRESSION;
				else if (Event2.reportObject.data_lsb == 2 && commandeDeporteeOption == true) modeMarcheAutoOcc = modeMarcheAuto::CMDDEPORTEE;
				break;
			case 9:
				if (Event2.reportObject.data_lsb == 0) modeMarcheAutoInocc = modeMarcheAuto::REGULDEBIT;
				else if (Event2.reportObject.data_lsb == 1 && regulSurDepressionOption == true) modeMarcheAutoInocc = modeMarcheAuto::REGULDEPRESSION;
				else if (Event2.reportObject.data_lsb == 2 && commandeDeporteeOption == true) modeMarcheAutoInocc = modeMarcheAuto::CMDDEPORTEE;
				break;
			}
			changedParams = true;
			for (int i = 0; i < 7; i++) {
				bitWrite(jourOccupe_int, i, jourOccupe[i]);
			}
		}
		if (Event2.reportObject.object == GENIE_OBJ_SLIDER) // If this event is from a SLIDER
		{
			switch (Event2.reportObject.index) {
			case 0:
				slideConsigne_m3h = Event2.reportObject.data_lsb;
				break;
			}
		}
		if (Event2.reportObject.object == GENIE_OBJ_WINBUTTON) // If this event is from a WinButton
		{
			//Serial.println("EVENT");
			//Serial.print(Event2.reportObject.object);//////////////////////////////
			str = "";
			switch (Event2.reportObject.index) {
			case 5: //FORM MENUADMIN ==>Clavier pour mode admin
			case 65:
				form = FORM::MENUADMIN_FULL;
				param = "mot de passe admin";
				genie.WriteObject(GENIE_OBJ_FORM, FORM::KEYBOARD, 0);
				break;
			case 20: //FORM MENUADMIN ==>Retour
			case 30: //FORM MENUADMIN ==>Retour
			case 19: //FORM MENUADMIN ==>Retour
			case 29: //FORM MENUADMIN ==>Retour
			case 88: //FORM MENUADMIN ==>Retour
			case 70: //FORM MENUADMIN ==>Retour
				form = FORM::MENUADMIN_FULL;
				genie.WriteObject(GENIE_OBJ_FORM, form, 0);
				break;
			case 0: //FORM Boite a boutons 
				if (boiteaboutonOption == true) form = FORM::BOUTONS;
				if (ecranOption == true) form = FORM::ECRAN;
				genie.WriteObject(GENIE_OBJ_FORM, form, 0);
				break;
			case 2: //FORM SYNOPTIQUE
			case 59:
				form = FORM::SYNOPTIQUE;
				waitPeriodTemps = 0;
				genie.WriteObject(GENIE_OBJ_FORM, form, 0);
				break;
			case 58://FORM CONFIG CTA
			case 4: //FORM CONFIG CTA
				form = FORM::CONFIG_CTA;
				genie.WriteObject(GENIE_OBJ_FORM, form, 0);
				break;
			case 16: //FORM Params Ventilateurs
			case 77:
			case 75:
			case 66:
				form = FORM::PARAMS_VENTILO;
				genie.WriteObject(GENIE_OBJ_FORM, form, 0);
				break;
			case 17: //FORM Params Registre
			case 76:
				form = FORM::PARAMS_RM;
				genie.WriteObject(GENIE_OBJ_FORM, form, 0);
				break;
			case 21: //modif de la date et l'heure
			case 67:
				form = FORM::DATE_ET_HEURE;
				genie.WriteObject(GENIE_OBJ_FORM, form, 0);
				break;
			case 3: //FORM Params CTA
			case 46: //FORM Params CTA
			case 60:
				form = FORM::PARAMS_CTA;
				genie.WriteObject(GENIE_OBJ_FORM, form, 0);
				break;
			case 52: //FORM Params CTA2
			case 40: //FORM Params CTA2
				form = FORM::PARAMS_CTA2;
				genie.WriteObject(GENIE_OBJ_FORM, form, 0);
				break;
			case 47: //FORM Params CTA3
				form = FORM::PARAMS_CTA3;
				genie.WriteObject(GENIE_OBJ_FORM, form, 0);
				break;
			case 57: //FORM Params RESEAU
				form = FORM::PARAMS_RESEAU;
				genie.WriteObject(GENIE_OBJ_FORM, form, 0);
				break;
			case 69: //FORM Options CTA
			case 68:
				form = FORM::OPTIONS_CTA;
				genie.WriteObject(GENIE_OBJ_FORM, form, 0);
				break;
			case 83:
				form = FORM::BATTERIES;
				genie.WriteObject(GENIE_OBJ_FORM, form, 0);
				break;
			case 78:
				bpmarche = false;
				break;
			case 79:
				bpmarche = true;
				break;
			case 84:
			case 85:
			case 89:
			case 90:
				form = FORM::FILTRE;
				genie.WriteObject(GENIE_OBJ_FORM, form, 0);
				break;
			case 91:
				V1.secondeChangementFiltre = Controllino_GetSecond();
				V1.minuteChangementFiltre = Controllino_GetMinute();
				V1.heureChangementFiltre = Controllino_GetHour();
				V1.jourChangementFiltre = Controllino_GetDay();
				V1.moisChangementFiltre = Controllino_GetMonth();
				V1.anneeChangementFiltre = Controllino_GetYear();
				V1.changementFiltre = timeStamp(Controllino_GetYear(), Controllino_GetMonth(), Controllino_GetDay(), Controllino_GetHour(), Controllino_GetMinute(), Controllino_GetSecond());
				saveCTA(0);
				break;
			case 92:
				V2.secondeChangementFiltre = Controllino_GetSecond();
				V2.minuteChangementFiltre = Controllino_GetMinute();
				V2.heureChangementFiltre = Controllino_GetHour();
				V2.jourChangementFiltre = Controllino_GetDay();
				V2.moisChangementFiltre = Controllino_GetMonth();
				V2.anneeChangementFiltre = Controllino_GetYear();
				V2.changementFiltre = timeStamp(Controllino_GetYear(), Controllino_GetMonth(), Controllino_GetDay(), Controllino_GetHour(), Controllino_GetMinute(), Controllino_GetSecond());
				saveCTA(0);
				break;
			case 81:
			case 18: //FORM MENU
			case 36: //FORM MENU
			case 41: //FORM MENU
			case 51: //FORM MENU
			case 7: //FORM MENU
			case 22: //FORM MENU	
			case 1: //FORM MENU	
			case 80:
				if (commandeDeporteeOption == false) form = MENU;
				else form = MENU_CMD;
				genie.WriteObject(GENIE_OBJ_FORM, form, 0);
				break;
				//Modifs de params au clavier
			case 6:
				processToKeyboard2(&seuilTempHorsGel, "seuil T?C Hors Gel", 0, 50);
				break;
			case 8:
				processToKeyboard2(&V1.Kp, "Kp Ventilo Soufflage", 0, 1000);
				break;
			case 9:
				processToKeyboard2(&V1.Ki, "Ki Ventilo Soufflage", 0, 1000);
				break;
			case 10:
				processToKeyboard2(&V1.Kd, "Kd Ventilo Soufflage", 0, 1000);
				break;
			case 11:
				processToKeyboard2(&V1.Kfactor, "Kfactor Ventilo Soufflage", 0, 1000);
				break;
			case 12:
				processToKeyboard2(&V2.Kp, "Kp Ventilo Reprise", 0, 1000);
				break;
			case 13:
				processToKeyboard2(&V2.Ki, "Ki Ventilo Reprise", 0, 1000);
				break;
			case 14:
				processToKeyboard2(&V2.Kd, "Kd Ventilo Reprise", 0, 1000);
				break;
			case 15:
				processToKeyboard2(&V2.Kfactor, "Kfactor Ventilo Reprise", 0, 1000);
				break;
			case 23:
				processToKeyboard2(&jourSemaine, "Jour de la semaine 1:Lundi 2:Mardi ...", 0, 7);
				break;
			case 24:
				processToKeyboard2(&jour, "Jours", 0, 31);
				break;
			case 25:
				processToKeyboard2(&mois, "Mois", 0, 12);
				break;
			case 26:
				processToKeyboard2(&annee, "Annee 2017=17,2018=18", 0, 100);
				break;
			case 27:
				processToKeyboard2(&heure, "Heure", 0, 24);
				break;
			case 28:
				processToKeyboard2(&minute, "Minute", 0, 59);
				break;
			case 31:
				processToKeyboard2(&SOUFFLAGE, "Num sonde soufflage", 0, 3);
				break;
			case 32:
				processToKeyboard2(&REPRISE, "Num sonde reprise", 0, 3);
				break;
			case 33:
				processToKeyboard2(&EXTRACTION, "Num sonde extraction", 0, 3);
				break;
			case 34:
				processToKeyboard2(&AIRNEUF, "Num sonde air neuf", 0, 3);
				break;
			case 35:
				processToKeyboard2(&debitMaxCTA, "Debit Max CTA", 0, 20000);
				break;
			case 37:
				processToKeyboard2(&TE.consigneTempEteOccupe, "Cons. Temp. Soufflage ete", 0, 50);
				break;
			case 38:
				processToKeyboard2(&TH.consigneTempHiverOccupe, "Cons. Temp. Soufflage Hiver", 0, 50);
				break;
			case 39:
				processToKeyboard2(&tempoModbus, "tempoModBus", 0, 1000);
				break;
			case 42:
				processToKeyboard2(&consignePressionModeOccupe, "Cons Pression reprise Occ", 0, pressionMaxCTA);
				break;
			case 43:
				processToKeyboard2(&consignePressionModeInoccupe, "Cons Pression reprise Inocc", 0, pressionMaxCTA);
				break;
			case 44:
				processToKeyboard2(&pressionMaxCTA, "Pression Max CTA", 0, 20000);
				break;
			case 45:
				processToKeyboard2(&V2.consignePressionModeInoccupe, "Temperature ete Inoccupe", 0, pressionMaxCTA);
				break;
			case 82:
				processToKeyboard2(&adresseIP, "Temperature hiver Inoccupe");
				break;
			case 48:
				processToKeyboard2(&V1.consigneDebitModeOccupe, "Soufflage cons debit Occupe", 0, debitMaxCTA);
				break;
			case 49:
				processToKeyboard2(&V1.consigneDebitModeInoccupe, "Soufflage cons debit Inocc.", 0, debitMaxCTA);
				break;
			case 50:
				processToKeyboard2(&V1.consigneDebitFreeCooling, "Soufflage cons debit FreeCooling", 0, debitMaxCTA);
				break;
			case 62:
				processToKeyboard2(&V2.consigneDebitModeOccupe, "Reprise cons debit Occupe", 0, debitMaxCTA);
				break;
			case 63:
				processToKeyboard2(&V2.consigneDebitModeInoccupe, "Reprise cons debit Inocc.", 0, debitMaxCTA);
				break;
			case 64:
				processToKeyboard2(&V2.consigneDebitFreeCooling, "Reprise cons debit FreeCooling", 0, debitMaxCTA);
				break;
			case 53:
				processToKeyboard2(&heureDebutOccupe, "Heure debut occupation");
				break;
			case 61:
				processToKeyboard2(&heureFinOccupe, "Heure fin occupation");
				break;
			case 54:
				processToKeyboard2(&passerelle, "Gateway");
				break;
			case 55:
				processToKeyboard2(&adresseIP, "IP Address");
				break;
			case 56:
				genie.WriteObject(GENIE_OBJ_FORM, FORM::PARAMS_RESEAU, 0);
				saveCTA(0);
				resetFunc();
				break;
			case 71:
				processToKeyboard2(&tempsMaxBatChaud, "Temperature max batterie chaude");
				break;
			case 72:
				processToKeyboard2(&tempsMinBatChaud, "Temperature min batterie chaude");
				break;
			case 73:
				processToKeyboard2(&tempsMaxBatFroid, "Temperature max batterie froide");
				break;
			case 74:
				processToKeyboard2(&tempsMinBatFroid, "Temperature min batterie froide");
				break;
			case 86:
				processToKeyboard2(&alarmePeriode, "seuil alarme periodique (en mois)", 0, 24);
				break;
			case 87:
				processToKeyboard2(&alarmeSurVentil, "Alarme seuil vitesse ventilateur (%) ", 0, 100);
				break;
			}
			if (KB) {
				if (readIntFromKB) {
					processToKeyboard2(int_ptr, param, int_KBmin, int_KBmax);
					readIntFromKB = false;
				}
				else {
					processToKeyboard2(ptr, param, double_KBmin, double_KBmax);
				}
				KB = false;
			}
			genie.WriteStr(55, passerelle);
		}
		if (Event2.reportObject.object == GENIE_OBJ_KEYBOARD) // If this event is from a Keyboard
		{
			if (Event2.reportObject.index == 0)  // If from Keyboard0
			{
				keyboardValue = genie.GetEventData(&Event); // Get data from Keyboard0
				if (keyboardValue == 12) // Valider
				{
					if (form == FORM::DATE_ET_HEURE) modifdate = true;
					if (form == FORM::MENUADMIN_FULL) {
						if (str.toInt() != codeAdmin) {
							if (commandeDeporteeOption == false)form = FORM::MENU;
							else form = FORM::MENU_CMD;
						}
					}
					else {
						if (readIntFromKB) {
							if (str.toInt() >= int_KBmin && str.toInt() <= int_KBmax) {
								*int_ptr = str.toInt();
								erreurKB = false;
								readIntFromKB = false;
							}
							else erreurKB = true;
						}
						else if (readStrFromKB) {
							readStrFromKB = false;
							*string_ptr = str;
							stringToIp();
						}
						else if (readHeureFromKB) {

							if (checkHeure(str.toInt())) {
								*int_ptr = str.toInt();
								erreurKB = false;
								readHeureFromKB = false;
							}
							else erreurKB = true;
						}
						else {
							if (str.toFloat() >= double_KBmin && str.toFloat() <= double_KBmax) {
								*ptr = str.toFloat();
								erreurKB = false;
							}
							else erreurKB = true;
						}
						changedParams = true;
						if (V1.changedPID) {
							V1.myPID.SetTunings(V1.Kp, V1.Ki, V1.Kd);
							V1.myPIDPression.SetTunings(V1.Kp, V1.Ki, V1.Kd);
							V1.changedPID = false;
							//Serial.print("\n\rV1.kp="); Serial.print(V1.Kp);
							//Serial.print("\n\rV1.ki="); Serial.print(V1.Ki);
							//Serial.print("\n\rV1.kd="); Serial.print(V1.Kd);
						}
						if (V2.changedPID) {
							V2.myPID.SetTunings(V2.Kp, V2.Ki, V2.Kd);
							V2.myPIDPression.SetTunings(V2.Kp, V2.Ki, V2.Kd);
							V2.changedPID = false;
							//Serial.print("\n\rV2.kp="); Serial.print(V2.Kp);
							//Serial.print("\n\rV2.ki="); Serial.print(V2.Ki);
							//Serial.print("\n\rV2.kd="); Serial.print(V2.Kd);
						}
					}
					if (!erreurKB) genie.WriteObject(GENIE_OBJ_FORM, form, 0);

				}
				else if (keyboardValue == 11) //ANNULER
				{
					if (V1.changedPID) V1.changedPID = false;
					if (V2.changedPID) V2.changedPID = false;
					str = "";
					 if (form == FORM::MENUADMIN_FULL && commandeDeporteeOption == false) form = FORM::MENU;
					else if (form == FORM::MENUADMIN_FULL && commandeDeporteeOption == true) form = FORM::MENU_CMD;

					genie.WriteObject(GENIE_OBJ_FORM, form, 0);
				}
				else if (keyboardValue == 10) //'.'
				{
					str += ".";
					genie.WriteStr(11, str);
				}
				else if (keyboardValue == 13) //DELETE
				{
					str.remove(str.length() - 1);
					genie.WriteStr(11, str);
				}
				else {
					str += keyboardValue;
					genie.WriteStr(11, str);
				}
			}
		}
		saveCTA(0);
	}
}

bool checkHeure(int h) {
	if (String(h).length() > 4 || String(h).length() < 3) return false;
	int H = (int)(h / 100);
	int m = h - H * 100;
	if (H > 23) return false;
	if (m > 59) return false;
	return true;
}

void processToKeyboard(int * ptr, String caption, int min, int max) {
	genie.WriteObject(GENIE_OBJ_FORM, FORM::KEYBOARD, 0);
	int_ptr = ptr;
	readIntFromKB = true;
	param = caption;
	int_KBmin = min;
	int_KBmax = max;
}
void processToKeyboard(int * ptr, String caption) { //HEURE
	genie.WriteObject(GENIE_OBJ_FORM, FORM::KEYBOARD, 0);
	int_ptr = ptr;
	readHeureFromKB = true;
	param = caption;
}
void processToKeyboard(unsigned long * ptr, String caption, int min, int max) {
	genie.WriteObject(GENIE_OBJ_FORM, FORM::KEYBOARD, 0);
	int_ptr = (int*)ptr;
	readIntFromKB = true;
	param = caption;
	int_KBmin = min;
	int_KBmax = max;
}
void processToKeyboard(double * dptr, String caption, double min, double max) {
	genie.WriteObject(GENIE_OBJ_FORM, FORM::KEYBOARD, 0);
	ptr = dptr;
	param = caption;
	double_KBmin = min;
	double_KBmax = max;
}
void processToKeyboard(String * ptr, String caption) {
	genie.WriteObject(GENIE_OBJ_FORM, FORM::KEYBOARD, 0);
	string_ptr = ptr;
	param = caption;

	readStrFromKB = true;
}
void processToKeyboard2(int * ptr, String caption, int min, int max) {
	genie2.WriteObject(GENIE_OBJ_FORM, FORM::KEYBOARD, 0);
	int_ptr = ptr;
	readIntFromKB = true;
	param = caption;
	int_KBmin = min;
	int_KBmax = max;
}
void processToKeyboard2(int * ptr, String caption) { //HEURE
	genie2.WriteObject(GENIE_OBJ_FORM, FORM::KEYBOARD, 0);
	int_ptr = ptr;
	readHeureFromKB = true;
	param = caption;
}
void processToKeyboard2(unsigned long * ptr, String caption, int min, int max) {
	genie2.WriteObject(GENIE_OBJ_FORM, FORM::KEYBOARD, 0);
	int_ptr = (int*)ptr;
	readIntFromKB = true;
	param = caption;
	int_KBmin = min;
	int_KBmax = max;
}
void processToKeyboard2(double * dptr, String caption, double min, double max) {
	genie2.WriteObject(GENIE_OBJ_FORM, FORM::KEYBOARD, 0);
	ptr = dptr;
	param = caption;
	double_KBmin = min;
	double_KBmax = max;
}
void processToKeyboard2(String * ptr, String caption) {
	genie2.WriteObject(GENIE_OBJ_FORM, FORM::KEYBOARD, 0);
	string_ptr = ptr;
	param = caption;

	readStrFromKB = true;
}


void stringToIp() {
	int point1 = adresseIP.indexOf('.');
	int point2 = adresseIP.indexOf('.', point1 + 1);
	int point3 = adresseIP.indexOf('.', point2 + 1);
	ip[0] = adresseIP.substring(0, point1).toInt();
	ip[1] = adresseIP.substring(point1 + 1, point2).toInt();
	ip[2] = adresseIP.substring(point2 + 1, point3).toInt();
	ip[3] = adresseIP.substring(point3 + 1).toInt();
	point1 = passerelle.indexOf('.');
	point2 = passerelle.indexOf('.', point1 + 1);
	point3 = passerelle.indexOf('.', point2 + 1);
	gw[0] = passerelle.substring(0, point1).toInt();
	gw[1] = passerelle.substring(point1 + 1, point2).toInt();
	gw[2] = passerelle.substring(point2 + 1, point3).toInt();
	gw[3] = passerelle.substring(point3 + 1).toInt();
}

void IPToString() {
	adresseIP = "";
	adresseIP += ip[0]; adresseIP += "."; adresseIP += ip[1]; adresseIP += "."; adresseIP += ip[2]; adresseIP += "."; adresseIP += ip[3];
	passerelle = "";
	passerelle += gw[0]; passerelle += "."; passerelle += gw[1]; passerelle += "."; passerelle += gw[2]; passerelle += "."; passerelle += gw[3];
}


void startScreen() {
	/*4D SYSTEMS*/
	genie.Begin(Serial1);
	genie.AttachEventHandler(myGenieEventHandler);
	pinMode(RESETLINE, OUTPUT);  // Set D4 on Arduino to Output (4D Arduino Adaptor V2 - Display Reset)
	digitalWrite(RESETLINE, 0);  // Reset the Display via D4
	delay(100);
	digitalWrite(RESETLINE, 1);  // unReset the Display via D4
	delay(3500); //let the display start up after the reset (This is important)
	genie.WriteContrast(15);
	waitPeriod = millis();
}
void startScreen2() {
	/*4D SYSTEMS*/
	genie2.Begin(Serial2);
	genie2.AttachEventHandler(myGenieEventHandler);
	pinMode(RESETLINE2, OUTPUT);  // Set D4 on Arduino to Output (4D Arduino Adaptor V2 - Display Reset)
	digitalWrite(RESETLINE2, 0);  // Reset the Display via D4
	delay(100);
	digitalWrite(RESETLINE2, 1);  // unReset the Display via D4
	delay(3500); //let the display start up after the reset (This is important)
	genie2.WriteContrast(15);
	waitPeriod = millis();
}



void readMesures() {
	sensors.requestTemperatures();
	if (temperature[0].status == READ_OK)temperature[0].value = sensors.getTempCByIndex(0);
	if (temperature[1].status == READ_OK)temperature[1].value = sensors.getTempCByIndex(1);
	if (temperature[2].status == READ_OK)temperature[2].value = sensors.getTempCByIndex(2);
	if (temperature[3].status == READ_OK)temperature[3].value = sensors.getTempCByIndex(3);
/*Serial.print(temperature[0].status);
	Serial.print("\t valeur:");
	Serial.print(sensors.getTempCByIndex(0));
	Serial.print("\n\r");*/
	/*Serial.print("temp 1\t status:");
	Serial.print(temperature[1].status);
	Serial.print("\t valeur:");
	Serial.print(sensors.getTempCByIndex(1));
	Serial.print("\n\r");*/
	///Serial.print(sensors.getTempCByIndex(2));
	//Serial.print("\n\r");
	//.print(sensors.getTempCByIndex(3));
	//Serial.print("\n\r");
}

void writeScreen2() {
	String timeString = String(jour); timeString.concat("/"); timeString.concat(String(mois)); timeString += "/"; timeString.concat(String(annee)); timeString += " "; timeString.concat(String(heure)); timeString += ":"; timeString.concat(String(minute));
	String modeString = "Mode: ";
	String temp = "";
	String stringdate = "";
	String stringRegistre = "";
	String stringFiltre = "";
	String stringModeMarche = "";
	switch (form) {
	case FORM::PARAMS_RESEAU:
		genie2.WriteStr(32, adresseIP);
		genie2.WriteStr(55, passerelle);
		genie2.WriteStr(56, tempoModbus);
		break;
	case FORM::MENU:
		genie2.WriteStr(58, timeString);
		break;
	case FORM::MENU_CMD:
		genie2.WriteStr(54, timeString);
		break;
	case FORM::MENUADMIN_FULL:
		genie2.WriteStr(31, versioncontrollino);
		break;
	case FORM::SYNOPTIQUE:
		if (modeMarche_Applique == modeMarche::ARRET) modeString += "ARRET, ";
		if (modeMarche_Applique == modeMarche::MANU) modeString += "MANU, ";
		if (modeMarche_Applique == modeMarche::AUTO) modeString += "AUTO, ";
		if (occupe == true) modeString += "Occupe, ";
		else modeString += "Inoccupe, ";
		if (changeOver == true) modeString += "Ete,";
		else modeString += "Hiver,";
		if (modeApplique == HORSGEL)modeString += "\n\r	HorsGel";
		if (modeApplique == FREECOOLING)modeString += "\n\r FREECOOLING";
		if (modeApplique == FREEHEATING)modeString += "\n\r FREEHEATING";
		genie2.WriteStr(0, temperature[SOUFFLAGE].value);
		genie2.WriteStr(1, temperature[REPRISE].value);
		genie2.WriteStr(2, temperature[EXTRACTION].value);
		genie2.WriteStr(3, temperature[AIRNEUF].value);		
		genie2.WriteStr(5, P3.readAna());
		genie2.WriteStr(26, V1.debit);
		genie2.WriteStr(25, V2.debit);
		if (registreOption == false) stringRegistre = "not connected";
		else {
			if (RegistreONOFF == false) stringRegistre = "close";
			else stringRegistre = "open";
		}
		genie2.WriteStr(27, stringRegistre);
		genie2.WriteStr(57, modeString);
		genie2.WriteStr(6, modeApplique);
		if (V1.alarmePeriode == true || V2.alarmePeriode == true || V2.alarmeSurVentil == true || V2.alarmePerteDeCharge == true || V1.alarmeSurVentil == true || V1.alarmePerteDeCharge == true) {
			genie2.WriteObject(GENIE_OBJ_LED, 10, true);
		}
		break;
	case FORM::OPTIONS_CTA:
		genie2.WriteObject(GENIE_OBJ_4DBUTTON, 6, (int)registreOption);
		genie2.WriteObject(GENIE_OBJ_4DBUTTON, 7, (int)commandeDeporteeOption);
		//genie2.WriteObject(GENIE_OBJ_4DBUTTON, 6, registreOption);
		//genie2.WriteObject(GENIE_OBJ_4DBUTTON, 7, commandeDeporteeOption);
		genie2.WriteObject(GENIE_OBJ_4DBUTTON, 8, (int)GTCOption);
		genie2.WriteObject(GENIE_OBJ_4DBUTTON, 9, (int)alarmeEncrassementFiltreOption);
		genie2.WriteObject(GENIE_OBJ_4DBUTTON, 10, (int)regulSurDepressionOption);
		if (ecranOption == true) genie2.WriteObject(GENIE_OBJ_4DBUTTON, 11, true);
		if (boiteaboutonOption == true) genie2.WriteObject(GENIE_OBJ_4DBUTTON, 11, false);
		genie2.WriteObject(GENIE_OBJ_4DBUTTON, 12, (int)batterieChaudeOption);
		genie2.WriteObject(GENIE_OBJ_4DBUTTON, 13, (int)batterieFroideOption);
		break;
	case FORM::DATE_ET_HEURE:
		if (jourSemaine == 1) stringdate += "Lundi ";
		if (jourSemaine == 2) stringdate += "Mardi ";
		if (jourSemaine == 3) stringdate += "Mercredi ";
		if (jourSemaine == 4) stringdate += "Jeudi ";
		if (jourSemaine == 5) stringdate += "Vendredi ";
		if (jourSemaine == 6) stringdate += "Samedi ";
		if (jourSemaine == 7) stringdate += "Dimanche ";
		stringdate += String(jour);
		stringdate += " ";
		if (mois == 1) stringdate += "Janvier ";
		if (mois == 2) stringdate += "Fevrier ";
		if (mois == 3) stringdate += "Mars ";
		if (mois == 4) stringdate += "Avril ";
		if (mois == 5) stringdate += "Mai ";
		if (mois == 6) stringdate += "Juin ";
		if (mois == 7) stringdate += "Juillet ";
		if (mois == 8) stringdate += "Aout ";
		if (mois == 9) stringdate += "Septembre ";
		if (mois == 10) stringdate += "Octobre ";
		if (mois == 11) stringdate += "Novembre ";
		if (mois == 12) stringdate += "Decembre ";
		stringdate += "20";
		stringdate += String(annee);
		genie2.WriteStr(7, stringdate);
		genie2.WriteObject(GENIE_OBJ_LED_DIGITS, 0, heure);
		genie2.WriteObject(GENIE_OBJ_LED_DIGITS, 1, minute);
		break;
	case FORM::PARAMS_VENTILO:
		genie2.WriteObject(GENIE_OBJ_LED, 0, (int)V1.defaut);
		genie2.WriteObject(GENIE_OBJ_LED, 1, (int)V2.defaut);
		genie2.WriteObject(GENIE_OBJ_4DBUTTON, 0, (int)V1.commandeMarche);
		genie2.WriteObject(GENIE_OBJ_4DBUTTON, 5, (int)V2.commandeMarche);
		if ((modeMarche_Applique == AUTO && modeMarcheAutoOcc == REGULDEPRESSION && occupe) || (modeMarche_Applique == AUTO && modeMarcheAutoInocc == REGULDEPRESSION && !occupe)) genie2.WriteStr(12, V2.consigne_app_Pa);
		else genie2.WriteStr(12, V2.consigne_m3h);
		genie2.WriteStr(10, V1.consigne_m3h);
		genie2.WriteStr(13, V1.Kp);
		genie2.WriteStr(14, V1.Ki);
		genie2.WriteStr(15, V1.Kd);
		genie2.WriteStr(16, V1.Kfactor);
		genie2.WriteStr(17, V2.Kp);
		genie2.WriteStr(18, V2.Ki);
		genie2.WriteStr(19, V2.Kd);
		genie2.WriteStr(20, V2.Kfactor);
		genie2.WriteStr(21, V1.debit);
		genie2.WriteStr(22, V1.cons_app_pc);
		genie2.WriteStr(23, V2.debit);
		genie2.WriteStr(24, V2.cons_app_pc);
		break;
	case FORM::PARAMS_RM:
		genie2.WriteObject(GENIE_OBJ_4DBUTTON, 2, Registreforcage);
		break;
	case FORM::BOUTONS:
		genie2.WriteObject(GENIE_OBJ_USER_LED, 1, Ventilodefaut);
		genie2.WriteObject(GENIE_OBJ_USER_LED, 0, bpmarche);
		genie2.WriteStr(6, consigne_m3h);
		break;
	case FORM::CONFIG_CTA:
		genie2.WriteStr(9, SOUFFLAGE);
		genie2.WriteStr(33, REPRISE);
		genie2.WriteStr(34, EXTRACTION);
		genie2.WriteStr(35, AIRNEUF);
		genie2.WriteStr(36, debitMaxCTA);
		genie2.WriteStr(42, pressionMaxCTA);
		genie2.WriteStr(52, seuilTempHorsGel);
		break;
	case FORM::PARAMS_CTA:
		genie2.WriteObject(GENIE_OBJ_4DBUTTON, 4, (int)!regulTempReprise);
		genie2.WriteObject(GENIE_OBJ_DIPSW, 0, (int)modeMarche_Applique);
		genie2.WriteObject(GENIE_OBJ_4DBUTTON, 3, (int)changeOver);
		genie2.WriteStr(37, TE.consigneTempEteOccupe);
		genie2.WriteStr(38, TH.consigneTempHiverOccupe);
		genie2.WriteStr(5, TE.consigneTempEteInoccupe);
		genie2.WriteStr(8, TH.consigneTempHiverInoccupe); 
		genie2.WriteStr(39, seuilTempHorsGel);
		break;
	case FORM::PARAMS_CTA2:
		genie2.WriteStr(40, consignePressionModeOccupe);
		genie2.WriteStr(41, consignePressionModeInoccupe);
		//	genie2.WriteStr(42, V2.consignePressionModeOccupe);
		//genie2.WriteStr(43, V2.consignePressionModeInoccupe);

		genie2.WriteStr(44, V1.consigneDebitModeOccupe);
		genie2.WriteStr(45, V1.consigneDebitModeInoccupe);
		genie2.WriteStr(46, V1.consigneDebitFreeCooling);
		genie2.WriteStr(49, V2.consigneDebitModeOccupe);
		genie2.WriteStr(50, V2.consigneDebitModeInoccupe);
		genie2.WriteStr(51, V2.consigneDebitFreeCooling);
		break;
	case FORM::PARAMS_CTA3:
		if (modeMarcheAutoOcc == 0)  stringModeMarche = "Regulation debit";
		if (modeMarcheAutoOcc == 1)  stringModeMarche = "Regulation sur depression";
		if (modeMarcheAutoOcc == 2)  stringModeMarche = "Regulation commande deportee";
		genie2.WriteStr(70, stringModeMarche);
		genie2.WriteObject(GENIE_OBJ_DIPSW, 8, modeMarcheAutoOcc);
		if (modeMarcheAutoInocc == 0)  stringModeMarche = "Regulation debit";
		if (modeMarcheAutoInocc == 1)  stringModeMarche = "Regulation sur depression";
		if (modeMarcheAutoInocc == 2)  stringModeMarche = "Regulation commande deportee";
		genie2.WriteStr(71, stringModeMarche);
		genie2.WriteObject(GENIE_OBJ_DIPSW, 9, modeMarcheAutoInocc);


		if (String(heureDebutOccupe).length() == 4) {
			temp = String(heureDebutOccupe).substring(0, 2); temp += ":"; temp += String(heureDebutOccupe).substring(2);
		}
		else {
			temp = String(heureDebutOccupe).substring(0, 1); temp += ":"; temp += String(heureDebutOccupe).substring(1);
		}
		genie2.WriteStr(47, temp);
		if (String(heureFinOccupe).length() == 4) {
			temp = String(heureFinOccupe).substring(0, 2); temp += ":"; temp += String(heureFinOccupe).substring(2);
		}
		else {
			temp = String(heureFinOccupe).substring(0, 1); temp += ":"; temp += String(heureFinOccupe).substring(1);
		}
		genie2.WriteStr(48, temp);
		for (int i = 0; i < 7; i++) {
			genie2.WriteObject(GENIE_OBJ_DIPSW, i + 1, (int)jourOccupe[i]);
		}
		break;
	case FORM::ECRAN:
		genie2.WriteStr(63, V1.cons_app_pc);
		genie2.WriteStr(65, debitMaxCTA / 100 * (slideConsigne_m3h));
		genie2.WriteStr(64, V2.cons_app_pc);
		genie2.WriteObject(GENIE_OBJ_COOL_GAUGE, 0, ((V2.debit + V1.debit) / 2));
		genie2.WriteObject(GENIE_OBJ_SLIDER, 0, slideConsigne_m3h);
		genie2.WriteObject(GENIE_OBJ_GAUGE, 0, V1.cons_app_pc);
		genie2.WriteObject(GENIE_OBJ_GAUGE, 1, V2.cons_app_pc);
		if (V1.defaut == true || V2.defaut == true) {
			genie2.WriteObject(GENIE_OBJ_LED, 8, true);
		}
		else	genie2.WriteObject(GENIE_OBJ_LED, 8, false);
		if (V1.alarmePeriode == true || V2.alarmePeriode == true || V2.alarmeSurVentil == true || V2.alarmePerteDeCharge == true || V1.alarmeSurVentil == true || V1.alarmePerteDeCharge == true) {
			genie2.WriteObject(GENIE_OBJ_LED, 9, true);
		}
		else genie2.WriteObject(GENIE_OBJ_LED, 9, false);
	case FORM::FILTRE:
		genie2.WriteObject(GENIE_OBJ_LED, 2, (int)V1.alarmePeriode);
		genie2.WriteObject(GENIE_OBJ_LED, 3, (int)V2.alarmePeriode);
		genie2.WriteObject(GENIE_OBJ_LED, 4, (int)V2.alarmeSurVentil);
		genie2.WriteObject(GENIE_OBJ_LED, 5, (int)V2.alarmePerteDeCharge);
		genie2.WriteObject(GENIE_OBJ_LED, 6, (int)V1.alarmeSurVentil);
		genie2.WriteObject(GENIE_OBJ_LED, 7, (int)V1.alarmePerteDeCharge);
		genie2.WriteStr(66, alarmePeriode);
		genie2.WriteStr(67, alarmeSurVentil);
		stringFiltre = String(V1.jourChangementFiltre);
		stringFiltre += "/";
		stringFiltre += String(V1.moisChangementFiltre);
		stringFiltre += "/";
		stringFiltre += String(V1.anneeChangementFiltre);
		genie2.WriteStr(68, stringFiltre);
		stringFiltre = String(V2.jourChangementFiltre);
		stringFiltre += "/";
		stringFiltre += String(V2.moisChangementFiltre);
		stringFiltre += "/";
		stringFiltre += String(V2.anneeChangementFiltre);
		genie2.WriteStr(69, stringFiltre);
	case FORM::BATTERIES:
		genie2.WriteObject(GENIE_OBJ_4DBUTTON, 14, (int)BC.commandeMarche);
		genie2.WriteObject(GENIE_OBJ_4DBUTTON, 15, (int)BF.commandeMarche);
		genie2.WriteStr(30, tempsMaxBatChaud);
		genie2.WriteStr(43, tempsMinBatChaud);
		genie2.WriteStr(59, tempsMaxBatFroid);
		genie2.WriteStr(60, tempsMinBatFroid);
		genie2.WriteStr(61, BC.cons_app_pc);
		genie2.WriteStr(62, BF.cons_app_pc);
		genie2.WriteStr(74, BC.Kp);
		genie2.WriteStr(75, BF.Kp);
		genie2.WriteStr(72, BC.Ki);
		genie2.WriteStr(73, BF.Ki); 
		genie2.WriteStr(76, BC.Kd);
		genie2.WriteStr(77, BF.Kd);
		genie2.WriteStr(78, TH.Kp);
		genie2.WriteStr(83, TE.Kp);
		genie2.WriteStr(79, TH.Ki);
		genie2.WriteStr(82, TE.Ki);
		genie2.WriteStr(80, TH.Kd);
		genie2.WriteStr(81, TE.Kd);
		break;
	}

}

void writeScreen() {
	String timeString = String(jour); timeString.concat("/"); timeString.concat(String(mois)); timeString += "/"; timeString.concat(String(annee)); timeString += " "; timeString.concat(String(heure)); timeString += ":"; timeString.concat(String(minute));
	String modeString = "Mode: ";
	String temp = "";
	String stringdate = "";
	String stringRegistre = "";
	String stringFiltre = "";
	String stringModeMarche = "";
	switch (form) {
	case FORM::PARAMS_RESEAU:
		genie.WriteStr(32, adresseIP);
		genie.WriteStr(55, passerelle);
		genie.WriteStr(56, tempoModbus);
		break;
	case FORM::MENU:
		genie.WriteStr(58, timeString);
		break;
	case FORM::MENU_CMD:
		genie.WriteStr(54, timeString);
		break;
	case FORM::MENUADMIN_FULL:
		genie.WriteStr(31, versioncontrollino);
		break;
	case FORM::SYNOPTIQUE:
		if (modeMarche_Applique == modeMarche::ARRET) modeString += "ARRET, ";
		if (modeMarche_Applique == modeMarche::MANU) modeString += "MANU, ";
		if (modeMarche_Applique == modeMarche::AUTO) modeString += "AUTO, ";
		if (occupe == true) modeString += "Occupe, ";
		else modeString += "Inoccupe, ";
		if (changeOver == true) modeString += "Ete,";
		else modeString += "Hiver,";
		if (modeApplique == HORSGEL)modeString += "\n\r	HorsGel";
		if (modeApplique == FREECOOLING)modeString += "\n\r FREECOOLING";
		if (modeApplique == FREEHEATING)modeString += "\n\r FREEHEATING";
		genie.WriteStr(0, temperature[SOUFFLAGE].value);
		genie.WriteStr(1, temperature[REPRISE].value);
		genie.WriteStr(2, temperature[EXTRACTION].value);
		genie.WriteStr(3, temperature[AIRNEUF].value);
		genie.WriteStr(5, P3.readAna());
		genie.WriteStr(26, V1.debit);
		genie.WriteStr(25, V2.debit);
		if (registreOption == false) stringRegistre = "not connected";
		else {
			if (RegistreONOFF == false) stringRegistre = "close";
			else stringRegistre = "open";
		}
		genie.WriteStr(27, stringRegistre);
		genie.WriteStr(57, modeString);
		genie.WriteStr(6, modeApplique);
		if (V1.alarmePeriode == true || V2.alarmePeriode == true || V2.alarmeSurVentil == true || V2.alarmePerteDeCharge == true || V1.alarmeSurVentil == true || V1.alarmePerteDeCharge == true) {
			genie.WriteObject(GENIE_OBJ_LED, 10, true);
		}
		break;
	case FORM::OPTIONS_CTA:
		genie.WriteObject(GENIE_OBJ_4DBUTTON, 6, (int)registreOption);
		genie.WriteObject(GENIE_OBJ_4DBUTTON, 7, (int)commandeDeporteeOption);
		//genie.WriteObject(GENIE_OBJ_4DBUTTON, 6, registreOption);
		//genie.WriteObject(GENIE_OBJ_4DBUTTON, 7, commandeDeporteeOption);
		genie.WriteObject(GENIE_OBJ_4DBUTTON, 8, (int)GTCOption);
		genie.WriteObject(GENIE_OBJ_4DBUTTON, 9, (int)alarmeEncrassementFiltreOption);
		genie.WriteObject(GENIE_OBJ_4DBUTTON, 10, (int)regulSurDepressionOption);
		if (ecranOption == true) genie.WriteObject(GENIE_OBJ_4DBUTTON, 11, true);
		if (boiteaboutonOption == true) genie.WriteObject(GENIE_OBJ_4DBUTTON, 11, false);
		genie.WriteObject(GENIE_OBJ_4DBUTTON, 12, (int)batterieChaudeOption);
		genie.WriteObject(GENIE_OBJ_4DBUTTON, 13, (int)batterieFroideOption);

		break;
	case FORM::DATE_ET_HEURE:
		if (jourSemaine == 1) stringdate += "Lundi ";
		if (jourSemaine == 2) stringdate += "Mardi ";
		if (jourSemaine == 3) stringdate += "Mercredi ";
		if (jourSemaine == 4) stringdate += "Jeudi ";
		if (jourSemaine == 5) stringdate += "Vendredi ";
		if (jourSemaine == 6) stringdate += "Samedi ";
		if (jourSemaine == 7) stringdate += "Dimanche ";
		stringdate += String(jour);
		stringdate += " ";
		if (mois == 1) stringdate += "Janvier ";
		if (mois == 2) stringdate += "Fevrier ";
		if (mois == 3) stringdate += "Mars ";
		if (mois == 4) stringdate += "Avril ";
		if (mois == 5) stringdate += "Mai ";
		if (mois == 6) stringdate += "Juin ";
		if (mois == 7) stringdate += "Juillet ";
		if (mois == 8) stringdate += "Aout ";
		if (mois == 9) stringdate += "Septembre ";
		if (mois == 10) stringdate += "Octobre ";
		if (mois == 11) stringdate += "Novembre ";
		if (mois == 12) stringdate += "Decembre ";
		stringdate += "20";
		stringdate += String(annee);
		genie.WriteStr(7, stringdate);
		genie.WriteObject(GENIE_OBJ_LED_DIGITS, 0, heure);
		genie.WriteObject(GENIE_OBJ_LED_DIGITS, 1, minute);
		break;
	case FORM::PARAMS_VENTILO:
		genie.WriteObject(GENIE_OBJ_LED, 0, (int)V1.defaut);
		genie.WriteObject(GENIE_OBJ_LED, 1, (int)V2.defaut);
		genie.WriteObject(GENIE_OBJ_4DBUTTON, 0, (int)V1.commandeMarche);
		genie.WriteObject(GENIE_OBJ_4DBUTTON, 5, (int)V2.commandeMarche);
		if ((modeMarche_Applique == AUTO && modeMarcheAutoOcc == REGULDEPRESSION && occupe) || (modeMarche_Applique == AUTO && modeMarcheAutoInocc == REGULDEPRESSION && !occupe)) genie.WriteStr(12, V2.consigne_app_Pa);
			else genie.WriteStr(12, V2.consigne_m3h);
		genie.WriteStr(10, V1.consigne_m3h);
		genie.WriteStr(13, V1.Kp);
		genie.WriteStr(14, V1.Ki);
		genie.WriteStr(15, V1.Kd);
		genie.WriteStr(16, V1.Kfactor);
		genie.WriteStr(17, V2.Kp);
		genie.WriteStr(18, V2.Ki);
		genie.WriteStr(19, V2.Kd);
		genie.WriteStr(20, V2.Kfactor);
		genie.WriteStr(21, V1.debit);
		genie.WriteStr(22, V1.cons_app_pc);
		genie.WriteStr(23, V2.debit);
		genie.WriteStr(24, V2.cons_app_pc);
		break;
	case FORM::PARAMS_RM:
		genie.WriteObject(GENIE_OBJ_4DBUTTON, 2, Registreforcage);
		break;
	case FORM::BOUTONS:
		genie.WriteObject(GENIE_OBJ_USER_LED, 1, Ventilodefaut);
		genie.WriteObject(GENIE_OBJ_USER_LED, 0, bpmarche);
		genie.WriteStr(6, consigne_m3h);
		break;
	case FORM::CONFIG_CTA:
		genie.WriteStr(9, SOUFFLAGE);
		genie.WriteStr(33, REPRISE);
		genie.WriteStr(34, EXTRACTION);
		genie.WriteStr(35, AIRNEUF);
		genie.WriteStr(36, debitMaxCTA);
		genie.WriteStr(42, pressionMaxCTA);
		genie.WriteStr(52, seuilTempHorsGel);
		break;
	case FORM::PARAMS_CTA:
		genie.WriteObject(GENIE_OBJ_4DBUTTON, 4, (int)!regulTempReprise);
		genie.WriteObject(GENIE_OBJ_DIPSW, 0, (int)modeMarche_Applique);
		genie.WriteObject(GENIE_OBJ_4DBUTTON, 3, (int)changeOver);
		genie.WriteStr(37, TE.consigneTempEteOccupe);
		genie.WriteStr(38, TH.consigneTempHiverOccupe);
		genie.WriteStr(5, TE.consigneTempEteInoccupe);
		genie.WriteStr(8, TH.consigneTempHiverInoccupe);
		genie.WriteStr(39, seuilTempHorsGel);
		break;
	case FORM::PARAMS_CTA2:
		genie.WriteStr(40, consignePressionModeOccupe);
		genie.WriteStr(41, consignePressionModeInoccupe);
		//	genie.WriteStr(42, V2.consignePressionModeOccupe);
		//genie.WriteStr(43, V2.consignePressionModeInoccupe);

		genie.WriteStr(44, V1.consigneDebitModeOccupe);
		genie.WriteStr(45, V1.consigneDebitModeInoccupe);
		genie.WriteStr(46, V1.consigneDebitFreeCooling);
		genie.WriteStr(49, V2.consigneDebitModeOccupe);
		genie.WriteStr(50, V2.consigneDebitModeInoccupe);
		genie.WriteStr(51, V2.consigneDebitFreeCooling);
		break;
	case FORM::PARAMS_CTA3:
		if (modeMarcheAutoOcc == 0)  stringModeMarche = "Regulation debit";
		if (modeMarcheAutoOcc == 1)  stringModeMarche = "Regulation sur depression";
		if (modeMarcheAutoOcc == 2)  stringModeMarche = "Regulation commande deportee";
		genie.WriteStr(70, stringModeMarche);
		genie.WriteObject(GENIE_OBJ_DIPSW, 8, modeMarcheAutoOcc);
		if (modeMarcheAutoInocc == 0)  stringModeMarche = "Regulation debit";
		if (modeMarcheAutoInocc == 1)  stringModeMarche = "Regulation sur depression";
		if (modeMarcheAutoInocc == 2)  stringModeMarche = "Regulation commande deportee";
		genie.WriteStr(71, stringModeMarche);
		genie.WriteObject(GENIE_OBJ_DIPSW, 9, modeMarcheAutoInocc);
		

		if (String(heureDebutOccupe).length() == 4) {
			temp = String(heureDebutOccupe).substring(0, 2); temp += ":"; temp += String(heureDebutOccupe).substring(2);
		}
		else {
			temp = String(heureDebutOccupe).substring(0, 1); temp += ":"; temp += String(heureDebutOccupe).substring(1);
		}
		genie.WriteStr(47, temp);
		if (String(heureFinOccupe).length() == 4) {
			temp = String(heureFinOccupe).substring(0, 2); temp += ":"; temp += String(heureFinOccupe).substring(2);
		}
		else {
			temp = String(heureFinOccupe).substring(0, 1); temp += ":"; temp += String(heureFinOccupe).substring(1);
		}
		genie.WriteStr(48, temp);
		for (int i = 0; i < 7; i++) {
			genie.WriteObject(GENIE_OBJ_DIPSW, i + 1, (int)jourOccupe[i]);
		}
		break;
	case FORM::ECRAN:
		genie.WriteStr(63, V1.cons_app_pc);
		genie.WriteStr(65, debitMaxCTA / 100 * (slideConsigne_m3h));
		genie.WriteStr(64, V2.cons_app_pc);
		genie.WriteObject(GENIE_OBJ_COOL_GAUGE, 0, ((V2.debit + V1.debit) / 2));
		genie.WriteObject(GENIE_OBJ_SLIDER, 0, slideConsigne_m3h);
		genie.WriteObject(GENIE_OBJ_GAUGE, 0, V1.cons_app_pc);
		genie.WriteObject(GENIE_OBJ_GAUGE, 1, V2.cons_app_pc);
		if (V1.defaut == true || V2.defaut == true) {
			genie.WriteObject(GENIE_OBJ_LED, 8, true);
		}
		else	genie.WriteObject(GENIE_OBJ_LED, 8, false);
		if (V1.alarmePeriode == true || V2.alarmePeriode == true || V2.alarmeSurVentil == true || V2.alarmePerteDeCharge == true || V1.alarmeSurVentil == true || V1.alarmePerteDeCharge == true) {
			genie.WriteObject(GENIE_OBJ_LED, 9, true);
		}
		else genie.WriteObject(GENIE_OBJ_LED, 9, false);
	case FORM::FILTRE:
		genie.WriteObject(GENIE_OBJ_LED, 2, (int)V1.alarmePeriode);
		genie.WriteObject(GENIE_OBJ_LED, 3, (int)V2.alarmePeriode);
		genie.WriteObject(GENIE_OBJ_LED, 4, (int)V2.alarmeSurVentil);
		genie.WriteObject(GENIE_OBJ_LED, 5, (int)V2.alarmePerteDeCharge);
		genie.WriteObject(GENIE_OBJ_LED, 6, (int)V1.alarmeSurVentil);
		genie.WriteObject(GENIE_OBJ_LED, 7, (int)V1.alarmePerteDeCharge);
		genie.WriteStr(66, alarmePeriode);
		genie.WriteStr(67, alarmeSurVentil);
		stringFiltre = String(V1.jourChangementFiltre);
		stringFiltre += "/";
		stringFiltre += String(V1.moisChangementFiltre);
		stringFiltre += "/";
		stringFiltre += String(V1.anneeChangementFiltre);
		genie.WriteStr(68, stringFiltre);
		stringFiltre = String(V2.jourChangementFiltre);
		stringFiltre += "/";
		stringFiltre += String(V2.moisChangementFiltre);
		stringFiltre += "/";
		stringFiltre += String(V2.anneeChangementFiltre);
		genie.WriteStr(69, stringFiltre);
	case FORM::BATTERIES:
		genie.WriteObject(GENIE_OBJ_4DBUTTON, 14, (int)BC.commandeMarche);
		genie.WriteObject(GENIE_OBJ_4DBUTTON, 15, (int)BF.commandeMarche);
		genie.WriteStr(30, tempsMaxBatChaud);
		genie.WriteStr(43, tempsMinBatChaud);
		genie.WriteStr(59, tempsMaxBatFroid);
		genie.WriteStr(60, tempsMinBatFroid);
		genie.WriteStr(61, BC.cons_app_pc);
		genie.WriteStr(62, BF.cons_app_pc);
		genie.WriteStr(74, BC.Kp);
		genie.WriteStr(75, BF.Kp);
		genie.WriteStr(72, BC.Ki);
		genie.WriteStr(73, BF.Ki);
		genie.WriteStr(76, BC.Kd);
		genie.WriteStr(77, BF.Kd);
		genie.WriteStr(78, TH.Kp);
		genie.WriteStr(83, TE.Kp);
		genie.WriteStr(79, TH.Ki);
		genie.WriteStr(82, TE.Ki);
		genie.WriteStr(80, TH.Kd);
		genie.WriteStr(81, TE.Kd);
		break;
	}

}

/**
* Fonction de lecture de la temp?rature via un capteur DS18B20.
*/
/*
byte getTemperature(int sensorId) {
//sensors.requestTemperatures();
//byte data[9];
/* Reset le bus 1-Wire et s?lectionne le capteur */
//ds.reset();
//ds.select(temperature[sensorId].addr);
/* Lance une prise de mesure de temp?rature et attend la fin de la mesure */
//ds.write(0x44, 1);
//delay(50);
/* Reset le bus 1-Wire, s?lectionne le capteur et envoie une demande de lecture du scratchpad */
//ds.reset();
//ds.select(temperature[sensorId].addr);
//ds.write(0xBE);
/* Lecture du scratchpad */
//for (byte i = 0; i < 4; i++) {
//data[i] = ds.read();
//Serial.println(sensors.getTempCByIndex(sensorId));
/*Serial.print("\n\r i=");
Serial.print(i);
Serial.print("data");
Serial.print(data[i]);*/
//}
/* Calcul de la temp?rature en degr? Celsius */
//temperature[sensorId].value = ((data[1] << 8) | data[0]) * 0.0625;
//temperature[sensorId].value = sensors.getTempCByIndex(sensorId);
// Pas d'erreur
//Serial.print("\n\r value:");
//Serial.print(temperature[sensorId].value);
//Serial.print("\n\r okkkkkay");
//return READ_OK;
//}

/**
* Fonction de lecture de la temp?rature via un capteur DS18B20.
*/
byte getDS18B20Addresses(int nbSensors) {
	ds.reset_search();

	for (int i = 0; i < nbSensors; i++) {
		/* Recherche le prochain capteur 1-Wire disponible */
		if (!ds.search(temperature[i].addr)) {
			// Pas de capteur
			temperature[i].status = NO_SENSOR_FOUND;
			Serial.print("temperature[i].status = NO_SENSOR_FOUND");
			Serial.println(temperature[i].addr[0]);
		}
		// Verifie que l'adresse a ete correctement recue 
		if (OneWire::crc8(temperature[i].addr, 7) != temperature[i].addr[7]) {
			// Adresse invalide
			temperature[i].status = INVALID_ADDRESS;
			Serial.print("temperature[i].status = INVALID_ADDRESS");
		}
		// Verifie qu'il s'agit bien d'un DS18B20 
		if (temperature[i].addr[0] != 0x28) {
			// Mauvais type de capteur
			temperature[i].status = INVALID_SENSOR;
			Serial.print("temperature[i].status = INVALID_SENSOR");
		}
		//Serial.print(i); Serial.print(":"); Serial.println(temperature[i].status); Serial.print(":"); Serial.println(temperature[i].addr[0]);
		temperature[i].status = READ_OK;
	}
}

