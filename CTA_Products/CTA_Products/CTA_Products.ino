﻿

//#include "PID_one.h"

//#include <EEPROM.h>
#include <EEPROMVar.h>
#include <EEPROMex.h>
#include <SPI.h>
#include "genieArduino.h"
#include "ModbusIP.h"
#include "CTALib.h"
#include <DallasTemperature.h>
#include <OneWire.h>
#include <PID_v1.h>
#include <CONTROLLINO.h>


#define NB_JOURS_ANNEES_PRECEDENTES(a) (((a)*365) + (((a)+3)>>2))
#define PAS_BISSEXTILE(a) ((a)&3)
// VARIABLES GLOBALES

#define memoryBase 32

//PIN LAYOUT
#define pin_DEFAUT_V1 10                   //V2= REPRISE V1= SOUFF			//A check 
#define pin_CMD_RM1 7
#define pin_ONOFF_V1 5
#define pin_ONOFF_V2 6
#define pin_CMD_V1 12
#define pin_TEMPERATURE A13 
#define pin_DEFAUT_V2 11
#define pin_CMD_V2 13
#define pin_P1 A14		
#define pin_P2 A15	
#define pin_P3 A6  
#define RESETLINE 43
#define RESETLINE2 45
#define BP_MARCHE A10
#define BP_ARRET A11
#define POTAR A0
#define VOYANT_MARCHE 3
#define VOYANT_DEFAUT 2
#define PRESSOSTAT_P1 A11
#define PRESSOSTAT_P2 A12

int codeAdmin = 9876;
int codeUser = 1234;
bool newMdp = false;

bool userLoggedIn = false;
bool adminLoggedIn = false;





/*TIMER*/
int TIME = 0;
int tempsRestantMin = 0;
int tempsRestantSec = 0;

int defautFiltre = 0, defautFiltre_memo = 0;


/*TEMPERATURES*/
OneWire ds(pin_TEMPERATURE); //Cr?ation de l'objet OneWire pour manipuler le bus 1-Wire
DallasTemperature sensors(&ds);
int SOUFFLAGE = 0;
int REPRISE = 1;
int EXTRACTION = 2;
int AIRNEUF = 3; 
int SOUFFLAGE_memo = 0;
int REPRISE_memo = 1;
int EXTRACTION_memo = 2;
int AIRNEUF_memo = 3;

int seuilTempHorsGel = 5;
int debitMaxCTA = 6000;
int debitMaxCTA_memo = 6000;

int alarmePeriode;
int alarmeSurVentil;
int alarmePeriode_memo;
int alarmeSurVentil_memo;

bool occupe = false; //true = Occupe; false = inoccupe

bool KB = false;
bool changedParams = false;

bool GTCLive = true;

int heure;
int minute;
int annee;
int mois;
int jour;
int jourSemaine;


int heuremodif;
int minutemodif;
int anneemodif;
int moismodif;
int jourmodif;
int jourSemainemodif;
int anneeFiltre;

typedef struct {
	float value;
	byte addr[8];
	byte status;
}temperatureDS;
temperatureDS temperature[4];

/* Code de retour de la fonction getTemperature() */
enum DS18B20_RCODES {
	READ_OK,
	NO_SENSOR_FOUND,
	INVALID_ADDRESS,
	INVALID_SENSOR
};

/*
* ECRAN 4D SYSTEMS
*/
Genie genie;
static unsigned long waitPeriod;
static unsigned long waitPeriodTemps;
static unsigned long waitPeriodDate;
double *ptr;
int *int_ptr;
String * string_ptr;


int int_KBmin, int_KBmax;
double double_KBmin, double_KBmax;

bool readIntFromKB = false;
bool readStrFromKB = false;
bool readHeureFromKB = false;
bool erreurKB = false;
bool bpmarche = false;
bool bparret = false;
bool ecranbparret;
bool ecranbpmarche;

bool Ventilodefaut = false;
bool RegistreONOFF = false;
bool Registreforcage = false;
bool savedate = false;
bool modifdate = false;

int consigne_m3h;

double KpV1, KpV2, KiV1, KiV2, KdV1, KdV2, debitV1, debitV2;
int cons_app_pcV1, cons_app_pcV2, KfactorV1, KfactorV2, consigne_m3hV1, consigne_m3hV2;

String str = "";
String param = "";
String versioncontrollino = "CTA_Products.ino";
String IPC = "";
String IGW = "";
int slideConsigne_m3h;
bool FormTemperatureActivated = false;
enum FORM {
	PARAMS_RESEAU = 0,
	MENU_CMD = 1,
	MENUADMIN_FULL = 17,
	SYNOPTIQUE = 3,
	PARAMS_VENTILO = 4,
	KEYBOARD = 5,
	PARAMS_CTA = 6,
	CONFIG_CTA = 8,
	DATE_ET_HEURE = 12,
	MENU = 14,
	FILTRE = 19
};

FORM form = MENU;
int form_memo = 50;	// form précédente >> pour actualisasion de l'écran
/*
* PARAMETRES CTA
*/
enum modeMarche {
	ARRET,
	MANU,
	AUTO
};
enum modeMarcheAuto {
	INOCCUPE = 2,
	OCCUPE = 1,
	RECYCLAGE = 4
};


modeMarche modeMarche_Applique = ARRET;
modeMarcheAuto modeMarcheAuto_Applique = INOCCUPE;
int consignesTemp[4];

int heureDebutOccupe;
int heureFinOccupe;
bool jourOccupe[7];
int jourOccupe_int;

/*
* EBM PAPST ROTOR Dimension: 250 / 280 / 310 / 355 / 400 / 450 / 500 / 560 / 630 / 710 / 800 / 900
*                  K-FACTOR:  76 / 77 / 116 / 148 / 188 / 240 / 281 / 348 / 438 / 545 / 695 / 900
*/

Ventilo V2(pin_ONOFF_V2, pin_CMD_V2, pin_DEFAUT_V2, 240);
Ventilo V1(pin_ONOFF_V1, pin_CMD_V1, pin_DEFAUT_V1, 240);



Pression P1(pin_P1);
Pression P2(pin_P2);

//ModbusIP object
ModbusIP mb;
byte mac[] = { 0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED };
byte ip[] = { 192, 168, 1, 55 };
byte mydns[] = { 8, 8, 8, 8 };
byte gw[] = { 192, 168, 1, 1 };
String adresseIP, passerelle;
String adresseIP_memo, passerelle_memo;

String stringFiltre1 = "", stringFiltre2 = "";

String timeString_memo, stringdate_memo, problemeString_memo, modeString_memo;
double tempSoufflage_memo, tempReprise_memo, tempExtraction_memo, tempAirneuf_memo;
//double tempSoufflage = 0.0, tempReprise = 0.0, tempExtraction = 0.0, tempAirneuf = 0.0;
int debitV1_memo, debitV2_memo;
int consigneDebitModeOccupeV1_memo, consigneDebitModeOccupeV2_memo;
int consigneDebitModeInoccupeV1_memo, consigneDebitModeInoccupeV2_memo;
int consigneDebitFreeCoolingV1_memo, consigneDebitFreeCoolingV2_memo;

unsigned long tempoSec = 0;

bool testAfficheParamsCTA = true;
bool testAfficheParamsCTA2 = true;
bool testAfficheConfigCTA = false;
bool testAfficheFiltre = false, testAfficheFiltre1 = false, testAfficheFiltre2 = false;
bool testAfficheRes = false;
bool testAfficheMenuAdmin = false;
bool testAfficheParamsVentilo = false;

bool finTempo = true;
bool timeOK = true;

unsigned long debutTempoModbus = 0;
unsigned long tempoModbus = 500;

unsigned long tempoRecyclage = 15;//15 min

void setModbus() {
	//Config Modbus IP
	mb.config(mac, ip, mydns, gw);
	mb.addHreg(100);	//modeMarche_Applique
	mb.addHreg(101);	//modeMarcheAuto_Applique
	mb.addHreg(102);	//temperature[REPRISE].value * 100
	mb.addHreg(103);	//temperature[EXTRACTION].value * 100
	mb.addHreg(104);	//temperature[AIRNEUF].value * 100
	mb.addHreg(105);	//temperature[SOUFFLAGE].value * 100
	mb.addHreg(106);	//V1.debit
	mb.addHreg(107);	//V2.debit
	mb.addCoil(108);	//V1.alarmePeriode
	mb.addCoil(109);	//V2.alarmePeriode
	mb.addCoil(110);	//V1.alarmeSurVentil
	mb.addCoil(111);	//V2.alarmeSurVentil
	mb.addHreg(112);	//V1.consigneDebitFreeCooling
	mb.addHreg(113);	//V1.consigneDebitModeOccupe
	mb.addHreg(114);	//V1.consigneDebitModeInoccupe
	mb.addHreg(115);	//V1.consigne_app_m3h
	mb.addHreg(116);	//V1.Kp
	mb.addHreg(117);	//V1.Ki
	mb.addHreg(118);	//V1.Kd
	mb.addHreg(119);	//V1.Kfactor
	mb.addCoil(120);	//V1.marche
	mb.addHreg(121);	//V1.cons_app_pc
	mb.addCoil(122);	//V1.commandeMarche
	mb.addHreg(123);	//V2.consigneDebitFreeCooling
	mb.addHreg(124);	//V2.consigneDebitModeOccupe
	mb.addHreg(125);	//V2.consigneDebitModeInoccupe
	mb.addHreg(126);	//V2.consigne_app_m3h
	mb.addHreg(127);	//V2.Kp
	mb.addHreg(128);	//V2.Ki
	mb.addHreg(129);	//V2.Kd
	mb.addHreg(130);	//V2.Kfactor
	mb.addCoil(131);	//V2.marche
	mb.addHreg(132);	//V2.cons_app_pc
	mb.addCoil(133);	//V2.commandeMarche
	mb.addHreg(134);	//tempoRecyclage
	mb.addHreg(135);	//debitMaxCTA
	mb.addHreg(136);	//alarmePeriode
	mb.addHreg(137);	//alarmeSurVentil
	mb.addHreg(138);	//jour
	mb.addHreg(139);	//mois
	mb.addHreg(140);	//annee
	mb.addHreg(141);	//minute
	mb.addHreg(142);	//heure
	mb.addHreg(143);	//jour de la semaine
	mb.addHreg(144);	//V1.jourChangementFiltre
	mb.addHreg(145);	//V1.moisChangementFiltre
	mb.addHreg(146);	//V1.anneeChangementFiltre
	mb.addHreg(147);	//V2.jourChangementFiltre
	mb.addHreg(148);	//V2.moisChangementFiltre
	mb.addHreg(149);	//V2.anneeChangementFiltre
	mb.addHreg(150);	//codeUser
}

void readFromGTC() {

	modeMarche_Applique = (modeMarche)mb.Hreg(100);
	modeMarcheAuto_Applique = (modeMarcheAuto)mb.Hreg(101);

	V1.consigneDebitFreeCooling = mb.Hreg(112);
	V1.consigneDebitModeOccupe = mb.Hreg(113);
	V1.consigneDebitModeInoccupe = mb.Hreg(114);
	V1.Kp = (double)(mb.Hreg(116)) / 100;
	V1.Ki = (double)(mb.Hreg(117)) / 100;
	V1.Kd = (double)(mb.Hreg(118)) / 100;
	V1.Kfactor = mb.Hreg(119);
	V1.commandeMarche = (bool)mb.Coil(122);
	V2.consigneDebitFreeCooling = mb.Hreg(123);
	V2.consigneDebitModeOccupe = mb.Hreg(124);
	V2.consigneDebitModeInoccupe = mb.Hreg(125);
	V2.Kp = (double)(mb.Hreg(127)) / 100;
	V2.Ki = (double)(mb.Hreg(128)) / 100;
	V2.Kd = (double)(mb.Hreg(129)) / 100;
	V2.Kfactor = mb.Hreg(130);
	V2.commandeMarche = (bool)mb.Coil(133);

	tempoRecyclage = mb.Hreg(134);
	debitMaxCTA = mb.Hreg(135);
	alarmePeriode = mb.Hreg(136);
	alarmeSurVentil = mb.Hreg(137);

	if (mb.Hreg(138) != 0) {
		heure = mb.Hreg(142);
		minute = mb.Hreg(141);
		annee = mb.Hreg(140);
		mois = mb.Hreg(139);
		jour = mb.Hreg(138);
		jourSemaine = mb.Hreg(143);
	}

	V1.jourChangementFiltre = mb.Hreg(144);
	V1.moisChangementFiltre = mb.Hreg(145);
	V1.anneeChangementFiltre = mb.Hreg(146);
	V2.jourChangementFiltre = mb.Hreg(147);
	V2.moisChangementFiltre = mb.Hreg(148);
	V2.anneeChangementFiltre = mb.Hreg(149);
	codeUser = mb.Hreg(150);
		
}


void writeToGTC_variables() {
	mb.Hreg(102, (word)(temperature[REPRISE].value * 100));
	mb.Hreg(103, (word)(temperature[EXTRACTION].value * 100));
	mb.Hreg(104, (word)(temperature[AIRNEUF].value * 100));
	mb.Hreg(105, (word)(temperature[SOUFFLAGE].value * 100));
	mb.Hreg(106, V1.debit);
	mb.Hreg(107, V2.debit);
	mb.Coil(108, V1.alarmePeriode);
	mb.Coil(109, V2.alarmePeriode);
	mb.Coil(110, V1.alarmeSurVentil);
	mb.Coil(111, V2.alarmeSurVentil);


	mb.Hreg(115, V1.consigne_app_m3h);
	mb.Hreg(121, V1.cons_app_pc);
	mb.Hreg(126, V2.consigne_app_m3h);
	mb.Hreg(132, V2.cons_app_pc);
	mb.Coil(131, V2.marche);
	mb.Coil(120, V1.marche);
}

//void(*resetFunc) (void) = 0;

void writeToGTC_params() {

	mb.Hreg(100, modeMarche_Applique);
	mb.Hreg(101, modeMarcheAuto_Applique);

	mb.Hreg(112, V1.consigneDebitFreeCooling);
	mb.Hreg(113, V1.consigneDebitModeOccupe);
	mb.Hreg(114, V1.consigneDebitModeInoccupe);
	mb.Hreg(116, (word)(V1.Kp * 100));
	mb.Hreg(117, (word)(V1.Ki * 100));
	mb.Hreg(118, (word)(V1.Kd * 100));
	mb.Hreg(119, V1.Kfactor);
	mb.Coil(122, V1.commandeMarche);
	mb.Hreg(123, V2.consigneDebitFreeCooling);
	mb.Hreg(124, V2.consigneDebitModeOccupe);
	mb.Hreg(125, V2.consigneDebitModeInoccupe);
	mb.Hreg(127, (word)(V2.Kp * 100));
	mb.Hreg(128, (word)(V2.Ki * 100));
	mb.Hreg(129, (word)(V2.Kd * 100));
	mb.Hreg(130, V2.Kfactor);
	mb.Coil(133, V2.commandeMarche);

	mb.Hreg(134, tempoRecyclage);
	mb.Hreg(135, debitMaxCTA);
	mb.Hreg(136, alarmePeriode);
	mb.Hreg(137, alarmeSurVentil);

	mb.Hreg(144, V1.jourChangementFiltre);
	mb.Hreg(145, V1.moisChangementFiltre);
	mb.Hreg(146, V1.anneeChangementFiltre);
	mb.Hreg(147, V2.jourChangementFiltre);
	mb.Hreg(148, V2.moisChangementFiltre);
	mb.Hreg(149, V2.anneeChangementFiltre);
	mb.Hreg(150, codeUser);
}

void modbus() {
	mb.task();
	if (millis() - debutTempoModbus >= tempoModbus) {
		debutTempoModbus = millis();
		writeToGTC_variables();
		readFromGTC();
		saveCTA(0);
	}
}

void loadCTA(int address) {
	SOUFFLAGE = EEPROM.readInt(address); address += sizeof(int);
	REPRISE = EEPROM.readInt(address); address += sizeof(int);
	EXTRACTION = EEPROM.readInt(address); address += sizeof(int);
	AIRNEUF = EEPROM.readInt(address); address += sizeof(int);
	seuilTempHorsGel = EEPROM.readInt(address); address += sizeof(int);
	tempoRecyclage = EEPROM.readInt(address); address += sizeof(int);
	debitMaxCTA = EEPROM.readInt(address); address += sizeof(int);
	alarmePeriode = EEPROM.readInt(address); address += sizeof(int);
	alarmeSurVentil = EEPROM.readInt(address); address += sizeof(int);
	codeUser = EEPROM.readInt(address); address += sizeof(int);
										  
	address += sizeof(int);

	V1.startAddress = address;
	address += V1.addressSize;
	V2.startAddress = address;
	address += V2.addressSize;
	V1.loadFromEEPROM();
	V2.loadFromEEPROM();

	for (int i = 0; i < 4; i++) {
		ip[i] = EEPROM.readByte(address); address += sizeof(byte);
		jourOccupe_int = EEPROM.readByte(address);
	}
	for (int i = 0; i < 4; i++) {
		gw[i] = EEPROM.readByte(address); address += sizeof(byte);

	}
	IPToString();

	modeMarche_Applique = (modeMarche)EEPROM.readInt(address); address += sizeof(int);
	tempoModbus = EEPROM.readInt(address); address += sizeof(int);
	
}
										  
										  
void saveCTA(int address) {
	writeToGTC_params();
	EEPROM.updateInt(address, SOUFFLAGE); address += sizeof(int);
	EEPROM.updateInt(address, REPRISE); address += sizeof(int);
	EEPROM.updateInt(address, EXTRACTION); address += sizeof(int);
	EEPROM.updateInt(address, AIRNEUF); address += sizeof(int);
	EEPROM.updateInt(address, seuilTempHorsGel); address += sizeof(int);
	EEPROM.updateInt(address, tempoRecyclage); address += sizeof(int);
	EEPROM.updateInt(address, debitMaxCTA); address += sizeof(int);
	EEPROM.updateInt(address, alarmePeriode); address += sizeof(int);
	EEPROM.updateInt(address, alarmeSurVentil); address += sizeof(int);
	EEPROM.updateInt(address, codeUser); address += sizeof(int);

										  
	address += sizeof(int);

	address += V1.addressSize;
	address += V2.addressSize;
	V1.saveToEEPROM();
	V2.saveToEEPROM();

	//stringToIp();
	for (int i = 0; i < 4; i++) {
		EEPROM.updateByte(address, ip[i]); address += sizeof(byte);
	}
	for (int i = 0; i < 4; i++) {
		EEPROM.updateByte(address, gw[i]); address += sizeof(byte);
	}
	EEPROM.updateInt(address, modeMarche_Applique); address += sizeof(int);
	
	EEPROM.updateInt(address, tempoModbus); address += sizeof(int);

}

void initCTA() {

	loadCTA(0);
	V2.myPID.SetMode(AUTOMATIC);
	V2.myPID.SetOutputLimits(0, 255);
	V2.myPID.SetTunings(V2.Kp, V2.Ki, V2.Kd);
	V1.myPID.SetMode(AUTOMATIC);
	V1.myPID.SetOutputLimits(0, 255);
	V1.myPID.SetTunings(V1.Kp, V1.Ki, V1.Kd);
}


uint32_t timeStamp(uint8_t a, uint8_t m, uint8_t j, uint8_t hh, uint8_t mm, uint8_t ss)
{
	uint32_t nbjours;

	nbjours = NB_JOURS_ANNEES_PRECEDENTES(a);

	switch (m)
	{
	case  2: nbjours += 31; break;
	case  3: nbjours += 59; break;
	case  4: nbjours += 90; break;
	case  5: nbjours += 120; break;
	case  6: nbjours += 151; break;
	case  7: nbjours += 181; break;
	case  8: nbjours += 212; break;
	case  9: nbjours += 243; break;
	case 10: nbjours += 273; break;
	case 11: nbjours += 304; break;
	case 12: nbjours += 334; break;
	}

	if ((m < 3) || PAS_BISSEXTILE(a))
	{
		nbjours += j - 1;
	}
	else // le 29/02 est passé
	{
		nbjours += j;
	}
	unsigned long Ts = ss + (60 * (mm + (60 * (hh + (24 * nbjours)))));
	return  Ts;
}

void decomposerTimestamp(uint32_t ts, uint8_t *a, uint8_t *m, uint8_t *j, uint8_t *hh, uint8_t *mm, uint8_t *ss)
{
	uint32_t li;
	uint16_t deb, fin;
	uint8_t i;

	li = ts / 86400;
	*a = (li - ((li + 1401) / 1461)) / 365;

	li = li + 1 - NB_JOURS_ANNEES_PRECEDENTES(*a);
	fin = 0;
	for (i = 1; i <= 12; i++)
	{
		deb = fin;
		switch (i)
		{
		case  2:
			fin += 28 + (!PAS_BISSEXTILE(*a));
			break;
		case  4:
		case  6:
		case  9:
		case 11:
			fin += 30;
			break;
		default:
			fin += 31;
			break;
		}
		if (fin >= li)
		{
			*m = i;
			*j = li - deb;
			i = 12;
		}
	}

	li = ts % 86400;
	*hh = li / 3600;
	li = li % 3600;
	*mm = li / 60;
	*ss = li % 60;
}

void RTCmiseajour() {

	if (modifdate == true) {
		Controllino_RTC_init(0);
		Controllino_SetTimeDate(jour, jourSemaine, mois, annee, heure, minute, 12);
		delay(10);
		modifdate = false;
		return;
	}
	jour = Controllino_GetDay();
	jourSemaine = Controllino_GetWeekDay();
	minute = Controllino_GetMinute();
	heure = Controllino_GetHour();
	mois = Controllino_GetMonth();
	annee = Controllino_GetYear();


}

void setup() {
	EEPROM.setMemPool(memoryBase, EEPROMSizeNano); //Set memorypool base to 32, assume Arduino Uno board
	pinMode(pin_TEMPERATURE, INPUT);
	Serial.begin(9600);
	Serial1.begin(9600);
	sensors.begin();
	initCTA();
	Controllino_RTC_init(0);
	startScreen();
	form = MENU;
	//genie.WriteObject(GENIE_OBJ_FORM, form, 0);
	getDS18B20Addresses(4);
	delay(200);

	setModbus();
	delay(1000);
	writeToGTC_params();

	//genie.WriteObject(GENIE_OBJ_LED_DIGITS, 2, tempoRecyclage);
	//genie.WriteObject(GENIE_OBJ_LED_DIGITS, 3, 0);
	modeMarcheAuto_Applique = INOCCUPE;
	//genie.WriteObject(GENIE_OBJ_4DBUTTON, modeMarcheAuto_Applique, 1);
}


void regulDebit() {

	switch (modeMarche_Applique)
	{
	case 0: //ARRET
		V1.marche = false;
		V2.marche = false;
		break;
	case 1://MANU
		V1.marche = V1.commandeMarche;
		V2.marche = V2.commandeMarche;
		V1.consigne_m3h = V1.consigneDebitModeOccupe;
		V2.consigne_m3h = V2.consigneDebitModeOccupe;

		break;
	case 2: // AUTO
		V1.marche = true;
		V2.marche = true;

		switch (modeMarcheAuto_Applique) {
		case INOCCUPE:
			V1.consigne_m3h = V1.consigneDebitModeInoccupe;
			V2.consigne_m3h = V2.consigneDebitModeInoccupe;
			break;
		case OCCUPE:
			V1.consigne_m3h = V1.consigneDebitModeOccupe;
			V2.consigne_m3h = V2.consigneDebitModeOccupe;
			break;
		case RECYCLAGE:
			V1.consigne_m3h = V1.consigneDebitFreeCooling;
			V2.consigne_m3h = V2.consigneDebitFreeCooling;
			break;

		}

	}
	digitalWrite(pin_ONOFF_V1, V1.marche);
	digitalWrite(pin_ONOFF_V2, V2.marche);
	V2.calculDebit(P2.read());
	V1.calculDebit(P1.read());

	if (V1.marche) {
		V1.consigne_app_m3h = V1.consigne_m3h;
		V1.myPID.Compute();
	}
	else {
		V1.consigne_app_m3h = -1000;
		V1.cons_app_pts = 0;
	}
	V1.update();

	if (V2.marche) {
		V2.consigne_app_m3h = V2.consigne_m3h;
		V2.myPID.Compute();
	}
	else {
		V2.consigne_app_m3h = -1000;
		V2.cons_app_pts = 0;
	}
	V2.update();


	if (V1.marche || V2.marche) {
		digitalWrite(VOYANT_MARCHE, true);
	}
	writeScreen();
}

void loop() {
	genie.DoEvents(); // Gestion des evenements de l'ecran		

	myGenieEventHandler();
	regulDebit();
	writeScreen();
	modbus();
	//Serial.print("Mode : "); Serial.println(modeMarcheAuto_Applique);
	
	TimeHandler();
	
	if (millis() >= waitPeriod)
	{
		checkAlarme();
		writeScreen();
		waitPeriod = millis() + 600;
	}
	if (millis() >= waitPeriodTemps)
	{
		readMesures();
		waitPeriodTemps = millis() + 500;
	}
	if (millis() >= waitPeriodDate)
	{
		RTCmiseajour();

		waitPeriodDate = millis() + 30000;
	}
}

void checkAlarme() {

	if (V1.cons_app_pc > alarmeSurVentil &&  V1.memoCheckAlarmeSurVentil == true) {
		V1.alarmeSurVentil = true;
	}
	else V1.alarmeSurVentil = false;
	if (V2.cons_app_pc > alarmeSurVentil && V2.memoCheckAlarmeSurVentil == true) {
		V2.alarmeSurVentil = true;
	}
	else V2.alarmeSurVentil = false;

	unsigned long alarmePeriodememo = timeStamp(0, alarmePeriode, 0, 0, 0, 0);

	unsigned long now = timeStamp(Controllino_GetYear(), Controllino_GetMonth(), Controllino_GetDay(), Controllino_GetHour(), Controllino_GetMinute(), Controllino_GetSecond());

	if (V1.changementFiltre + alarmePeriodememo  < now) {
		V1.alarmePeriode = true;
	}
	else V1.alarmePeriode = false;

	if (V2.changementFiltre + alarmePeriodememo < now) {
		V2.alarmePeriode = true;
	}
	else V2.alarmePeriode = false;
	if (V1.cons_app_pc > alarmeSurVentil) {
		V2.memoCheckAlarmeSurVentil = true;
	}
	else V2.memoCheckAlarmeSurVentil = false;
	if (V1.cons_app_pc > alarmeSurVentil) {
		V1.memoCheckAlarmeSurVentil = true;
	}
	else V1.memoCheckAlarmeSurVentil = false;

}

void myGenieEventHandler(void)
{

	//Serial.println("ok");
	int keyboardValue;
	genieFrame Event;
	genie.DequeueEvent(&Event); // Remove the next queued event from the buffer, and process it below
	
								//If the cmd received is from a Reported Event (Events triggered from the Events tab of Workshop4 objects)
	if (Event.reportObject.cmd == GENIE_REPORT_EVENT)
	{
		waitPeriod = 0;
		if (Event.reportObject.object == GENIE_OBJ_4DBUTTON) // If this event is from a 4DButton
		{
			switch (Event.reportObject.index) {
			case 0: //MARCHEV1
				if (Event.reportObject.data_lsb == 0) V1.commandeMarche = false;
				else V1.commandeMarche = true;
				break;
			case 5: //MARCHEV2
				if (Event.reportObject.data_lsb == 0) V2.commandeMarche = false;
				else V2.commandeMarche = true;
				break;
			case 4: //MODE RECYCLAGE
				if (modeMarcheAuto_Applique != modeMarcheAuto::RECYCLAGE) { 
					TIME = 0;
					tempsRestantMin = (int)(tempoRecyclage - (TIME / 60) - 1);
					tempsRestantSec = 0;
					finTempo = true;
				}
				Serial.print("pushed button recyclage");
				modeMarcheAuto_Applique = modeMarcheAuto::RECYCLAGE;
				timeOK = true;
				break;
			case 1: //MODE OCCUPE
				modeMarcheAuto_Applique = modeMarcheAuto::OCCUPE; 
				TIME = 0;
				break;
			case 2: //MODE INOCCUPE
				modeMarcheAuto_Applique = modeMarcheAuto::INOCCUPE; 
				TIME = 0;
				break;
			}
			changedParams = true;
			saveCTA(0);
		}
		if (Event.reportObject.object == GENIE_OBJ_DIPSW) // If this event is from a DIPSWITCH
		{
			switch (Event.reportObject.index) {
			case 0: //MODE MARCHE
				if (Event.reportObject.data_lsb == 0) { modeMarche_Applique = modeMarche::ARRET;  }
				else if (Event.reportObject.data_lsb == 1) { modeMarche_Applique = modeMarche::MANU;  }
				else if (Event.reportObject.data_lsb == 2) { modeMarche_Applique = modeMarche::AUTO; }				
				break;
			}
			changedParams = true;
		}
		if (Event.reportObject.object == GENIE_OBJ_SLIDER) // If this event is from a SLIDER
		{
			switch (Event.reportObject.index) {
			case 0:
				slideConsigne_m3h = Event.reportObject.data_lsb;
				break;
			}
		}
		if (Event.reportObject.object == GENIE_OBJ_WINBUTTON) // If this event is from a WinButton
		{
			//Serial.println("EVENT");
			//Serial.print(Event.reportObject.object);//////////////////////////////
			str = "";
			switch (Event.reportObject.index) {
			case 65: form = FORM::MENUADMIN_FULL;
				param = "mot de passe admin";
				genie.WriteObject(GENIE_OBJ_FORM, FORM::KEYBOARD, 0);
				break;
			case 20: //FORM MENUADMIN ==>Retour
			case 30: //FORM MENUADMIN ==>Retour
			case 19: //FORM MENUADMIN ==>Retour
			case 29: //FORM MENUADMIN ==>Retour
			case 88: //FORM MENUADMIN ==>Retour
			case 70: //FORM MENUADMIN ==>Retour
			case 66: //FORM MENUADMIN ==>Retour
				form = FORM::MENUADMIN_FULL;
				genie.WriteObject(GENIE_OBJ_FORM, form, 0);
				break;
			case 59:// FORM SYNOPTIQUE
				form = FORM::SYNOPTIQUE;
				waitPeriodTemps = 0;
				genie.WriteObject(GENIE_OBJ_FORM, form, 0);
				break;
			case 72: //FORM CONFIG CTA
				form = FORM::CONFIG_CTA;
				genie.WriteObject(GENIE_OBJ_FORM, form, 0);
				break;
			case 77: //FORM Params Ventilateurs
				form = FORM::PARAMS_VENTILO;
				genie.WriteObject(GENIE_OBJ_FORM, form, 0);
				break;
			case 21: //modif de la date et l'heure
			case 67:
				form = FORM::DATE_ET_HEURE;
				genie.WriteObject(GENIE_OBJ_FORM, form, 0);
				break;
			case 60:
				form = FORM::PARAMS_CTA;
				param = "mot de passe utilisateur";
				genie.WriteObject(GENIE_OBJ_FORM, FORM::KEYBOARD, 0);
				Serial.println("KEEEEEEEEEEEYYYYYYYYYYYYBOARDDDDDDD");
				break;
			case 0: //FORM Params RESEAU
				form = FORM::PARAMS_RESEAU;
				genie.WriteObject(GENIE_OBJ_FORM, form, 0);
				break;
			case 78:
				bpmarche = false;
				break;
			case 79:
				bpmarche = true;
				break;
			case 89:
				form = FORM::FILTRE;
				genie.WriteObject(GENIE_OBJ_FORM, form, 0);
				break;
			case 91:
				V1.secondeChangementFiltre = Controllino_GetSecond();
				V1.minuteChangementFiltre = Controllino_GetMinute();
				V1.heureChangementFiltre = Controllino_GetHour();
				V1.jourChangementFiltre = Controllino_GetDay();
				V1.moisChangementFiltre = Controllino_GetMonth();
				V1.anneeChangementFiltre = Controllino_GetYear();
				V1.changementFiltre = timeStamp(Controllino_GetYear(), Controllino_GetMonth(), Controllino_GetDay(), Controllino_GetHour(), Controllino_GetMinute(), Controllino_GetSecond());
				saveCTA(0);
				break;
			case 92:
				V2.secondeChangementFiltre = Controllino_GetSecond();
				V2.minuteChangementFiltre = Controllino_GetMinute();
				V2.heureChangementFiltre = Controllino_GetHour();
				V2.jourChangementFiltre = Controllino_GetDay();
				V2.moisChangementFiltre = Controllino_GetMonth();
				V2.anneeChangementFiltre = Controllino_GetYear();
				V2.changementFiltre = timeStamp(Controllino_GetYear(), Controllino_GetMonth(), Controllino_GetDay(), Controllino_GetHour(), Controllino_GetMinute(), Controllino_GetMinute());
				saveCTA(0);
				break;
			case 81:
			case 18: //FORM MENU
			case 41: //FORM MENU
			case 51: //FORM MENU
			case 22:  //FORM MENU
				form = MENU;
				genie.WriteObject(GENIE_OBJ_FORM, form, 0);
				break;
			case 7: //FORM MENU
				form = MENU;
				genie.WriteObject(GENIE_OBJ_FORM, form, 0);
				Serial.println("retour OK");
				break;
			case 80: //FORM MENU
				adminLoggedIn = false;
				form = MENU;
				genie.WriteObject(GENIE_OBJ_FORM, form, 0);
				break;
			case 36: //FORM MENU depuis FORM PARAMS CTA
				userLoggedIn = false;
				form = MENU;
				genie.WriteObject(GENIE_OBJ_FORM, form, 0);
				break;
				//Modifs de params au clavier
			case 8:
				processToKeyboard(&V1.Kp, "Kp Ventilo Soufflage", 0, 1000);
				break;
			case 9:
				processToKeyboard(&V1.Ki, "Ki Ventilo Soufflage", 0, 1000);
				break;
			case 10:
				processToKeyboard(&V1.Kd, "Kd Ventilo Soufflage", 0, 1000);
				break;
			case 11:
				processToKeyboard(&V1.Kfactor, "Kfactor Ventilo Soufflage", 0, 1000);
				break;
			case 12:
				processToKeyboard(&V2.Kp, "Kp Ventilo Reprise", 0, 1000);
				break;
			case 13:
				processToKeyboard(&V2.Ki, "Ki Ventilo Reprise", 0, 1000);
				break;
			case 14:
				processToKeyboard(&V2.Kd, "Kd Ventilo Reprise", 0, 1000);
				break;
			case 15:
				processToKeyboard(&V2.Kfactor, "Kfactor Ventilo Reprise", 0, 1000);
				break;
			case 17:
				processToKeyboard(&tempoRecyclage, "Tempo Recyclage", 0, 1000);
				break;
			case 23:
				processToKeyboard(&jourSemaine, "Jour de la semaine 1:Lundi 2:Mardi ...", 0, 7);
				break;
			case 24:
				processToKeyboard(&jour, "Jours", 0, 31);
				break;
			case 25:
				processToKeyboard(&mois, "Mois", 0, 12);
				break;
			case 26:
				processToKeyboard(&annee, "Annee 2017=17,2018=18", 0, 100);
				break;
			case 27:
				processToKeyboard(&heure, "Heure", 0, 24);
				break;
			case 28:
				processToKeyboard(&minute, "Minute", 0, 59);
				break;
			case 34:
				processToKeyboard(&SOUFFLAGE, "Num sonde soufflage", 0, 3);
				break;
			case 32:
				processToKeyboard(&REPRISE, "Num sonde reprise", 0, 3);
				break;
			case 33:
				processToKeyboard(&EXTRACTION, "Num sonde extraction", 0, 3);
				break;
			case 31:
				processToKeyboard(&AIRNEUF, "Num sonde air neuf", 0, 3);
				break;
			case 35:
				processToKeyboard(&debitMaxCTA, "Debit Max CTA", 0, 20000);
				break;
			case 39:
				processToKeyboard(&tempoModbus, "tempoModBus", 0, 1000);
				break;
			case 2:
				processToKeyboard(&V1.consigneDebitModeOccupe, "Soufflage cons debit Occupe", 0, debitMaxCTA);
				break;
			case 3:
				processToKeyboard(&V1.consigneDebitModeInoccupe, "Soufflage cons debit Inocc.", 0, debitMaxCTA);
				break;
			case 16:
				processToKeyboard(&V1.consigneDebitFreeCooling, "Soufflage cons debit FreeCooling", 0, debitMaxCTA);
				break;
			case 1:
				processToKeyboard(&V2.consigneDebitModeOccupe, "Reprise cons debit Occupe", 0, debitMaxCTA);
				break;
			case 4:
				processToKeyboard(&V2.consigneDebitModeInoccupe, "Reprise cons debit Inocc.", 0, debitMaxCTA);
				break;
			case 5:
				processToKeyboard(&V2.consigneDebitFreeCooling, "Reprise cons debit FreeCooling", 0, debitMaxCTA);
				break;
			case 54:
				processToKeyboard(&passerelle, "Gateway");
				break;
			case 55:
				processToKeyboard(&adresseIP, "IP Address");
				break;
			case 56:
				form = FORM::PARAMS_RESEAU;
				genie.WriteObject(GENIE_OBJ_FORM, form, 0);
				saveCTA(0);
				//resetFunc();
				break;

			case 86:
				form_memo = 50;
				processToKeyboard(&alarmePeriode, "seuil alarme periodique (en mois)", 0, 24);
				break;
			case 87:
				form_memo = 50;
				processToKeyboard(&alarmeSurVentil, "Alarme seuil vitesse ventilateur (%) ", 0, 100);
				break;
			case 6:
				newMdp = true;
				processToKeyboard(&codeUser, "Nouveau mot de passe (4 chiffres) ", 0, 9999);
				break;
			}
			Serial.print("			KB = "); Serial.println(KB);
			if (KB) {
				Serial.print("			readIntFromKB = "); Serial.println(readIntFromKB);
				if (readIntFromKB) {
					processToKeyboard(int_ptr, param, int_KBmin, int_KBmax);
					readIntFromKB = false;
				}
				else {
					processToKeyboard(ptr, param, double_KBmin, double_KBmax);
				}
				KB = false;
			}
		}
		if (Event.reportObject.object == GENIE_OBJ_KEYBOARD) // If this event is from a Keyboard
		{
			if (Event.reportObject.index == 0)  // If from Keyboard0
			{
				keyboardValue = genie.GetEventData(&Event); // Get data from Keyboard0
				if (keyboardValue == 12) // Valider
				{
					testAfficheParamsCTA2 = true;
					testAfficheConfigCTA = true;
					testAfficheFiltre = true;
					testAfficheRes = true;
					testAfficheMenuAdmin = true;
					testAfficheParamsVentilo = true;
					if (form == FORM::DATE_ET_HEURE) modifdate = true;
					if (form == FORM::MENUADMIN_FULL) {
						if (!adminLoggedIn) {
							if (str.toInt() != codeAdmin) form = FORM::MENU;
							else {
								adminLoggedIn = true;
								form = FORM::MENUADMIN_FULL;
								genie.WriteObject(GENIE_OBJ_FORM, form, 0);
							}
						}
					}
					if (form == FORM::PARAMS_CTA) {
						if (newMdp == true) {
							codeUser = str.toInt();
							newMdp = false;
							form = FORM::PARAMS_CTA;
							genie.WriteObject(GENIE_OBJ_FORM, form, 0);
						}
						if (!userLoggedIn) {
							if (str.toInt() != codeUser) form = FORM::MENU; 
							else {
								userLoggedIn = true;
								testAfficheParamsCTA = true;
								form = FORM::PARAMS_CTA;
							}
							genie.WriteObject(GENIE_OBJ_FORM, form, 0);
						}
						ModifVar(str);
					}
					else {
						ModifVar(str);
						changedParams = true;
						if (V1.changedPID) {
							V1.myPID.SetTunings(V1.Kp, V1.Ki, V1.Kd);
							V1.changedPID = false;
						}
						if (V2.changedPID) {
							V2.myPID.SetTunings(V2.Kp, V2.Ki, V2.Kd);
							V2.changedPID = false;
						}
					}
					if (!erreurKB) genie.WriteObject(GENIE_OBJ_FORM, form, 0);

				}
				else if (keyboardValue == 11) //ANNULER
				{
					testAfficheParamsCTA2 = true;
					testAfficheConfigCTA = true;
					testAfficheFiltre = true;
					testAfficheRes = true;
					testAfficheMenuAdmin = true;
					testAfficheParamsVentilo = true;
					if (V1.changedPID) V1.changedPID = false;
					if (V2.changedPID) V2.changedPID = false;
					str = "";
					if ((form == FORM::PARAMS_CTA && !userLoggedIn) || (form == FORM::MENUADMIN_FULL && !adminLoggedIn)) {
						form = MENU;
					}

					genie.WriteObject(GENIE_OBJ_FORM, form, 0);
				}
				else if (keyboardValue == 10) //'.'
				{
					str += ".";
					genie.WriteStr(11, str);
				}
				else if (keyboardValue == 13) //DELETE
				{
					str.remove(str.length() - 1);
					genie.WriteStr(11, str);
				}
				else {
					str += keyboardValue;
					genie.WriteStr(11, str);
				}
			}
		}
		saveCTA(0);
	}
	
}

bool checkHeure(int h) {
	if (String(h).length() > 4 || String(h).length() < 3) return false;
	int H = (int)(h / 100);
	int m = h - H * 100;
	if (H > 23) return false;
	if (m > 59) return false;
	return true;
}

void processToKeyboard(int * ptr, String caption, int min, int max) {
	genie.WriteObject(GENIE_OBJ_FORM, FORM::KEYBOARD, 0);
	int_ptr = ptr;
	readIntFromKB = true;
	param = caption;
	genie.WriteStr(53, param);
	int_KBmin = min;
	int_KBmax = max;
}
void processToKeyboard(int * ptr, String caption) { //HEURE
	genie.WriteObject(GENIE_OBJ_FORM, FORM::KEYBOARD, 0);
	int_ptr = ptr;
	readHeureFromKB = true;
	param = caption;
}
void processToKeyboard(unsigned long * ptr, String caption, int min, int max) {
	genie.WriteObject(GENIE_OBJ_FORM, FORM::KEYBOARD, 0);
	int_ptr = (int*)ptr;
	readIntFromKB = true;
	param = caption;
	int_KBmin = min;
	int_KBmax = max;
}
void processToKeyboard(double * dptr, String caption, double min, double max) {
	genie.WriteObject(GENIE_OBJ_FORM, FORM::KEYBOARD, 0);
	ptr = dptr;
	param = caption;
	double_KBmin = min;
	double_KBmax = max;
}
void processToKeyboard(String * ptr, String caption) {
	genie.WriteObject(GENIE_OBJ_FORM, FORM::KEYBOARD, 0);
	string_ptr = ptr;
	param = caption;

	readStrFromKB = true;
}



void stringToIp() {
	int point1 = adresseIP.indexOf('.');
	int point2 = adresseIP.indexOf('.', point1 + 1);
	int point3 = adresseIP.indexOf('.', point2 + 1);
	ip[0] = adresseIP.substring(0, point1).toInt();
	ip[1] = adresseIP.substring(point1 + 1, point2).toInt();
	ip[2] = adresseIP.substring(point2 + 1, point3).toInt();
	ip[3] = adresseIP.substring(point3 + 1).toInt();
	point1 = passerelle.indexOf('.');
	point2 = passerelle.indexOf('.', point1 + 1);
	point3 = passerelle.indexOf('.', point2 + 1);
	gw[0] = passerelle.substring(0, point1).toInt();
	gw[1] = passerelle.substring(point1 + 1, point2).toInt();
	gw[2] = passerelle.substring(point2 + 1, point3).toInt();
	gw[3] = passerelle.substring(point3 + 1).toInt();
}

void IPToString() {
	adresseIP = "";
	adresseIP += ip[0]; adresseIP += "."; adresseIP += ip[1]; adresseIP += "."; adresseIP += ip[2]; adresseIP += "."; adresseIP += ip[3];
	passerelle = "";
	passerelle += gw[0]; passerelle += "."; passerelle += gw[1]; passerelle += "."; passerelle += gw[2]; passerelle += "."; passerelle += gw[3];
}


void startScreen() {
	/*4D SYSTEMS*/
	genie.Begin(Serial1);
	genie.AttachEventHandler(myGenieEventHandler);
	pinMode(RESETLINE, OUTPUT);  // Set D4 on Arduino to Output (4D Arduino Adaptor V2 - Display Reset)
	digitalWrite(RESETLINE, 0);  // Reset the Display via D4
	delay(100);
	digitalWrite(RESETLINE, 1);  // unReset the Display via D4
	delay(5000); //let the display start up after the reset (This is important)
	genie.WriteContrast(15);
	waitPeriod = millis();
}

void readMesures() {
	sensors.requestTemperatures();
	if (temperature[0].status == READ_OK)temperature[0].value = sensors.getTempCByIndex(0);
	if (temperature[1].status == READ_OK)temperature[1].value = sensors.getTempCByIndex(1);
	if (temperature[2].status == READ_OK)temperature[2].value = sensors.getTempCByIndex(2);
	if (temperature[3].status == READ_OK)temperature[3].value = sensors.getTempCByIndex(3);
}

void writeScreen() {
	String timeString;
	if (jour >= 10) { timeString.concat(String(jour)); }
	else { timeString.concat("0"); timeString.concat(String(jour)); }	
	timeString.concat("/");
	if (mois >= 10) { timeString.concat(String(mois)); }
	else { timeString.concat("0"); timeString.concat(String(mois)); }
	timeString += "/";
	if (annee >= 10) { timeString.concat(String(annee)); }	//Heures
	else { timeString.concat("0"); timeString.concat(String(annee)); }
	timeString += "  "; 
	if (heure >= 10) { timeString.concat(String(heure)); }	//Heures
	else { timeString.concat("0"); timeString.concat(String(heure)); }
	timeString += ":";
	if (minute >= 10) { timeString.concat(String(minute)); }	//Minutes
	else{ timeString.concat("0"); timeString.concat(String(minute)); }
	String modeString = "Mode: ";
	String problemeString = "DEFAUTS: ";
	String temp = "";
	String stringdate = "";
	String stringRegistre = "";
	String stringFiltre = "";
	String stringModeMarche = "";

	//Serial.print("FORM:"); Serial.println(form);
	switch (form) {
	case FORM::PARAMS_RESEAU:
		if (adresseIP != adresseIP_memo || passerelle != passerelle_memo || form_memo != 0) {
			genie.WriteStr(32, adresseIP);
			genie.WriteStr(55, passerelle);
			adresseIP_memo = adresseIP;
			passerelle_memo = passerelle;
			//genie.WriteStr(30, tempoModbus);
		}
		else if (adresseIP == adresseIP_memo && passerelle == passerelle_memo && testAfficheRes == true) {
			genie.WriteStr(32, adresseIP);
			genie.WriteStr(55, passerelle);
			testAfficheRes = false;
		}
		form_memo = 0;
		break;
	case FORM::MENU:
		if (timeString != timeString_memo || form_memo != 1) { genie.WriteStr(58, timeString); timeString_memo = timeString; }
		form_memo = 1;
		break;
	case FORM::MENUADMIN_FULL:
		if (form_memo != 2 || testAfficheMenuAdmin == true) {
			genie.WriteStr(62, versioncontrollino);
			genie.WriteStr(30, codeUser);
			testAfficheMenuAdmin = false;
		}
		form_memo = 2;
		break;
	case FORM::SYNOPTIQUE:
		if (modeMarche_Applique == modeMarche::ARRET) modeString += "ARRET";
		if (modeMarche_Applique == modeMarche::MANU) modeString += "MANU";
		if (modeMarche_Applique == modeMarche::AUTO) modeString += "AUTO";

		/*genie.WriteStr(0, temperature[SOUFFLAGE].value);
		genie.WriteStr(1, temperature[REPRISE].value);
		genie.WriteStr(2, temperature[EXTRACTION].value);
		genie.WriteStr(3, temperature[AIRNEUF].value);*/

		if (modeString != modeString_memo || form_memo != 3) { genie.WriteStr(57, modeString); modeString_memo = modeString; }


		if ((int)V1.debit != debitV1_memo || form_memo != 3) { genie.WriteStr(26, (int)V1.debit); debitV1_memo = (int)V1.debit; }
		if ((int)V2.debit != debitV2_memo || form_memo != 3) { genie.WriteStr(25, (int)V2.debit); debitV2_memo = (int)V2.debit; }
		/*genie.WriteStr(26, (int)V1.debit);
		genie.WriteStr(25, (int)V2.debit);*/

		//genie.WriteStr(57, modeString);

		if (temperature[SOUFFLAGE].value != tempSoufflage_memo || form_memo != 3) { genie.WriteStr(0, temperature[SOUFFLAGE].value); tempSoufflage_memo = temperature[SOUFFLAGE].value; }
		if (temperature[REPRISE].value != tempReprise_memo || form_memo != 3) { genie.WriteStr(1, temperature[REPRISE].value); tempReprise_memo = temperature[REPRISE].value; }
		if (temperature[EXTRACTION].value != tempExtraction_memo || form_memo != 3) { genie.WriteStr(2, temperature[EXTRACTION].value); tempExtraction_memo = temperature[EXTRACTION].value; }
		if (temperature[AIRNEUF].value != tempAirneuf_memo || form_memo != 3) { genie.WriteStr(3, temperature[AIRNEUF].value); tempAirneuf_memo = temperature[AIRNEUF].value; }
		/*
		tempSoufflage = temperature[SOUFFLAGE].value; tempReprise = temperature[REPRISE].value; tempExtraction = temperature[EXTRACTION].value; tempAirneuf = temperature[AIRNEUF].value;

		if (tempSoufflage != tempSoufflage_memo || form_memo != 3) { genie.WriteStr(0, temperature[SOUFFLAGE].value); tempSoufflage_memo = temperature[SOUFFLAGE].value; }
		if (tempReprise != tempReprise_memo || form_memo != 3) { genie.WriteStr(1, temperature[REPRISE].value); tempReprise_memo = temperature[REPRISE].value; }
		if (tempExtraction != tempExtraction_memo || form_memo != 3) { genie.WriteStr(2, temperature[EXTRACTION].value); tempExtraction_memo = temperature[EXTRACTION].value; }
		if (tempAirneuf != tempAirneuf_memo || form_memo != 3) { genie.WriteStr(3, temperature[AIRNEUF].value); tempAirneuf_memo = temperature[AIRNEUF].value; }
		*/
		defautFiltre = 0;
		if (V1.alarmePeriode == true || V1.alarmeSurVentil == true) defautFiltre = 1;
		if (V2.alarmePeriode == true || V2.alarmeSurVentil == true) {
			if (defautFiltre == 1) defautFiltre = 3;
			else defautFiltre = 2;
		}
		if (defautFiltre != defautFiltre_memo || form_memo != 3) {
			genie.WriteObject(GENIE_OBJ_STRINGS, 4, defautFiltre);
			//genie.WriteStr(4, defautFiltre);
			defautFiltre_memo = defautFiltre;
		}

		tempsRestantMin = (int)(tempoRecyclage - (TIME / 60) - 1);
		tempsRestantSec = (tempoRecyclage * 60 - TIME) % 60;
		if (tempsRestantSec == 0) tempsRestantMin = tempsRestantMin + 1;
		genie.WriteObject(GENIE_OBJ_LED_DIGITS, 2, tempsRestantMin);
		genie.WriteObject(GENIE_OBJ_LED_DIGITS, 3, tempsRestantSec);

		if (form_memo != 3 || finTempo == true) {
			//if(tempsRestantMin == 0 && tempsRestantSec == 0 && finTempo == true){
			genie.WriteObject(GENIE_OBJ_4DBUTTON, modeMarcheAuto_Applique, 1);
			finTempo = false;
			Serial.print("Mode marche auto applique : "); Serial.println(modeMarcheAuto_Applique);
		}
		//else(Serial.println("BBBBB"));

		form_memo = 3;
		break;

	case FORM::DATE_ET_HEURE:
		if (jourSemaine == 1) stringdate += "Lundi ";
		if (jourSemaine == 2) stringdate += "Mardi ";
		if (jourSemaine == 3) stringdate += "Mercredi ";
		if (jourSemaine == 4) stringdate += "Jeudi ";
		if (jourSemaine == 5) stringdate += "Vendredi ";
		if (jourSemaine == 6) stringdate += "Samedi ";
		if (jourSemaine == 7) stringdate += "Dimanche ";
		stringdate += String(jour);
		stringdate += " ";
		if (mois == 1) stringdate += "Janvier ";
		if (mois == 2) stringdate += "Fevrier ";
		if (mois == 3) stringdate += "Mars ";
		if (mois == 4) stringdate += "Avril ";
		if (mois == 5) stringdate += "Mai ";
		if (mois == 6) stringdate += "Juin ";
		if (mois == 7) stringdate += "Juillet ";
		if (mois == 8) stringdate += "Aout ";
		if (mois == 9) stringdate += "Septembre ";
		if (mois == 10) stringdate += "Octobre ";
		if (mois == 11) stringdate += "Novembre ";
		if (mois == 12) stringdate += "Decembre ";
		stringdate += "20";
		stringdate += String(annee);
		if (stringdate != stringdate_memo || form_memo != 4) { genie.WriteStr(7, stringdate); stringdate_memo = stringdate; }
		genie.WriteObject(GENIE_OBJ_LED_DIGITS, 0, heure);
		genie.WriteObject(GENIE_OBJ_LED_DIGITS, 1, minute);
		form_memo = 4;
		break;
	case FORM::PARAMS_VENTILO:
		genie.WriteObject(GENIE_OBJ_4DBUTTON, 0, (int)V1.commandeMarche);
		genie.WriteObject(GENIE_OBJ_4DBUTTON, 5, (int)V2.commandeMarche);
		if (V1.consigne_m3h != consigne_m3hV1 || V2.consigne_m3h != consigne_m3hV2 || V1.Kp != KpV1 || V2.Kp != KpV2 || V1.Ki != KiV1 || V2.Ki != KiV2 || V1.Kd != KdV1 || V2.Kd != KdV2 || V1.Kfactor != KfactorV1 || V2.Kfactor != KfactorV2 || V1.debit != debitV1 || V2.debit != debitV2 || V1.cons_app_pc != cons_app_pcV1 || V2.cons_app_pc != cons_app_pcV2 || form_memo != 5) {
			genie.WriteStr(12, V2.consigne_m3h);
			genie.WriteStr(10, V1.consigne_m3h);
			genie.WriteStr(13, V1.Kp);
			genie.WriteStr(14, V1.Ki);
			genie.WriteStr(15, V1.Kd);
			genie.WriteStr(16, V1.Kfactor);
			genie.WriteStr(17, V2.Kp);
			genie.WriteStr(18, V2.Ki);
			genie.WriteStr(19, V2.Kd);
			genie.WriteStr(20, V2.Kfactor);
			genie.WriteStr(21, V1.debit);
			genie.WriteStr(22, V1.cons_app_pc);
			genie.WriteStr(23, V2.debit);
			genie.WriteStr(24, V2.cons_app_pc);
			consigne_m3hV1 = V1.consigne_m3h, consigne_m3hV2 = V2.consigne_m3h, KpV1 = V1.Kp, KpV2 = V2.Kp, KiV1 = V1.Ki, KiV2 = V2.Ki, KdV2 = V2.Kd, KdV1 = V1.Kd, KfactorV1 = V1.Kfactor, KfactorV2 = V2.Kfactor, debitV1 = V1.debit, debitV2=V2.debit, cons_app_pcV1 = V1.cons_app_pc, cons_app_pcV2= V2.cons_app_pc;
		}
		else if (V1.consigne_m3h == consigne_m3hV1 && V2.consigne_m3h == consigne_m3hV2 && V1.Kp == KpV1 && V2.Kp == KpV2 && V1.Ki == KiV1 && V2.Ki == KiV2 && V1.Kd == KdV1 && V2.Kd == KdV2 && V1.Kfactor == KfactorV1 && V2.Kfactor == KfactorV2 && V1.debit == debitV1 && V2.debit == debitV2 && V1.cons_app_pc == cons_app_pcV1 && V2.cons_app_pc == cons_app_pcV2 && testAfficheParamsVentilo == true) {
			genie.WriteStr(12, V2.consigne_m3h);
			genie.WriteStr(10, V1.consigne_m3h);
			genie.WriteStr(13, V1.Kp);
			genie.WriteStr(14, V1.Ki);
			genie.WriteStr(15, V1.Kd);
			genie.WriteStr(16, V1.Kfactor);
			genie.WriteStr(17, V2.Kp);
			genie.WriteStr(18, V2.Ki);
			genie.WriteStr(19, V2.Kd);
			genie.WriteStr(20, V2.Kfactor);
			genie.WriteStr(21, V1.debit);
			genie.WriteStr(22, V1.cons_app_pc);
			genie.WriteStr(23, V2.debit);
			genie.WriteStr(24, V2.cons_app_pc);
			testAfficheParamsVentilo = false;
		}
		form_memo = 5;
		break;
	case FORM::CONFIG_CTA:
		if (SOUFFLAGE != SOUFFLAGE_memo || REPRISE != REPRISE_memo || EXTRACTION != EXTRACTION_memo || AIRNEUF != AIRNEUF_memo || debitMaxCTA != debitMaxCTA_memo || form_memo != 6) {
			genie.WriteStr(35, SOUFFLAGE);
			genie.WriteStr(33, REPRISE);
			genie.WriteStr(34, EXTRACTION);
			genie.WriteStr(9, AIRNEUF);
			genie.WriteStr(36, debitMaxCTA);
			SOUFFLAGE_memo = SOUFFLAGE; REPRISE_memo = REPRISE; EXTRACTION_memo = EXTRACTION; AIRNEUF_memo = AIRNEUF; debitMaxCTA_memo = debitMaxCTA;
			Serial.println("CONFIG CTA --------- 1");
		}
		else if (SOUFFLAGE == SOUFFLAGE_memo && REPRISE == REPRISE_memo && EXTRACTION == EXTRACTION_memo && AIRNEUF == AIRNEUF_memo && debitMaxCTA == debitMaxCTA_memo && testAfficheConfigCTA == true) {
			genie.WriteStr(35, SOUFFLAGE);
			genie.WriteStr(33, REPRISE);
			genie.WriteStr(34, EXTRACTION);
			genie.WriteStr(9, AIRNEUF);
			genie.WriteStr(36, debitMaxCTA);
			testAfficheConfigCTA = false;
			Serial.println("CONFIG CTA --------- 2");

		}
		form_memo = 6;
		break;
	case FORM::PARAMS_CTA:
		Serial.println("PARAMS CTA !!!		1");
		genie.WriteObject(GENIE_OBJ_DIPSW, 0, (int)modeMarche_Applique);

		if (testAfficheParamsCTA == true) { write(); testAfficheParamsCTA = false; }

		Serial.println("PARAMS CTA !!!		2");

		genie.WriteStr(31, tempoRecyclage);

		if (V1.consigneDebitModeOccupe != consigneDebitModeOccupeV1_memo || form_memo != 7) {
			write();
			consigneDebitModeOccupeV1_memo = V1.consigneDebitModeOccupe;
		}
		else if (V1.consigneDebitModeOccupe == consigneDebitModeOccupeV1_memo && testAfficheParamsCTA2 == true) {
			write();
			testAfficheParamsCTA2 = false;
		}

		if (V2.consigneDebitModeOccupe != consigneDebitModeOccupeV2_memo || form_memo != 7) {
			write();
			consigneDebitModeOccupeV2_memo = V2.consigneDebitModeOccupe;
		}
		else if (V2.consigneDebitModeOccupe == consigneDebitModeOccupeV2_memo && testAfficheParamsCTA2 == true) {
			write();
			testAfficheParamsCTA2 = false;
		}
		if (V1.consigneDebitModeInoccupe != consigneDebitModeInoccupeV1_memo || form_memo != 7) {
			write();
			consigneDebitModeInoccupeV1_memo = V1.consigneDebitModeInoccupe;
		}
		else if (V1.consigneDebitModeInoccupe == consigneDebitModeInoccupeV1_memo && testAfficheParamsCTA2 == true) {
			write();
			testAfficheParamsCTA2 = false;
		}
		if (V2.consigneDebitModeInoccupe != consigneDebitModeInoccupeV2_memo || form_memo != 7) {
			write();
			consigneDebitModeInoccupeV2_memo = V2.consigneDebitModeInoccupe;
		}
		else if (V2.consigneDebitModeInoccupe == consigneDebitModeInoccupeV2_memo && testAfficheParamsCTA2 == true) {
			write();
			testAfficheParamsCTA2 = false;
		}
		if (V1.consigneDebitFreeCooling != consigneDebitFreeCoolingV1_memo || form_memo != 7) {
			write();
			consigneDebitFreeCoolingV1_memo = V1.consigneDebitFreeCooling;
		}
		else if (V1.consigneDebitFreeCooling == consigneDebitFreeCoolingV1_memo && testAfficheParamsCTA2 == true) {
			write();
			testAfficheParamsCTA2 = false;
		}
		if (V2.consigneDebitFreeCooling != consigneDebitFreeCoolingV2_memo || form_memo != 7) {
			write();
			consigneDebitFreeCoolingV2_memo = V2.consigneDebitFreeCooling;
		}
		else if (V2.consigneDebitFreeCooling == consigneDebitFreeCoolingV2_memo && testAfficheParamsCTA2 == true) {
			write();
			testAfficheParamsCTA2 = false;
		}

		Serial.println("PARAMS CTA !!!		3");

		form_memo = 7;
		break;
	case FORM::FILTRE:
		genie.WriteObject(GENIE_OBJ_LED, 2, (int)V1.alarmePeriode);
		genie.WriteObject(GENIE_OBJ_LED, 3, (int)V2.alarmePeriode);
		genie.WriteObject(GENIE_OBJ_LED, 4, (int)V2.alarmeSurVentil);
		genie.WriteObject(GENIE_OBJ_LED, 6, (int)V1.alarmeSurVentil);

		if (alarmePeriode != alarmePeriode_memo || alarmeSurVentil != alarmeSurVentil_memo || form_memo != 8) {
			genie.WriteStr(66, alarmePeriode);
			genie.WriteStr(67, alarmeSurVentil);
			alarmePeriode_memo = alarmePeriode;
			alarmeSurVentil_memo = alarmeSurVentil;
		}
		else if (alarmePeriode == alarmePeriode_memo && alarmeSurVentil == alarmeSurVentil_memo && testAfficheFiltre == true) {
			genie.WriteStr(66, alarmePeriode);
			genie.WriteStr(67, alarmeSurVentil);
			testAfficheFiltre = false;
		}

		stringFiltre = String(V1.jourChangementFiltre);
		stringFiltre += "/";
		stringFiltre += String(V1.moisChangementFiltre);
		stringFiltre += "/";
		stringFiltre += String(V1.anneeChangementFiltre);
		if (stringFiltre != stringFiltre1 || form_memo != 8 || testAfficheFiltre1 == true) {
			genie.WriteStr(68, stringFiltre);
			stringFiltre1 = stringFiltre;
			testAfficheFiltre1 = false;
		}
		stringFiltre1 = stringFiltre;
		stringFiltre = String(V2.jourChangementFiltre);
		stringFiltre += "/";
		stringFiltre += String(V2.moisChangementFiltre);
		stringFiltre += "/";
		stringFiltre += String(V2.anneeChangementFiltre);
		if (stringFiltre != stringFiltre2 || form_memo != 8 || testAfficheFiltre2 == true) {
			genie.WriteStr(69, stringFiltre);
			stringFiltre2 = stringFiltre;
			testAfficheFiltre2 = false;
		}
		form_memo = 8;
		break;
	case FORM::KEYBOARD:
		form_memo = 9;
		//break;
	}

}


/**
* Fonction de lecture de la temp?rature via un capteur DS18B20.
*/
byte getDS18B20Addresses(int nbSensors) {
	ds.reset_search();

	for (int i = 0; i < nbSensors; i++) {
		/* Recherche le prochain capteur 1-Wire disponible */
		if (!ds.search(temperature[i].addr)) {
			// Pas de capteur
			temperature[i].status = NO_SENSOR_FOUND;
			Serial.print("temperature[i].status = NO_SENSOR_FOUND");
			Serial.println(temperature[i].addr[0]);
		}
		// Verifie que l'adresse a ete correctement recue 
		if (OneWire::crc8(temperature[i].addr, 7) != temperature[i].addr[7]) {
			// Adresse invalide
			temperature[i].status = INVALID_ADDRESS;
			Serial.print("temperature[i].status = INVALID_ADDRESS");
		}
		// Verifie qu'il s'agit bien d'un DS18B20 
		if (temperature[i].addr[0] != 0x28) {
			// Mauvais type de capteur
			temperature[i].status = INVALID_SENSOR;
			Serial.print("temperature[i].status = INVALID_SENSOR");
		}
		//Serial.print(i); Serial.print(":"); Serial.println(temperature[i].status); Serial.print(":"); Serial.println(temperature[i].addr[0]);
		temperature[i].status = READ_OK;
	}
}

void TimeHandler(void) {
	
	if (modeMarcheAuto_Applique == 4 && timeOK == true) {
		Serial.println("yess");
		if (millis() >= tempoSec) {
			if (TIME >= tempoRecyclage * 60) {
				Serial.println("paasss");	
				modeMarcheAuto_Applique = modeMarcheAuto::INOCCUPE;
				genie.WriteObject(GENIE_OBJ_4DBUTTON, modeMarcheAuto_Applique, 1);
				timeOK = false;
				TIME = 0;
				tempsRestantMin = 0;
				tempsRestantSec = 0;
				saveCTA(0);
			}
			else {
				TIME++;
				Serial.print("\t\t\t TIME =");
				Serial.println(TIME);
				tempoSec = millis() + 1000;
			}
			
		}
	}
	/*if (TIME >= tempoRecyclage*60 + 1) {
		Serial.println("paasss");
		//genie.WriteObject(GENIE_OBJ_4DBUTTON, modeMarcheAuto_Applique, 1);
		TIME = 0;
		tempsRestantMin = 0;
		tempsRestantSec = 0;
		modeMarcheAuto_Applique = modeMarcheAuto::INOCCUPE;
	}
	else {
		TIME++;
		Serial.print("TIME =");
		Serial.println(TIME);
	}*/
}

void ModifVar(String str) {
	if (readIntFromKB) {
		if (str.toInt() >= int_KBmin && str.toInt() <= int_KBmax) {
			*int_ptr = str.toInt();
			erreurKB = false;
			readIntFromKB = false;
		}
		else erreurKB = true;
	}
	else if (readStrFromKB) {
		readStrFromKB = false;
		erreurKB = false;
		*string_ptr = str;
		stringToIp();
	}
	else if (readHeureFromKB) {

		if (checkHeure(str.toInt())) {
			*int_ptr = str.toInt();
			erreurKB = false;
			readHeureFromKB = false;
		}
		else erreurKB = true;
	}
	else {
		if (str.toFloat() >= double_KBmin && str.toFloat() <= double_KBmax) {
			*ptr = str.toFloat();
			erreurKB = false;
		}
		else erreurKB = true;
	}
}
void write() {
	genie.WriteStr(6, V1.consigneDebitModeOccupe);
	genie.WriteStr(5, V2.consigneDebitModeOccupe);
	genie.WriteStr(8, V1.consigneDebitModeInoccupe);
	genie.WriteStr(27, V2.consigneDebitModeInoccupe);
	genie.WriteStr(29, V1.consigneDebitFreeCooling);
	genie.WriteStr(28, V2.consigneDebitFreeCooling);
}